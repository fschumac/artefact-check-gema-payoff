﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using CheckGEMAPayoff.Classes;
using System.Windows.Markup;
using System.Globalization;
using CheckGEMAPayoff.Properties;
using DevFsFunctions.Logging;

namespace CheckGEMAPayoff
{
	/// <summary>
	/// Interaktionslogik für MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			// Ensure the current culture passed into bindings 
			// is the OS culture. By default, WPF uses en-US 
			// as the culture, regardless of the system settings.
			FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

			Manager.instance.ArtistInfo.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(CollectionChanged);
			Manager.instance.ManagedWorks.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(CollectionChanged);
			Manager.instance.AccountingSet.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(CollectionChanged);

			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandler);
			Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
			Title += " [" + System.Reflection.Assembly.GetExecutingAssembly().FullName.Split(",".ToCharArray())[1].Split("=".ToCharArray())[1] + "]";

      Log log = new Log();
      Manager.instance.Configuration = Configuration.LoadConfiguration(Manager.CONFIGURATION_FILENAME, log);
      if (log.CountByType(EnumLogData.Error) > 0)
        MessageBox.Show(log.ToString(), "Error loading configuration", MessageBoxButton.OK, MessageBoxImage.Error);
      else
      {
        Configure_ConcertlistDataFormat.LoadConfiguration();
        Configure_AdministeredPublishers.Init();
      }

			//if (Properties.Settings.Default.CallUpgrade)
			//{
			//	Properties.Settings.Default.Upgrade();
			//	Properties.Settings.Default.CallUpgrade = false;
			//}

			////Settings.Default.Upgrade();
			//LoadPublishers();
		}

		private void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
		{
			logging.Logger.instance.ErrorRoutine((Exception)e.ExceptionObject, "");
			MessageBox.Show("Ein unerwarteter Fehler ist aufgetreten. Bitte benachrichtige den Entwickler! Das Fehlerlog wurde in der Datei CheckGEMAPayoff_ErrorLog.txt gespeichert." + Environment.NewLine + "Fehlerbeschreibung: " + ((Exception)e.ExceptionObject).Message + Environment.NewLine + "Ausgelöst von: " + ((Exception)e.ExceptionObject).TargetSite.Name, "Fehler!", MessageBoxButton.OK, MessageBoxImage.Error);
		}

		//private void LoadPublishers()
		//{
		//	string[] p = Settings.Default.AdministeredPublisher.Split(Manager.instance.PublisherDataSep.ToCharArray());

		//	foreach (string item in p)
		//		Manager.instance.Publisher2Check.Add(item);
		//}

		#region UserInputCheck
		private void CheckUserInput()
		{
			ToolTip tooltip = new ToolTip();
			tabItemCheck.ToolTip = tooltip;

			bool showDataOk = true;
			if (Manager.instance.ArtistInfo.Count == 0)
				showDataOk = false;
			else
				foreach(ArtistInfo aInfo in Manager.instance.ArtistInfo)
					if (aInfo.ErrorCount > 0)
					{
						showDataOk = false;
						break;
					}

			//bool managedWorksOk = true;
			//if (Manager.instance.ManagedWorks.Count == 0)
			//  managedWorksOk = false;

			bool gemaDataOk = true;
			if (GEMAData.ErrorLog.Count > 0 || Manager.instance.AccountingSet.Count == 0)
				gemaDataOk = false;

			// if (showDataOk && managedWorksOk && gemaDataOk)
			if (showDataOk && gemaDataOk)
				{
				tabItemCheck.IsEnabled = true;
				tooltip.Content = "Alle Informationen ohne Fehler geladen." + Environment.NewLine + "Die Auswertung kann beginnen!";
			}
			else
			{
				tabItemCheck.IsEnabled = false;
				string toolTipText = String.Empty;
				if (!showDataOk)
					toolTipText += "Die Showdaten wurden nicht korrekt geladen." + Environment.NewLine;

				//if (!managedWorksOk)
				//  toolTipText += "Die Daten der verwalteten Werke wuden nicht korrekt geladen." + Environment.NewLine;

				if (!gemaDataOk)
					toolTipText += "Die GEMA-Abrechnungsdaten wurden nicht korrekt geladen." + Environment.NewLine;

				tooltip.Content = toolTipText + "Alle Daten müssen korrekt geladen werden, bevor eine Auswertung stattfinden kann.";
			}
		}

		private void CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			CheckUserInput();
		}
		#endregion

	}
}
