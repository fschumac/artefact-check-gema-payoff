using System;

using System.IO;

namespace logging
{
	/// <summary>
	/// Einfache Klasse zum Schreiben von Fehler- oder Statusmeldungen in ein Logfile
	/// </summary>
	public sealed class Logger
	{
		private static volatile Logger _instance;
		private static object syncRoot = new Object();

		private Logger()
		{
		}

		public static Logger instance
		{
			get
			{
				if (_instance == null)
				{
					lock (syncRoot)
					{
						if (_instance == null)
							_instance = new Logger();
					}
				}

				return _instance;
			}
		}

		public bool ErrorRoutine(Exception objException)
		{
			return ErrorRoutine(objException, "");
		}

		public bool ErrorRoutine(Exception objException, string additionalInfo)
		{
			try
			{
				string logFileName = "CheckGEMAPayoff_ErrorLog.txt";

				if (!File.Exists(logFileName))
				{
					FileStream fs = new FileStream(logFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
					fs.Close();
				}

				StreamWriter sw = new StreamWriter(logFileName, true);
				sw.WriteLine("Error       : " +  
					objException.Message.ToString().Trim());
				sw.WriteLine("Date        : " + 
					DateTime.Now.ToShortDateString());
				sw.WriteLine("Time        : " + 
					DateTime.Now.ToLongTimeString());
				sw.WriteLine("Methode     : " +
					objException.TargetSite.Name);
				if (additionalInfo != "")
					sw.WriteLine("Zusatzinfo  : " +  
						additionalInfo);
				sw.WriteLine("Stack Trace : " + 
					objException.StackTrace.ToString().Trim());
				sw.WriteLine(@"-----------------------------------------------------------------------");
				sw.Flush();
				sw.Close();

				if (objException.InnerException != null)
					ErrorRoutine(objException.InnerException, "InnerException");

				return true;
			}
			catch(Exception)
			{
				return false;
			}
		}

		public bool StatusRoutine(string statusInfo)
		{
			return StatusRoutine(statusInfo, "Status");
		}

		public bool StatusRoutine(string statusInfo, string title)
		{
			try
			{
				string logFileName = "CheckGEMAPayoff_ " + title + "_Log.txt";

				if (!File.Exists(logFileName))
				{
					FileStream fs = new FileStream(logFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
					fs.Close();
				}

				StreamWriter sw = new StreamWriter(logFileName, true);
				sw.Write(DateTime.Now.ToShortDateString() + " ");
				sw.Write(DateTime.Now.ToLongTimeString() + ": ");
				sw.WriteLine(statusInfo);
				sw.Flush();
				sw.Close();

				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public Exception GetInnerException(Exception ex)
		{
			Exception result = ex;

			while (result.InnerException != null)
				result = result.InnerException;

			return result;
		}

	}
}
