﻿using MBOClassRepresentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckGEMAPayoff
{
  public class DataModel
  {
    public DataModel()
    {
      Artists = new ObservableCollection<Artist>();
      LiveMusicEvents = new ObservableCollection<LiveMusicEvent>();
      _venues = new HashSet<Venue>();
    }

    public ObservableCollection<Artist> Artists { get; private set; }
    public ObservableCollection<LiveMusicEvent> LiveMusicEvents { get; private set; }
    private HashSet<Venue> _venues;

    public Venue GetVenue(string venueName, string postalCode, string city)
    {
      Venue result = _venues.FirstOrDefault(x => x.Names.Contains(venueName.Trim(), StringComparer.OrdinalIgnoreCase) && x.Postalcode.Equals(postalCode.Trim()) && x.City.Equals(city.Trim()));
      if (result == null)
      {
        result = new Venue(venueName.Trim());
        result.Postalcode = postalCode.Trim();
        result.City = city.Trim();

        _venues.Add(result);
      }

      return result;
    }

    public Artist GetArtist(string name)
    {
      Artist result = Artists.FirstOrDefault(x => x.Names.Contains(name.Trim(), StringComparer.OrdinalIgnoreCase));
      if (result == null)
      {
        result = new Artist(name.Trim());

        Artists.Add(result);
      }

      return result;
    }
  }
}
