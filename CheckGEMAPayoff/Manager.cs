﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CheckGEMAPayoff.Classes;
using System.Collections.ObjectModel;
using CheckGEMAPayoff.GEMA_Accounting;
using System.IO;
using System.Windows;
using CheckGEMAPayoff.Check;
using Microsoft.Win32;
using CheckGEMAPayoff.Export;
using DevFsFunctions.Logging;

namespace CheckGEMAPayoff
{
	class Manager
	{
		#region Singleton
		private static volatile Manager _instance;
		private static object syncRoot = new Object();

		private Manager()
		{
			_artistInfo = new ObservableCollection<ArtistInfo>();
			_managedWorks = new ObservableCollection<Work>();

			_AccountingSet = new ObservableCollection<Gig>();
			_reported_Gigs = new ObservableCollection<ReportedGig>();

			_checkGigs = new CheckGigs();
      _data = new DataModel();
		}

		public static Manager instance
		{
			get
			{
				if (_instance == null)
				{
					lock (syncRoot)
					{
						if (_instance == null)
							_instance = new Manager();
					}
				}

				return _instance;
			}
		}
    #endregion

    #region Data
    public static string CONFIGURATION_FILENAME = "config.xml";

		private ObservableCollection<ArtistInfo> _artistInfo;
		public ObservableCollection<ArtistInfo> ArtistInfo
		{
			get { return _artistInfo; }
		}

		private ObservableCollection<Work> _managedWorks;
		public ObservableCollection<Work> ManagedWorks
		{
			get { return _managedWorks; }
		}

		private ObservableCollection<Gig> _AccountingSet;
		public ObservableCollection<Gig> AccountingSet
		{
			get { return _AccountingSet; }
			set { _AccountingSet = value; }
		}

		private ObservableCollection<ReportedGig> _reported_Gigs;
		public ObservableCollection<ReportedGig> Reported_Gigs
		{
			get { return _reported_Gigs; }
			set { _reported_Gigs = value; }
		}

		private CheckGigs _checkGigs;
		public CheckGigs CheckedGigs
		{
			get { return _checkGigs; }
			set { _checkGigs = value; }
		}

		//private ObservableCollection<String> _publisher2Check;
		//public ObservableCollection<String> Publisher2Check
		//{
		//	get { return _publisher2Check; }
		//}

    private Configuration _configuration;
    public Configuration Configuration
    {
      get { return _configuration; }
      set { _configuration = value; }
    }

    private DataModel _data;
    public DataModel Data
    {
      get { return _data; }
    }

		//public ObservableCollection<ReportedGig> ReportedButNotAccountedGigs
		//{
		//  get { return _checkGigs.ReportedButNotAccountedGigs; }
		//}

		//public ObservableCollection<ReportedGig> NotCompletelyAccountedGigs
		//{
		//  get { return _checkGigs.NotCompletelyAccountedGigs; }
		//}

		//public ObservableCollection<ReportedGig> CorrectAccounted
		//{
		//  get { return _checkGigs.CorrectAccounted; }
		//}

		//public ObservableCollection<ReportedGig> WrongCategory
		//{
		//  get { return _checkGigs.WrongCategory; }
		//}
		#endregion

		#region ListRequests
		//internal GEMA_PayoffSet GetGEMAPayoffSet(int idRoot)
		//{
		//  GEMA_PayoffSet result = (from p in _GEMAPayoffSet	where p.id == idRoot select p).FirstOrDefault();

		//  return result;
		//}

		//internal GEMA_Beneficiary GetGEMABeneficiary(int idRoot)
		//{
		//  GEMA_Beneficiary result = (from p in _GEMA_Beneficiary where p.id == idRoot select p).FirstOrDefault();

		//  return result;
		//}

		//internal GEMA_WorkIdentification GetGEMAWorkIdentification(int idRoot)
		//{
		//  GEMA_WorkIdentification result = (from p in _GEMAWorkIdentification where p.ID == idRoot select p).FirstOrDefault();

		//  return result;
		//}

		internal ArtistInfo GetArtistInfo(string artistName)
		{
			ArtistInfo result = (from p in _artistInfo where p.Name == artistName select p).FirstOrDefault();

			return result;
		}

		//internal List<WorkUsage> AllReportedSongs()
		//{
		//  List<WorkUsage> resultSet = new List<WorkUsage>();

		//  foreach (ArtistInfo artistInfo in this.ArtistInfo)
		//  {
		//    foreach (ReportedGig showData in artistInfo.Shows)
		//    {
		//      foreach (Work work in artistInfo.Tracklists[showData.TrackListId])
		//      {
		//        if ((from p in Manager.instance.ManagedWorks where p.WorkNumber == work.WorkNumber select p).FirstOrDefault() != null)
		//        {
		//          WorkUsage workUsage = new WorkUsage();

		//          workUsage.Work = work;
		//          workUsage.Gig = showData;
		//          workUsage.Artist = artistInfo.Name;

		//          //workUsage.Work.WorkNumber = work.WorkNumber;
		//          //workUsage.Gig.DayOfShow = showData.DayOfShow;
		//          //workUsage.Gig.Venue = showData.Venue;
		//          //workUsage.Work.Title = work.Title;
		//          //workUsage.Work.Composer = work.Composer;
		//          //workUsage.Gig.Category = showData.Category;

		//          resultSet.Add(workUsage);
		//        }
		//      }
		//    }
		//  }

		//  resultSet.Sort();
		//  return resultSet;
		//}

		//internal List<WorkUsage> AllAccountedSongs(int year)
		//{
		//  List<WorkUsage> resultSet = new List<WorkUsage>();

		//  foreach (Gig gig in this.GEMA_Accounting)
		//  {
		//    foreach (Work work in gig.Works)
		//    {
		//      // only include usages in the given year
		//      if (gig.DayOfShow.Year != year)
		//        continue;

		//      // if the track usage is already listed (worknumber, date, plzvenue), just add the usage
		//      WorkUsage existingUsage = (from p in resultSet where p.Work.WorkNumber == work.WorkNumber && p.Gig.DayOfShow == gig.DayOfShow && p.Gig.Venue.Plz == gig.Venue.Plz select p).FirstOrDefault();

		//      if (existingUsage == null)
		//      {
		//        WorkUsage workUsage = new WorkUsage();

		//        workUsage.Work = work;
		//        workUsage.Gig = gig;

		//        resultSet.Add(workUsage);
		//      }
		//      //if (existingUsage != null)
		//      //{
		//      //  existingUsage.Gig.Usages.Add(usage);
		//      //}
		//      //else
		//      //{
		//      //  WorkUsage workUsage = new WorkUsage();


		//      //  workUsage.WorkNumber = usage.WorkIdentification.WorkNumber;
		//      //  workUsage.Gig.DayOfShow = usage.DayOfShow;
		//      //  workUsage.Gig.Venue = usage.Venue;
		//      //  workUsage.Gig.Category = usage.Category;
		//      //  workUsage.Composer = usage.WorkIdentification.Composer;
		//      //  workUsage.Title = usage.WorkIdentification.Title;
		//      //  workUsage.Gig.Usages.Add(usage);

		//      //  resultSet.Add(workUsage);
		//      //}
		//    }
		//  }

		//  resultSet.Sort();
		//  return resultSet;
		//}

		//internal ObservableCollection<CheckAccounting> Evaluate(int year)
		//{
		//  ObservableCollection<CheckAccounting> resultSet = new ObservableCollection<CheckAccounting>();

		//  List<WorkUsage> reportedUsage = Manager.instance.AllReportedSongs();
		//  List<WorkUsage> accountedUsage = Manager.instance.AllAccountedSongs(year);

		//  foreach (WorkUsage u in reportedUsage)
		//    Console.WriteLine(u.Work.WorkNumber);

		//  ComparerWorkUsageComplete cComplete = new ComparerWorkUsageComplete();
		//  ComparerWorkUsageIgnoreCat cIgnoreCat = new ComparerWorkUsageIgnoreCat();

		//  int curIndexReportedUsage = 0;
		//  int curIndexAccountedUsage = 0;

		//  while (curIndexReportedUsage < reportedUsage.Count || curIndexAccountedUsage < accountedUsage.Count)
		//  {
		//    WorkUsage curItemReportedUsage = curIndexReportedUsage < reportedUsage.Count ? reportedUsage[curIndexReportedUsage] : null;
		//    WorkUsage curItemAccountedUsage = curIndexAccountedUsage < accountedUsage.Count ? accountedUsage[curIndexAccountedUsage] : null;

		//    if (curItemAccountedUsage == null)
		//    {
		//      // Abgerechnete Liste durch, Gemeldete Liste noch Daten => Daten wurden nicht abgerechnet
		//      CheckAccounting checkGig = (from p in resultSet where p.DayOfShow == curItemReportedUsage.Gig.DayOfShow && p.Venue.Plz == curItemReportedUsage.Gig.Venue.Plz select p).FirstOrDefault();
		//      if (checkGig == null)
		//      {
		//        checkGig = new CheckAccounting(curItemReportedUsage.Gig) { Artist = curItemReportedUsage.Artist };
		//        resultSet.Add(checkGig);
		//      }
		//      checkGig.Works.Add(new WorkAccountingCheckItem(curItemReportedUsage.Work, AccountingState.notAccounted));
		//      curIndexReportedUsage++;
		//      continue;
		//    }

		//    if (curItemReportedUsage == null)
		//    {
		//      // Gemeldet Liste durch, Abgerechnet Liste sind noch Daten => Daten wurden nicht gemeldet
		//      CheckAccounting checkGig = (from p in resultSet where p.DayOfShow == curItemAccountedUsage.Gig.DayOfShow && p.Venue.Plz == curItemAccountedUsage.Gig.Venue.Plz select p).FirstOrDefault();
		//      if (checkGig == null)
		//      {
		//        checkGig = new CheckAccounting(curItemAccountedUsage.Gig);
		//        resultSet.Add(checkGig);
		//      }
		//      checkGig.Works.Add(new WorkAccountingCheckItem(curItemAccountedUsage.Work, AccountingState.notReported));
		//      curIndexAccountedUsage++;
		//      continue;
		//    }

		//    if (cIgnoreCat.Compare(curItemReportedUsage, curItemAccountedUsage) == 1)
		//    {
		//      //report > account => Report is missing
		//      CheckAccounting checkGig = (from p in resultSet where p.DayOfShow == curItemAccountedUsage.Gig.DayOfShow && p.Venue.Plz == curItemAccountedUsage.Gig.Venue.Plz select p).FirstOrDefault();
		//      if (checkGig == null)
		//      {
		//        checkGig = new CheckAccounting(curItemAccountedUsage.Gig);
		//        resultSet.Add(checkGig);
		//      }
		//      checkGig.Works.Add(new WorkAccountingCheckItem(curItemAccountedUsage.Work, AccountingState.notReported));
		//      curIndexAccountedUsage++;
		//    }
		//    else if (cIgnoreCat.Compare(curItemReportedUsage, curItemAccountedUsage) == -1)
		//    {
		//      // account > report => Account is missing
		//      CheckAccounting checkGig = (from p in resultSet where p.DayOfShow == curItemReportedUsage.Gig.DayOfShow && p.Venue.Plz == curItemReportedUsage.Gig.Venue.Plz select p).FirstOrDefault();
		//      if (checkGig == null)
		//      {
		//        checkGig = new CheckAccounting(curItemReportedUsage.Gig) { Artist = curItemReportedUsage.Artist };
		//        resultSet.Add(checkGig);
		//      }
		//      checkGig.Works.Add(new WorkAccountingCheckItem(curItemReportedUsage.Work, AccountingState.notAccounted));
		//      curIndexReportedUsage++;
		//    }
		//    else
		//    {
		//      // equal => check for Category
		//      if (cComplete.Compare(curItemReportedUsage, curItemAccountedUsage) == 0)
		//      {
		//        // correct
		//        CheckAccounting checkGig = (from p in resultSet where p.DayOfShow == curItemAccountedUsage.Gig.DayOfShow && p.Venue.Plz == curItemAccountedUsage.Gig.Venue.Plz select p).FirstOrDefault();
		//        if (checkGig == null)
		//        {
		//          checkGig = new CheckAccounting(curItemReportedUsage.Gig) { Artist = curItemReportedUsage.Artist };
		//          resultSet.Add(checkGig);
		//        }
		//        checkGig.Works.Add(new WorkAccountingCheckItem(curItemAccountedUsage.Work, AccountingState.correct));
		//      }
		//      else
		//      {
		//        // wrong Category
		//        CheckAccounting checkGig = (from p in resultSet where p.DayOfShow == curItemAccountedUsage.Gig.DayOfShow && p.Venue.Plz == curItemAccountedUsage.Gig.Venue.Plz select p).FirstOrDefault();
		//        if (checkGig == null)
		//        {
		//          checkGig = new CheckAccounting(curItemAccountedUsage.Gig) { Artist = curItemReportedUsage.Artist };
		//          resultSet.Add(checkGig);
		//        }
		//        checkGig.Works.Add(new WorkAccountingCheckItem(curItemReportedUsage.Work, AccountingState.wrongCategory));
		//      }
		//      curIndexAccountedUsage++;
		//      curIndexReportedUsage++;
		//    }
		//  }

		//  return resultSet;
		//  //CheckGigs checkGigs = new CheckGigs();
		//  //checkGigs.CreateAccountedGigsList();
		//  //checkGigs.CheckGigList();
		//  //dataGridReportedButNotAccountedGigs.DataContext = checkGigs.ReportedButNotAccountedGigs;
		//  //dataGridNotCompletelyAccountedGigs.DataContext = checkGigs.NotCompletelyAccountedGigs;
		//}

		#endregion

		#region Helper
		public void SelectTab(System.Windows.Controls.TabControl tabControl, string tabName)
		{
			foreach (System.Windows.Controls.TabItem tabItem in tabControl.Items)
			{
				if (tabItem.Name == tabName)
				{
					//tabControl.SelectedItem = tabItem;
					tabItem.IsSelected = true;
					break;
				}
			}

		}

		public bool IsInbetween(DateTime date2check, DateTime lowerBorder, DateTime upperBorder)
		{
			if (upperBorder == DateTime.MinValue)
				if (date2check.Date == lowerBorder.Date)
					return true;
				else
					return false;

			if (lowerBorder.Date <= date2check.Date && date2check.Date <= upperBorder.Date)
				return true;
			else
				return false;
		}

		#endregion

		internal IGEMA_Accounting_Importer GetImporterGEMA(StreamReader sr)
		{
			ObservableCollection<IGEMA_Accounting_Importer> resultSet = new ObservableCollection<IGEMA_Accounting_Importer>();

			var type = typeof(IGEMA_Accounting_Importer);
			var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(p => type.IsAssignableFrom(p) && p.IsClass);
			foreach (Type mytype in types)
			{
				IGEMA_Accounting_Importer GEMA_Importer = (IGEMA_Accounting_Importer)Activator.CreateInstance(mytype);

				// get the line, the Importer wants to check
				string firstLine = sr.ReadLine();
				int currentLine = 1;
				while (currentLine++ < GEMA_Importer.lineNumber4FormatCheck)
					firstLine = sr.ReadLine();

				if (GEMA_Importer.FormatCheck(firstLine))
					resultSet.Add(GEMA_Importer);

				sr.BaseStream.Position = 0;
				sr.DiscardBufferedData();
			}

			if (resultSet.Count == 0)
			{
				System.Windows.MessageBox.Show("Für das Format der GEMA - Abrechnung wurde kein Import-Modul gefunden." + Environment.NewLine + "Bitte stelle sicher, dass du die Datei, die du von der GEMA bekommen hast, nicht verändert hast!", "GEMA Abrechnung wurde nicht erkannt!", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
				return null;
			}
			else if (resultSet.Count == 1)
				return resultSet[0];
			else
			{
				MessageBox.Show("Für die GEMA-Datei wurde mehr als ein möglicher Importer gefunden.");
				return resultSet[0];
			}

		}

		public void InitReportedGigsList()
		{
			_reported_Gigs.Clear();

			foreach (ArtistInfo a in this.ArtistInfo)
			{
				foreach (ReportedGig gig in a.Shows)
				{
					ReportedGig reformattedGig = (from p in _reported_Gigs where p.DayOfShow == gig.DayOfShow && p.Venue.Postalcode == gig.Venue.Postalcode && p.Venue.Name == gig.Venue.Name && p.ArtistName == gig.ArtistName select p).FirstOrDefault();

					if (reformattedGig == null)
					{
						// => Gig noch nicht in der Liste => Hinzufügen
						reformattedGig = new ReportedGig();
						reformattedGig.ArtistName = a.Name;
						reformattedGig.DayOfShow = gig.DayOfShow;
						reformattedGig.Category = gig.Category;
						reformattedGig.Venue = gig.Venue;
						reformattedGig.Organiser = gig.Organiser;
						reformattedGig.DayOfTransmission = gig.DayOfTransmission;
						reformattedGig.OnlineTransmission = gig.OnlineTransmission;
						reformattedGig.TrackListName = gig.TrackListName;
						reformattedGig.rowNumber = gig.rowNumber;
            reformattedGig.EventName = gig.EventName;

						_reported_Gigs.Add(reformattedGig);
					}

					// Werke hinzufügen
					foreach (Work work in gig.Works)
						if (!reformattedGig.Works.Contains(work))
							reformattedGig.Works.Add(work);
				}
			}
		}

		public void Check()
		{
			_checkGigs.Go(ManagedWorks, Reported_Gigs, AccountingSet);
		}

		public void ExportChecks(IEnumerable<ReportedGig> notAccounted, IEnumerable<ReportedGig> incomplete, IEnumerable<ReportedGig> wrongCategory, IEnumerable<ReportedGig> correct, IEnumerable<Gig> accountedNotReported, int formatNumber, Log log)
		{
			SaveFileDialog sfd = new SaveFileDialog();
			sfd.AddExtension = true;
      if (formatNumber == 2)
      {
        sfd.DefaultExt = "xlsx";
        sfd.Filter = "Excel files (*.xlsx)|*.xlsx";
      }
      else
      {
        sfd.DefaultExt = "csv";
        sfd.Filter = "csv files (*.csv)|*.csv";
      }

			if (sfd.ShowDialog() == true)
			{
				if (File.Exists(sfd.FileName))
					if (MessageBox.Show("Die Datei '" + sfd.FileName + "' exisitiert bereits" + Environment.NewLine + "Willst du sie überschreiben?", "Reklamationsliste speichern", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.No)
						return;

        IExport exportModul = null;
        if (formatNumber == 1)
          exportModul = new StandardExport();
        else if (formatNumber == 0)
          exportModul = new GEMA2017Export();
        else if (formatNumber == 2)
          exportModul = new GEMA2017XLSX_Export();

        if (exportModul != null)
          exportModul.Do(notAccounted, incomplete, sfd.FileName, log);
        else
          MessageBox.Show("No import modul defined for id [" + formatNumber.ToString() + "].", "Export NOT successfull", MessageBoxButton.OK, MessageBoxImage.Error);

				//using (StreamWriter writer = new StreamWriter(sfd.FileName, false, Encoding.UTF8))
				//{
				//	// write Header
				//	string seperator = ";";
				//	writer.WriteLine("Abgerechnete Kategorie" + seperator + "Datum VA" + seperator + "Location" + seperator + "Organiser" + seperator + "Venue" + seperator + "Artist" + seperator + "Composer" + seperator + "Werktitel" + seperator + "Werknummer" + seperator + "Werkversion" + seperator + "Einreichungstag" + seperator + "Einreichungsart" + seperator + "Fehlerbeschreibung");

				//	// Komplett fehlende Gigs
				//	//foreach (ReportedGig gig in CheckedGigs.ReportedButNotAccountedGigs)
				//	foreach (ReportedGig gig in notAccounted)
				//	{
				//		foreach (Work work in gig.Works)
				//		{
				//			writer.WriteLine(gig.Category + seperator + gig.DayOfShow.ToShortDateString() + seperator + gig.Venue.Name + seperator + gig.Organiser.Name + ", " + gig.Organiser.Postalcode + " " + gig.Organiser.City + ", " + gig.Organiser.Street + seperator + gig.Venue.Postalcode + " " + gig.Venue.City + seperator + gig.ArtistName + seperator + work.Composer + seperator + work.Title + seperator + work.WorkNumber.ToString() + seperator + work.Version.ToString() + seperator + gig.DayOfTransmission.ToShortDateString() + seperator + (gig.OnlineTransmission ? "Online" : "Papier") + seperator + "Kompletter Gig nicht abgerechnet");
				//		}

				//		// Seperator
				//		writer.WriteLine(seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator);
				//	}

				//	// Unvollständig abgerechnete
				//	// foreach (ReportedGig gig in CheckedGigs.NotCompletelyAccountedGigs)
				//	foreach (ReportedGig gig in incomplete)
				//	{
				//		foreach (Work work in gig.Works)
				//		{
				//			writer.WriteLine(gig.Category + seperator + gig.DayOfShow.ToShortDateString() + seperator + gig.Venue.Name + seperator + gig.Organiser.Name + ", " + gig.Organiser.Postalcode + " " + gig.Organiser.City + ", " + gig.Organiser.Street + seperator + gig.Venue.Postalcode + " " + gig.Venue.City + seperator + gig.ArtistName + seperator + work.Composer + seperator + work.Title + seperator + work.WorkNumber.ToString() + seperator + work.Version.ToString() + seperator + gig.DayOfTransmission.ToShortDateString() + seperator + (gig.OnlineTransmission ? "Online" : "Papier") + seperator + "Werk fehlt in Abrechnung");
				//		}

				//		writer.WriteLine(seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator);
				//	}

				//	// Falsche Kategorie
				//	//foreach (ReportedGig gig in CheckedGigs.WrongCategory)
				//	foreach (ReportedGig gig in wrongCategory)
				//	{
				//		foreach (Work work in gig.Works)
				//		{
				//			writer.WriteLine(gig.Category + seperator + gig.DayOfShow.ToShortDateString() + seperator + gig.Venue.Name + seperator + gig.Organiser.Name + ", " + gig.Organiser.Postalcode + " " + gig.Organiser.City + ", " + gig.Organiser.Street + seperator + gig.Venue.Postalcode + " " + gig.Venue.City + seperator + gig.ArtistName + seperator + work.Composer + seperator + work.Title + seperator + work.WorkNumber.ToString() + seperator + work.Version.ToString() + seperator + gig.DayOfTransmission.ToShortDateString() + seperator + (gig.OnlineTransmission ? "Online" : "Papier") + seperator + "Werk in falscher Kategorie abgerechnet");
				//		}

				//		writer.WriteLine(seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator);
				//	}
				//}

				//MessageBox.Show("Reklamationsliste in Datei '" + sfd.FileName + "' gespeichert.");
			}
		}

		public void TriplyfyGemaData(IEnumerable<Gig> accountedGigs)
		{
		}

		public string EncodeNonAsciiCharacters(string value)
		{
			StringBuilder sb = new StringBuilder();
			foreach (char c in value)
			{
				if (c > 127)
				{
					// This character is too big for ASCII
					string encodedValue = "\\u" + ((int)c).ToString("x4");
					sb.Append(encodedValue);
				}
				else
				{
					sb.Append(c);
				}
			}
			return sb.ToString();
		}
	}
}
