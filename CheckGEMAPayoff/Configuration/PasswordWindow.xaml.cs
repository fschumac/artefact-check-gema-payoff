﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CheckGEMAPayoff.Properties;

namespace CheckGEMAPayoff
{
	/// <summary>
	/// Interaction logic for PasswordWindow.xaml
	/// </summary>
	public partial class PasswordWindow : Window
	{
		public PasswordWindow()
		{
			this.InitializeComponent();
			
			// Insert code required on object creation below this point.
			TextBlock_EndpointName.Text = Settings.Default["Endpoint_URL"].ToString();
			TextBlock_UserName.Text = Settings.Default["User_Name"].ToString();
		}

		private void Button_Ok_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			this.DialogResult = true;
		}

		private void Button_Cancel_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			this.DialogResult = false;
		}
	}
}