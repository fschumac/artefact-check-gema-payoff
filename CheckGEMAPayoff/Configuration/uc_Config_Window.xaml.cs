﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CheckGEMAPayoff.Configuration;
using System.Linq;

namespace CheckGEMAPayoff
{
	/// <summary>
	/// Interaction logic for uc_Config_Window.xaml
	/// </summary>
	public partial class uc_Config_Window : UserControl
	{
		private List<IConfigurationControl> _configurationControls;

		public uc_Config_Window()
		{
			this.InitializeComponent();

			_configurationControls = new List<IConfigurationControl>();
			_configurationControls.Add(Config_SparqlEndpoint);
		}

		private void Button_Save_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			foreach (IConfigurationControl ctrl in _configurationControls)
				ctrl.Save();

			Window parent = Window.GetWindow(this);
			parent.Close();
		}

		private void Button_Cancel_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			Window parent = Window.GetWindow(this);
			parent.Close();
		}
	}
}