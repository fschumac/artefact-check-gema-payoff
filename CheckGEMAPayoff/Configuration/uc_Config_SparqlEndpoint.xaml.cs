﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CheckGEMAPayoff.Configuration;
using CheckGEMAPayoff.Properties;

namespace CheckGEMAPayoff
{
	/// <summary>
	/// Interaction logic for uc_Config_SparqlEndpoint.xaml
	/// </summary>
	public partial class uc_Config_SparqlEndpoint : UserControl, IConfigurationControl
	{
		public uc_Config_SparqlEndpoint()
		{
			this.InitializeComponent();

			Properties.Settings.Default.Upgrade();

			if (Settings.Default["Endpoint_URL"] != null)
				TextBox_Endpoint_URL.Text = Settings.Default["Endpoint_URL"].ToString();
			if (Settings.Default["Graph_Name"] != null)
				TextBox_Graph_Name.Text = Settings.Default["Graph_Name"].ToString();
			if (Settings.Default["Local_Name"] != null)
				TextBox_Local_Name.Text = Settings.Default["Local_Name"].ToString();
			if (Settings.Default["User_Name"] != null)
				TextBox_User_Name.Text = Settings.Default["User_Name"].ToString();
		}

		public void Save()
		{
			Settings.Default["Endpoint_URL"] = TextBox_Endpoint_URL.Text;
			Settings.Default["Graph_Name"] = TextBox_Graph_Name.Text ;
			Settings.Default["Local_Name"] = TextBox_Local_Name.Text;
			Settings.Default["User_Name"] = TextBox_User_Name.Text;

			Settings.Default.Save();
		}
	}
}