﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheckGEMAPayoff.Configuration
{
	public class PasswordStore
	{
		#region Singleton
		private static volatile PasswordStore _instance;
		private static object syncRoot = new Object();

		private PasswordStore()
		{
			_isPasswordSet = false;
		}

		public static PasswordStore instance
		{
			get
			{
				if (_instance == null)
				{
					lock (syncRoot)
					{
						if (_instance == null)
							_instance = new PasswordStore();
					}
				}

				return _instance;
			}
		}
		#endregion

		#region Variables
		private string _password;
		public string Password
		{
			get	{	return _password; }
			set 
			{ 
				_password = value;
				_isPasswordSet = true;
			}
		}

		private bool _isPasswordSet;
		public bool IsPasswordSet
		{
			get { return _isPasswordSet; }
		}

		#endregion
	}
}
