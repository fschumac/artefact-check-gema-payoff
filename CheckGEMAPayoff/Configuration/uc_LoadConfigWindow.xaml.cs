﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckGEMAPayoff
{
	/// <summary>
	/// Interaction logic for uc_LoadConfigWindow.xaml
	/// </summary>
	public partial class uc_LoadConfigWindow : UserControl
	{
		public uc_LoadConfigWindow()
		{
			this.InitializeComponent();
		}

		private void TextBlock_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			ConfigurationWindow cWindow = new ConfigurationWindow();
			cWindow.ShowDialog();
		}

		private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
		{
				//ConfigurationWindow cWindow = new ConfigurationWindow();
				//cWindow.ShowDialog();
		}
	}
}