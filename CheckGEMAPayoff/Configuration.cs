﻿using DevFsExcel;
using DevFsFunctions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CheckGEMAPayoff
{
  public class Configuration
  {
    public string PathShowData { get; set; }
    public string PathGEMAFile { get; set; }
    public string PathManagedWorksFiles { get; set; }
    public int FirstDataRow { get; set; }
    public string Column_ShowDate { get; set; }
    public string Column_ShowTime { get; set; }
    public string Column_VenueName { get; set; }
    public string Column_VenueCity { get; set; }
    public string Column_VenueStreet { get; set; }
    public string Column_OrganiserName { get; set; }
    public string Column_OrganiserCity { get; set; }
    public string Column_OrganiserStreet { get; set; }
    public string Column_SetlistFileName { get; set; }
    public string Column_SubmissionType { get; set; }
    public string Column_SubmissionDate { get; set; }
    public string Column_Artist { get; set; }
    public string Column_EventType { get; set; }
    public string Column_EventName { get; set; }
    public string Column_Bandleader { get; set; }
    public string ConcertlistSheetName { get; set; }
    public List<string> AdministeredPublishers { get; set; }

    public Configuration()
    {
      AdministeredPublishers = new List<string>();
    }

    public static Configuration LoadConfiguration(string filename, Log log)
    {
      Configuration configuration = null;

      if (!File.Exists(filename))
      {
        log.Add("The configuration file [" + filename + "] could not be found. We initialized the configuration with default values", EnumLogData.Warning);
        configuration = GetDefaultConfiguration();
      }
      else
      {
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(Configuration));

        try
        {
          using (StreamReader streamReader = new StreamReader(filename))
            configuration = xmlSerializer.Deserialize(streamReader) as Configuration;
        }
        catch (Exception ex)
        {
          log.Add("Error reading configuration file [" + filename + "]: " + ex.GetBaseException().Message + Environment.NewLine + "Please resolve the problem before you continue.", EnumLogData.Error);
          configuration = null;
        }
      }

      return configuration;
    }

    public void SaveConfiguration(string filename, Log log)
    {
      XmlSerializer xmlSerializer = new XmlSerializer(typeof(Configuration));

      try
      {
        using (StreamWriter sw = new StreamWriter(filename))
          xmlSerializer.Serialize(sw, this);
      }
      catch (Exception ex)
      {
        log.Add("Error writing configuration to file [" + filename + "]. Your configuration has NOT been saved: " + ex.GetBaseException().Message, EnumLogData.Error);
      }
    }

    private static Configuration GetDefaultConfiguration()
    {
      Configuration configuration = new Configuration();

      configuration.AdministeredPublishers.Add("Kick The Flame Musikverlag Rajk Barthel");
      configuration.Column_Artist = "O";
      configuration.Column_Bandleader = "P";
      configuration.Column_EventType = "N";
      configuration.Column_EventName = "D";
      configuration.Column_OrganiserCity = "K";
      configuration.Column_OrganiserName = "I";
      configuration.Column_OrganiserStreet = "J";
      configuration.Column_SetlistFileName = "L";
      configuration.Column_ShowDate = "A";
      configuration.Column_ShowTime = "B";
      configuration.Column_SubmissionDate = "V";
      configuration.Column_SubmissionType = "U";
      configuration.Column_VenueCity = "G";
      configuration.Column_VenueName = "E";
      configuration.Column_VenueStreet = "F";
      configuration.ConcertlistSheetName = "Konzerte";
      configuration.FirstDataRow = 7;

      return configuration;
    }

    public bool CheckConfiguration(Log log)
    {
      int logCountBefore = log.Count;

      if (!ExcelFunctions.IsValidColumnString(Column_ShowTime, false))
        log.Add("Spalte ShowTime: [" + Column_ShowTime + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(Column_SubmissionDate, false))
        log.Add("Spalte Einreichungsdatum: [" + Column_SubmissionDate + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(Column_SubmissionType, false))
        log.Add("Spalte Einreichungsform: [" + Column_SubmissionType + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(Column_EventName, false))
        log.Add("Spalte Eventname: [" + Column_EventName + "] ist keine gültige Excelspalte");

      if (FirstDataRow <= 0)
        log.Add("Erste Zeile mit Daten: [" + FirstDataRow.ToString() + "] muss eine Zahl größer als Null sein.");

      if (!ExcelFunctions.IsValidColumnString(Column_OrganiserCity, false))
        log.Add("Spalte Veranstalter Ort: [" + Column_OrganiserCity + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(Column_OrganiserName, false))
        log.Add("Spalte Veranstaltername: [" + Column_OrganiserName + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(Column_OrganiserStreet, false))
        log.Add("Spalte Veranstalter Straße: [" + Column_OrganiserStreet + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(Column_ShowDate, false))
        log.Add("Spalte Aufführungstag: [" + Column_ShowDate + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(Column_SetlistFileName, false))
        log.Add("Spalte Titellistennummer: [" + Column_SetlistFileName + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(Column_VenueCity, false))
        log.Add("Spalte Aufführungsort: [" + Column_VenueCity + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(Column_VenueName, false))
        log.Add("Spalte Venue Name: [" + Column_VenueName + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(Column_VenueStreet, false))
        log.Add("Spalte Venue Straße: [" + Column_VenueStreet + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(Column_Artist, false))
        log.Add("Spalte Artist: [" + Column_Artist + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(Column_Bandleader, false))
        log.Add("Spalte Musikalischer Leiter: [" + Column_Bandleader + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(Column_EventType, false))
        log.Add("Spalte Art der Veranstaltung: [" + Column_EventType + "] ist keine gültige Excelspalte");

      if (log.Count > logCountBefore)
      {
        return false;
      }

      return true;
    }

  }
}
