﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using CheckGEMAPayoff.Classes;
using System.IO;

namespace CheckGEMAPayoff.GEMA_Accounting
{
	public class GEMA_Accounting_Importer_2012_till_2014 : IGEMA_Accounting_Importer
	{
		ObservableCollection<GEMA_PayoffSet> _payoffSet;
		ObservableCollection<GEMA_Beneficiary> _beneficiary;
		ObservableCollection<GEMA_WorkIdentification> _workIdentification;
		ObservableCollection<GEMA_Usage> _usage;

		private const string GEMA_CSV_SEPERATOR = ";";

		public GEMA_Accounting_Importer_2012_till_2014()
		{
			_payoffSet = new ObservableCollection<GEMA_PayoffSet>();
			_beneficiary = new ObservableCollection<GEMA_Beneficiary>();
			_workIdentification = new ObservableCollection<GEMA_WorkIdentification>();
			_usage = new ObservableCollection<GEMA_Usage>();
		}

		public bool FormatCheck(string firstLine)
		{
			// split the data
			string[] lineSplit = firstLine.Split(GEMA_CSV_SEPERATOR.ToCharArray());

			if (lineSplit.Length != 5)
			{
				return false;
			}

			if (lineSplit[0] != "01")
				return false;

			int id = -1;
			if (!Int32.TryParse(lineSplit[1], out id))
				return false;

			DateTime creationDate = ConvertGEMADateTime(lineSplit[3], new ObservableCollection<string>());
			if (creationDate == DateTime.MinValue)
				return false;

			return true;
		}

		public string Name
		{
			get { return "GEMA Abrechnung 2012 - 2014"; }
		}

		public string Description
		{
			get { return "Importer für die GEMA-Abrechnungen vom 01.04.2012 bis 01.04.2014" ; }
		}



		private void ReadLine(string line, int lineCount, ObservableCollection<string> errorLog)
		{
			if (_payoffSet == null || _beneficiary == null || _workIdentification == null || _usage == null)
				throw new Exception("Importer wurde nicht initialisiert!");

			// split the data
			string[] lineSplit = line.Split(GEMA_CSV_SEPERATOR.ToCharArray());

			if (lineSplit.Length > 0)
			{
				// depending on the set type
				switch (lineSplit[0])
				{
					case "01": ReadCategory(lineSplit, lineCount, errorLog);
						break;
					case "02": ReadMainAccount(lineSplit, lineCount, errorLog);
						break;
					case "03": ReadWorkIdentification(lineSplit, lineCount, errorLog);
						break;
					case "04": ReadUsageReport(lineSplit, lineCount, errorLog);
						break;
					default: 
						break;
				}
			}
		}

		private void ReadCategory(string[] lineSplit, int lineCount, ObservableCollection<string> errorLog)
		{
			if (lineSplit.Length != 5)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Es wurden 5 Felder erwartet, aber " + lineSplit.Length.ToString() + " gefunden.");
				return;
			}

			GEMA_PayoffSet data = new GEMA_PayoffSet();

			//id
			int id = -1;
			if (!Int32.TryParse(lineSplit[1], out id))
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID: " + lineSplit[1] + " ist keine Zahl.");
			}
			data.id = id;

			// sparte
			if (lineSplit[2].Length < 3)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Sparte: " + lineSplit[2] + " ist zu kurz.");
			}
			else
				data.Category = lineSplit[2].Substring(1, lineSplit[2].Length - 2);

			// datum
			DateTime creationDate = ConvertGEMADateTime(lineSplit[3], errorLog);
			if (creationDate == DateTime.MinValue)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Datum: " + lineSplit[3] + " konnte nicht gelesen werden.");
			}
			data.PayoffDate = creationDate;

			// bemerkung
			data.Description = lineSplit[4];

			_payoffSet.Add(data);
		}

		private void ReadMainAccount(string[] lineSplit, int lineCount, ObservableCollection<string> errorLog)
		{
			if (lineSplit.Length != 7)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Es wurden 7 Felder erwartet, aber " + lineSplit.Length.ToString() + " gefunden.");
				return;
			}

			GEMA_Beneficiary bene = new GEMA_Beneficiary();

			//id
			int id = -1;
			if (!Int32.TryParse(lineSplit[1], out id))
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID: " + lineSplit[1] + " ist keine Zahl.");
			}
			bene.id = id;

			//Überkonto
			int aboveAccount = -1;
			if (!Int32.TryParse(lineSplit[3], out aboveAccount))
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Überkontonummer: " + lineSplit[3] + " ist keine Zahl.");
			}
			bene.AboveAccountNumber = aboveAccount;

			//Hauptkontonummer
			int mainAccount = -1;
			if (!Int32.TryParse(lineSplit[4], out mainAccount))
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Hauptkontonummer: " + lineSplit[4] + " ist keine Zahl.");
			}
			bene.MainAccountNumber = mainAccount;

			// Hauptkontoinhaber
			if (lineSplit[5].Length < 3)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Hauptkontoinhaber: " + lineSplit[5] + " ist zu kurz.");
			}
			else
				bene.MainAccountOwner = lineSplit[5].Substring(1, lineSplit[5].Length - 2);

			// bemerkung
			bene.Description = lineSplit[6];

			// Zugehörige Abrechnung
			int idRoot = -1;
			if (!Int32.TryParse(lineSplit[2], out idRoot))
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID Satzart 01: " + lineSplit[2] + " ist keine Zahl.");
			}

			bene.PayoffSet = (from p in _payoffSet	where p.id == idRoot select p).FirstOrDefault();

			_beneficiary.Add(bene);
		}

		private void ReadWorkIdentification(string[] lineSplit, int lineCount, ObservableCollection<string> errorLog)
		{
			if (lineSplit.Length != 8)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Es wurden 8 Felder erwartet, aber " + lineSplit.Length.ToString() + " gefunden.");
				return;
			}

			GEMA_WorkIdentification wi = new GEMA_WorkIdentification();

			//id
			int id = -1;
			if (!Int32.TryParse(lineSplit[1], out id))
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID: " + lineSplit[1] + " ist keine Zahl.");
			}
			wi.ID = id;

			//werknummer
			int worknumber = -1;
			if (!Int32.TryParse(lineSplit[3], out worknumber))
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Werknummer: " + lineSplit[3] + " ist keine Zahl.");
			}
			wi.WorkNumber = worknumber;

			//fassungsnummer
			int copynumber = -1;
			if (!Int32.TryParse(lineSplit[4], out copynumber))
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Fassungsnummer: " + lineSplit[4] + " ist keine Zahl.");
			}
			wi.Version = copynumber;

			// Werktitel
			if (lineSplit[5].Length < 3)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Werktitel: " + lineSplit[5] + " ist zu kurz.");
			}
			else
				wi.Title = lineSplit[5].Substring(1, lineSplit[5].Length - 2);

			// Komponist
			if (lineSplit[6].Length < 3)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Komponist: " + lineSplit[6] + " ist zu kurz.");
			}
			else
				wi.Composer = lineSplit[6].Substring(1, lineSplit[6].Length - 2);

			// Editor
			if (lineSplit[5].Length < 2)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Bearbeiter: " + lineSplit[7] + " ist zu kurz.");
			}
			else if (lineSplit[5].Length > 2)
				wi.Editor = lineSplit[7].Substring(1, lineSplit[7].Length - 2);

			// Zugehöriger Berechtigter
			int idRoot = -1;
			if (!Int32.TryParse(lineSplit[2], out idRoot))
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID Satzart 02: " + lineSplit[2] + " ist keine Zahl.");
			}
			else
				wi.Beneficiary = (from p in _beneficiary where p.id == idRoot select p).FirstOrDefault();

			_workIdentification.Add(wi);
		}

		private void ReadUsageReport(string[] lineSplit, int lineCount, ObservableCollection<string> errorLog)
		{
			// only Category "U" implemented yet
			if (lineSplit.Length != 9)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Es wurden 9 Felder erwartet, aber " + lineSplit.Length.ToString() + " gefunden.");
				return;
			}

			if (lineSplit[3] != "\"U\"" && lineSplit[3] != "\"UD\"" && lineSplit[3] != "\"M\"")
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Bisher werden nur die Abrechnungsarten \"U\", \"UD\" und \"M\" unterstützt.");
				return;
			}

			GEMA_Usage usage = new GEMA_Usage();

			//id
			int id = -1;
			if (!Int32.TryParse(lineSplit[1], out id))
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID: " + lineSplit[1] + " ist keine Zahl.");
			}
			usage.id = id;

			// sparte
			if (lineSplit[3].Length < 3)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Sparte: " + lineSplit[3] + " ist zu kurz.");
			}
			else
				usage.Category = lineSplit[3].Substring(1, lineSplit[3].Length - 2);


			// datum
			DateTime showDate = ConvertGEMADate(lineSplit[4]);
			if (showDate == DateTime.MinValue)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Datum: " + lineSplit[4] + " konnte nicht gelesen werden.");
			}
			else
				usage.DayOfShow = showDate;

			// Venue
			if (lineSplit[6].Length < 3)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Ort: " + lineSplit[6] + " ist zu kurz.");
			}
			else
			{
				string[] tmp = lineSplit[6].Substring(1, lineSplit[6].Length - 2).Trim().Split(" ".ToCharArray());

				// check wheter we found a postal code. It has to contain at least one digit
				System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(tmp[0], @"\d");
				if (match.Success)
				{
					usage.Venue.Postalcode = tmp[0];

					StringBuilder sb = new StringBuilder();
					for (int i = 1; i < tmp.Length; i++)
						sb.Append(tmp[i] + " ");
					usage.Venue.City = sb.ToString().Trim();
				}
				else
					usage.Venue.City = lineSplit[6].Substring(1, lineSplit[6].Length - 2).Trim();
			}

			// VA-Raum
			if (lineSplit[7].Length >= 3)
				usage.Venue.Name = lineSplit[7].Substring(1, lineSplit[7].Length - 2);

			// VA-Raum
			if (lineSplit[8].Length >= 3)
				usage.Organiser.Name = lineSplit[8].Substring(1, lineSplit[8].Length - 2);

			// Zugehöriges Werk
			int idRoot = -1;
			if (!Int32.TryParse(lineSplit[2], out idRoot))
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID Satzart 03: " + lineSplit[2] + " ist keine Zahl.");
			}
			else
				usage.WorkIdentification = (from p in _workIdentification where p.ID == idRoot select p).FirstOrDefault();

			_usage.Add(usage);
		}

		#region Helper
		private DateTime ConvertGEMADateTime(string p, ObservableCollection<string> errorLog)
		{
			if (p.Length < 3)
				return DateTime.MinValue;

			string[] tmp = p.Substring(1, p.Length - 2).Split(" ".ToCharArray());

			if (tmp.Length != 2)
				return DateTime.MinValue;

			int year;
			if (!Int32.TryParse(tmp[0].Substring(0, 4), out year))
				return DateTime.MinValue;
			int day;
			if (!Int32.TryParse(tmp[0].Substring(4, 2), out day))
				return DateTime.MinValue;
			int month;
			if (!Int32.TryParse(tmp[0].Substring(6, 2), out month))
				return DateTime.MinValue;

			int hour;
			if (!Int32.TryParse(tmp[1].Substring(0, 2), out hour))
				return DateTime.MinValue;
			int minute;
			if (!Int32.TryParse(tmp[1].Substring(3, 2), out minute))
				return DateTime.MinValue;
			int second;
			if (!Int32.TryParse(tmp[1].Substring(6, 2), out second))
				return DateTime.MinValue;

			DateTime result = DateTime.MinValue;

			try
			{
				result = new DateTime(year, month, day, hour, minute, second);
			}
			catch (Exception ex)
			{
				errorLog.Add("Fehler bei der Datumskonvertierung: Die Zeichenkette '" + p + "' konnte nicht als Datum interpretiert werden: " + ex.Message);
			}

			return result;
		}

		private DateTime ConvertGEMADate(string p)
		{
			if (p.Length < 3)
				return DateTime.MinValue;

			int year;
			if (!Int32.TryParse(p.Substring(7, 4), out year))
				return DateTime.MinValue;
			int month;
			if (!Int32.TryParse(p.Substring(4, 2), out month))
				return DateTime.MinValue;
			int day;
			if (!Int32.TryParse(p.Substring(1, 2), out day))
				return DateTime.MinValue;

			return new DateTime(year, month, day);
		}

		#endregion





		public ObservableCollection<Gig> Get_GEMA_Accounting(ObservableCollection<string> infoLog, ObservableCollection<string> errorLog, StreamReader dataStream)
		{
			String line;
			int lineCount = 0;

			// Import the data into the internal format
			while ((line = dataStream.ReadLine()) != null)
			{
				lineCount++;
				ReadLine(line, lineCount, errorLog);
			}

			// write the data to the Gig-class-structure
			ObservableCollection<Gig> resultSet = new ObservableCollection<Gig>();

			foreach (GEMA_Usage usage in _usage)
			{
				// get the gig
				Gig gig = (from p in resultSet where p.DayOfShow == usage.DayOfShow && p.Venue.Postalcode == usage.Venue.Postalcode && p.Venue.Name == usage.Venue.Name select p).FirstOrDefault();

				if (gig == null)
				{
					// this gig is not in the list yet => create it
					gig = new Gig();

					gig.DayOfShow = usage.DayOfShow;
					gig.Venue.City = usage.Venue.City;
					gig.Venue.Postalcode = usage.Venue.Postalcode;
					gig.Venue.Name = usage.Venue.Name;
					gig.Organiser.Name = usage.Organiser.Name;
					gig.Category = usage.Category;

					resultSet.Add(gig);
				}

				// get the beneficiary
				Beneficiary bene = (from p in gig.Beneficiaries where p.MainAccountNumber == usage.WorkIdentification.Beneficiary.MainAccountNumber select p).FirstOrDefault();
				if (bene == null)
				{
					// new beneficiary => create it
					bene = new Beneficiary();

					bene.AboveAccountNumber = usage.WorkIdentification.Beneficiary.AboveAccountNumber;
					bene.MainAccountNumber = usage.WorkIdentification.Beneficiary.MainAccountNumber;
					bene.MainAccountOwner = usage.WorkIdentification.Beneficiary.MainAccountOwner;

					gig.Beneficiaries.Add(bene);
				}

				// if a usage is accounted for more than one beneficiary (i.e. Label and Artist), just use one Data
				if ((from p in gig.Works where p.WorkNumber == usage.WorkIdentification.WorkNumber select p).FirstOrDefault() == null)
					gig.Works.Add(usage.WorkIdentification);
			}

			return resultSet;
		}


		public int lineNumber4FormatCheck
		{
			get { return 1; }
		}
	}
}
