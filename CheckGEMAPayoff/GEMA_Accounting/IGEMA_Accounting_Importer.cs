﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using CheckGEMAPayoff.Classes;
using System.IO;

namespace CheckGEMAPayoff.GEMA_Accounting
{
	public interface IGEMA_Accounting_Importer
	{
		bool FormatCheck(string firstLine);
		int lineNumber4FormatCheck { get; }
		string Name { get; }
		string Description { get; }
		ObservableCollection<Gig> Get_GEMA_Accounting(ObservableCollection<string> infoLog, ObservableCollection<string> errorLog, StreamReader dataStream);
	}

	public interface IGEMA_Type
	{
	}
}
