﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using CheckGEMAPayoff.Classes;
using System.IO;

namespace CheckGEMAPayoff.GEMA_Accounting
{
	public class GEMA_Accounting_Importer_2014 : IGEMA_Accounting_Importer
	{
		private ObservableCollection<DataItem> _gema_accounting;

		private const string GEMA_CSV_SEPERATOR = ";";

		public GEMA_Accounting_Importer_2014()
		{
			_gema_accounting = new ObservableCollection<DataItem>();
		}

		public bool FormatCheck(string firstLine)
		{
			ObservableCollection<string> errorLog = new ObservableCollection<string>();
			ReadLine(firstLine, 0, errorLog);
			if (errorLog.Count == 0)
				return true;
			else
				return false;
		}

		public string Name
		{
			get { return "GEMA Abrechnung ab 2014"; }
		}

		public string Description
		{
			get { return "Importer für die GEMA-Abrechnungen ab dem 01.04.2014"; }
		}

		public ObservableCollection<Gig> Get_GEMA_Accounting(ObservableCollection<string> infoLog, ObservableCollection<string> errorLog, StreamReader dataStream)
		{
			String line;
			int lineCount = 1;
			_gema_accounting.Clear();

			// Read the data into the internal format
			// ignore the first line
			line = dataStream.ReadLine();
			while ((line = dataStream.ReadLine()) != null)
			{
				lineCount++;
				ReadLine(line, lineCount, errorLog);
			}

			// convert to GIG datatype
			ObservableCollection<Gig> resultSet = new ObservableCollection<Gig>();

			foreach (DataItem item in _gema_accounting)
			{
				Gig gig = (from p in resultSet where Manager.instance.IsInbetween(p.DayOfShow, item.Datum_von, item.Datum_bis) && p.Venue.Name == item.VARaum && item.Ort.Contains(p.Venue.Postalcode) select p).FirstOrDefault();

				if (gig == null)
				{
					gig = new Gig();
					gig.Category = item.Sparte;
					gig.DayOfShow = item.Datum_von;
					gig.DayOfShow_Last = item.Datum_bis;
					gig.Organiser.Name = item.Veranstalter;
					gig.Venue.Name = item.VARaum;
					string[] tmp = item.Ort.Split(" ".ToCharArray());
					gig.Venue.Postalcode = tmp[0];
					StringBuilder sb = new StringBuilder();
					for (int i = 1; i < tmp.Length; i++)
						sb.Append(tmp[i] + " ");
					gig.Venue.City = sb.ToString().Trim();

					resultSet.Add(gig);
				}

				Beneficiary bene = (from p in gig.Beneficiaries where p.MainAccountNumber == item.Hauptkontonummer select p).FirstOrDefault();
				if (bene == null)
				{
					bene = new Beneficiary();

					bene.AboveAccountNumber = item.Ueberkontonummer;
					bene.MainAccountNumber = item.Hauptkontonummer;
					bene.MainAccountOwner = item.HauptkontoInhaberName;

					gig.Beneficiaries.Add(bene);
				}

				if ((from p in gig.Works where p.WorkNumber == item.Werknummer select p).FirstOrDefault() == null)
				{
					Work work = new Work();

					work.Composer = item.KomponistName;
					work.Editor = item.Bearbeiter;
					work.Publisher = item.HauptkontoInhaberName;
					work.Title = item.Werktitel;
					work.Version = item.Werkversion;
					work.WorkNumber = item.Werknummer;

					gig.Works.Add(work);
				}
			}

			return resultSet;
		}

		private void ReadLine(string line, int lineCount, ObservableCollection<string> errorLog)
		{
			// split the data
			string[] lineSplit = line.Split(GEMA_CSV_SEPERATOR.ToCharArray());

			if (lineSplit.Length < 19)
			{
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Es wurden 18 Felder erwartet, aber " + lineSplit.Length.ToString() + " gefunden.");
				return;
			}

			DataItem item = new DataItem();
			int tmpInt = 0;
			DateTime tmpDate = DateTime.MinValue;

			item.Satzart = lineSplit[0].Trim(" \"".ToCharArray());
			if (!Int32.TryParse(lineSplit[1].Trim(" \"".ToCharArray()), out tmpInt))
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Überkontonummer: " + lineSplit[1].Trim(" \"".ToCharArray()) + " ist keine Zahl.");
			else
				item.Ueberkontonummer = tmpInt;
			if (!Int32.TryParse(lineSplit[2].Trim(" \"".ToCharArray()), out tmpInt))
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Hauptkontonummer: " + lineSplit[2].Trim(" \"".ToCharArray()) + " ist keine Zahl.");
			else
				item.Hauptkontonummer = tmpInt;
			item.HauptkontoInhaberName = lineSplit[3].Trim(" \"".ToCharArray());
			item.KomponistName = lineSplit[4].Trim(" \"".ToCharArray());
			item.Bearbeiter = lineSplit[5].Trim(" \"".ToCharArray());
			item.Werktitel = lineSplit[6].Trim(" \"".ToCharArray());
			if (!Int32.TryParse(lineSplit[7].Trim(" \"".ToCharArray()), out tmpInt))
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Werktitel: " + lineSplit[7].Trim(" \"".ToCharArray()) + " ist keine Zahl.");
			else
				item.Werknummer = tmpInt;
			if (!Int32.TryParse(lineSplit[8].Trim(" \"".ToCharArray()), out tmpInt))
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Fassungsnummer: " + lineSplit[8].Trim(" \"".ToCharArray()) + " ist keine Zahl.");
			else
				item.Werkversion = tmpInt;
			//if (!Int32.TryParse(lineSplit[9], out tmpInt))
			//  errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Segmentnummer: " + lineSplit[9] + " ist keine Zahl.");
			//else
			//  item.Segmentnummer = tmpInt;
			item.Segmentnummer = lineSplit[9].Trim(" \"".ToCharArray());
			item.Sparte = lineSplit[10].Trim(" \"".ToCharArray());
			item.Datum_von = ConvertGEMADate(lineSplit[11].Trim(" \"".ToCharArray()));
			if (item.Datum_von == DateTime.MinValue)
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Datum-von: " + lineSplit[11].Trim(" \"".ToCharArray()) + " ist kein Datum.");
			item.Datum_bis = ConvertGEMADate(lineSplit[12].Trim(" \"".ToCharArray()));
			if ((item.Datum_bis == DateTime.MinValue) && (lineSplit[12].Trim(" \"".ToCharArray()).Length > 2))
				errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Datum-bis: " + lineSplit[12].Trim(" \"".ToCharArray()) + " ist kein Datum.");
			item.Beginn = lineSplit[13].Trim(" \"".ToCharArray());
			item.Ort = lineSplit[14].Trim(" \"".ToCharArray());
			item.VARaum = lineSplit[15].Trim(" \"".ToCharArray());
			item.Veranstalter = lineSplit[16].Trim(" \"".ToCharArray());
			item.Abrechnung = lineSplit[17].Trim(" \"".ToCharArray());

			_gema_accounting.Add(item);
		}

		private DateTime ConvertGEMADate(string p)
		{
			string[] splitDate = p.Split(".".ToCharArray());

			if (splitDate.Length != 3)
				return DateTime.MinValue;

			int year;
			if (!Int32.TryParse(splitDate[2], out year))
				return DateTime.MinValue;
			int month;
			if (!Int32.TryParse(splitDate[1], out month))
				return DateTime.MinValue;
			int day;
			if (!Int32.TryParse(splitDate[0], out day))
				return DateTime.MinValue;

			return new DateTime(year, month, day);
		}

		public int lineNumber4FormatCheck
		{
			get { return 2; }
		}

	}

	internal class DataItem
	{
		public string Satzart { get; set; }
		public int Ueberkontonummer { get; set; }
		public int Hauptkontonummer { get; set; }
		public string HauptkontoInhaberName { get; set; }
		public string KomponistName { get; set; }
		public string Bearbeiter { get; set; }
		public string Werktitel { get; set; }
		public int Werknummer { get; set; }
		public int Werkversion { get; set; }
		public string Segmentnummer { get; set; }
		public string Sparte { get; set; }
		public DateTime Datum_von { get; set; }
		public DateTime Datum_bis { get; set; }
		public string Beginn { get; set; }
		public string Ort { get; set; }
		public string VARaum { get; set; }
		public string Veranstalter { get; set; }
		public string Abrechnung { get; set; }
	}
}
