﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheckGEMAPayoff.Classes;
using System.IO;
using DevFsFunctions.Logging;

namespace CheckGEMAPayoff.Export
{
  public class GEMA2017Export : IExport
  {
    public void Do(IEnumerable<ReportedGig> notAccounted, IEnumerable<ReportedGig> incomplete, string fileName, Log log)
    {
      // seperate bands
      Dictionary<string, List<ReportedGig>> missingByArtist = new Dictionary<string, List<ReportedGig>>();
      foreach (ReportedGig item in notAccounted)
      {
        if (!missingByArtist.ContainsKey(item.ArtistName))
          missingByArtist.Add(item.ArtistName, new List<ReportedGig>());

        missingByArtist[item.ArtistName].Add(item);
      }
      foreach (ReportedGig item in incomplete)
      {
        if (!missingByArtist.ContainsKey(item.ArtistName))
          missingByArtist.Add(item.ArtistName, new List<ReportedGig>());

        missingByArtist[item.ArtistName].Add(item);
      }

      foreach (string artist in missingByArtist.Keys)
      {
        string fileNameByBand = CreateFileName(fileName, artist);

        using (StreamWriter writer = new StreamWriter(fileNameByBand, false, Encoding.UTF8))
        {
          // write Header
          string seperator = ";";
          writer.WriteLine("VERANSTALTUNGSDATUM" + seperator + "VERANSTALTUNGSORT" + seperator + "AUFTRITTSZEIT (Anfang)" + seperator + "AUFTRITTSZEIT (Ende)" + seperator + "VERANSTALTER (inkl. Anschift)" + seperator + "INTERPRET" + seperator + "HAUPT- / VORGRUPPE/gleichwertig" + seperator + "MUSIKLEITER / BANDLEADER" + seperator + "WERKNUMMER" + seperator + "WERKTITEL" + seperator + "KOMPONIST" + seperator + "BEARBEITER" + seperator + "VERLAG" + seperator + "ANZAHL WERKAUFFÜHRUNGEN" + seperator + "BESETZUNG" + seperator + "Potpourri/Fragment" + seperator + "SPIELDAUER  (E-MUSIK)");

          foreach (ReportedGig gig in missingByArtist[artist])
          {
            foreach (Work work in gig.Works)
            {
              writer.WriteLine(gig.DayOfShow.ToString("dd.MM.yyyy") + seperator +
                               gig.Venue.Display + seperator +
                               seperator +
                               seperator +
                               gig.Organiser.Display + seperator +
                               artist + seperator +
                               seperator +
                               seperator +
                               work.WorkNumber.ToString() + work.Version.ToString("000") + seperator +
                               work.Title + seperator +
                               work.Composer + seperator +
                               work.Editor + seperator +
                               seperator +
                               "1" + seperator +
                               seperator +
                               seperator);

            }
          }

        }
      }
    }

    private string CreateFileName(string fileName, string artist)
    {
      int pos = fileName.LastIndexOf(".");
      if (pos < 0)
        pos = fileName.Length - 1;

      return fileName.Insert(pos, artist);
    }
  }
}
