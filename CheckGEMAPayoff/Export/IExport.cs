﻿using CheckGEMAPayoff.Classes;
using DevFsFunctions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckGEMAPayoff.Export
{
  interface IExport
  {
    void Do(IEnumerable<ReportedGig> notAccounted, IEnumerable<ReportedGig> incomplete, string fileName, Log log);
  }
}
