﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheckGEMAPayoff.Classes;
using DevFsFunctions.Logging;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using DevFsExcel;

namespace CheckGEMAPayoff.Export
{
  public class GEMA2017XLSX_Export : IExport
  {
    string templateFile = "GEMA-Formular_Reklamation_Aufführungen_Inland.xlsx";
    const int FIRSTDATAROW = 25; // 0-basiert!

    public void Do(IEnumerable<ReportedGig> notAccounted, IEnumerable<ReportedGig> incomplete, string fileName, Log log)
    {
      ISheet sheet = ExcelFunctions.GetSheetFromExcelFile(templateFile, 0, log);

      FillFormular(notAccounted, incomplete, sheet, log);

      SaveReclamationFile(sheet.Workbook, fileName, log);
    }

    private void FillFormular(IEnumerable<ReportedGig> notAccounted, IEnumerable<ReportedGig> incomplete, ISheet sheet, Log log)
    {
      int currentRow = FIRSTDATAROW;
      int sequenceNumber = 1;

      foreach (ReportedGig gig in notAccounted)
      {
        foreach (Work work in gig.Works)
        {
          WriteGig2Row(sheet.CreateRow(currentRow), gig, work, sequenceNumber, "Kompletter Gig nicht abgerechnet", log);
          
          currentRow++; sequenceNumber++;
        }
      }

      foreach (ReportedGig gig in incomplete)
      {
        foreach (Work work in gig.Works)
        {
          WriteGig2Row(sheet.CreateRow(currentRow), gig, work, sequenceNumber, "Werk fehlt bei abgerechnetem Gig", log);

          currentRow++; sequenceNumber++;
        }
      }
    }

    private void SaveReclamationFile(IWorkbook workbook, string fileName, Log log)
    {
      try
      {
        FileStream saveExcelStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
        workbook.Write(saveExcelStream);
      }
      catch (Exception ex)
      {
        log.Add("Error saving reclamation file: " + ex.GetBaseException().Message);
      }
    }

    private void WriteGig2Row(IRow targetRow, ReportedGig gig, Work work, int sequenceNumber, string reclamationReason, Log log)
    {
      try
      {
        // Laufende Nummer
        targetRow.GetCell(0, MissingCellPolicy.CREATE_NULL_AS_BLANK).SetCellValue(sequenceNumber);

        // Benutzer ID

        //** Datum
        targetRow.GetCell(2, MissingCellPolicy.CREATE_NULL_AS_BLANK).SetCellValue(gig.DayOfShow.ToString("dd.MM.yyyy"));

        // Beginn
        targetRow.GetCell(3, MissingCellPolicy.CREATE_NULL_AS_BLANK).SetCellValue(gig.ShowTime);

        // Ende

        //** Venue (incl. Adresse)
        targetRow.GetCell(5, MissingCellPolicy.CREATE_NULL_AS_BLANK).SetCellValue(gig.Venue.Display);

        //** Veranstalter (inkl. Adresse)
        targetRow.GetCell(6, MissingCellPolicy.CREATE_NULL_AS_BLANK).SetCellValue(gig.Organiser.Display);

        // Artist
        targetRow.GetCell(7, MissingCellPolicy.CREATE_NULL_AS_BLANK).SetCellValue(gig.ArtistName);

        // Art der Veranstaltung

        // musikalischer Leiter

        // GEMA-Werknummer
        targetRow.GetCell(10, MissingCellPolicy.CREATE_NULL_AS_BLANK).SetCellValue(work.WorkNumber);

        //** Werktitel
        targetRow.GetCell(11, MissingCellPolicy.CREATE_NULL_AS_BLANK).SetCellValue(work.Title);

        //** Komponist
        targetRow.GetCell(12, MissingCellPolicy.CREATE_NULL_AS_BLANK).SetCellValue(work.Composer);

        // Bearbeiter
        targetRow.GetCell(13, MissingCellPolicy.CREATE_NULL_AS_BLANK).SetCellValue(work.Editor);

        // Verlag

        // Anzahl der Werkaufführungen
        targetRow.GetCell(15, MissingCellPolicy.CREATE_NULL_AS_BLANK).SetCellValue(1);

        // Besetzung

        // Potpurri/Fragment

        // Spieldauer (mandatory für RICHTIGE Musik)

        // Reklamationsgrund 
        targetRow.GetCell(19, MissingCellPolicy.CREATE_NULL_AS_BLANK).SetCellValue(reclamationReason);
      }
      catch (Exception ex)
      {
        log.Add("Error writing reclamation file for work [" + work.Title + "] of artist [" + gig.ArtistName + "] at the show at [" + gig.DayOfShow + "]: " + ex.GetBaseException().Message);
      }
    }
  }
}
