﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheckGEMAPayoff.Classes;
using System.IO;
using DevFsFunctions.Logging;

namespace CheckGEMAPayoff.Export
{
  public class StandardExport : IExport
  {
    public void Do(IEnumerable<ReportedGig> notAccounted, IEnumerable<ReportedGig> incomplete, string fileName, Log log)
    {
      using (StreamWriter writer = new StreamWriter(fileName, false, Encoding.UTF8))
      {
        // write Header
        string seperator = ";";
        writer.WriteLine("Abgerechnete Kategorie" + seperator + "Datum VA" + seperator + "Location" + seperator + "Organiser" + seperator + "Venue" + seperator + "Artist" + seperator + "Composer" + seperator + "Werktitel" + seperator + "Werknummer" + seperator + "Werkversion" + seperator + "Einreichungstag" + seperator + "Einreichungsart" + seperator + "Fehlerbeschreibung");

        // Komplett fehlende Gigs
        //foreach (ReportedGig gig in CheckedGigs.ReportedButNotAccountedGigs)
        foreach (ReportedGig gig in notAccounted)
        {
          foreach (Work work in gig.Works)
          {
            writer.WriteLine(gig.Category + seperator + gig.DayOfShow.ToShortDateString() + seperator + gig.Venue.Name + seperator + gig.Organiser.Name + ", " + gig.Organiser.Postalcode + " " + gig.Organiser.City + ", " + gig.Organiser.Street + seperator + gig.Venue.Postalcode + " " + gig.Venue.City + seperator + gig.ArtistName + seperator + work.Composer + seperator + work.Title + seperator + work.WorkNumber.ToString() + seperator + work.Version.ToString() + seperator + gig.DayOfTransmission.ToShortDateString() + seperator + (gig.OnlineTransmission ? "Online" : "Papier") + seperator + "Kompletter Gig nicht abgerechnet");
          }

          // Seperator
          writer.WriteLine(seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator);
        }

        // Unvollständig abgerechnete
        // foreach (ReportedGig gig in CheckedGigs.NotCompletelyAccountedGigs)
        foreach (ReportedGig gig in incomplete)
        {
          foreach (Work work in gig.Works)
          {
            writer.WriteLine(gig.Category + seperator + gig.DayOfShow.ToShortDateString() + seperator + gig.Venue.Name + seperator + gig.Organiser.Name + ", " + gig.Organiser.Postalcode + " " + gig.Organiser.City + ", " + gig.Organiser.Street + seperator + gig.Venue.Postalcode + " " + gig.Venue.City + seperator + gig.ArtistName + seperator + work.Composer + seperator + work.Title + seperator + work.WorkNumber.ToString() + seperator + work.Version.ToString() + seperator + gig.DayOfTransmission.ToShortDateString() + seperator + (gig.OnlineTransmission ? "Online" : "Papier") + seperator + "Werk fehlt in Abrechnung");
          }

          writer.WriteLine(seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator);
        }

        // Falsche Kategorie
        //foreach (ReportedGig gig in CheckedGigs.WrongCategory)
        //foreach (ReportedGig gig in wrongCategory)
        //{
        //  foreach (Work work in gig.Works)
        //  {
        //    writer.WriteLine(gig.Category + seperator + gig.DayOfShow.ToShortDateString() + seperator + gig.Venue.Name + seperator + gig.Organiser.Name + ", " + gig.Organiser.Postalcode + " " + gig.Organiser.City + ", " + gig.Organiser.Street + seperator + gig.Venue.Postalcode + " " + gig.Venue.City + seperator + gig.ArtistName + seperator + work.Composer + seperator + work.Title + seperator + work.WorkNumber.ToString() + seperator + work.Version.ToString() + seperator + gig.DayOfTransmission.ToShortDateString() + seperator + (gig.OnlineTransmission ? "Online" : "Papier") + seperator + "Werk in falscher Kategorie abgerechnet");
        //  }

        //  writer.WriteLine(seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator + seperator);
        //}
      }
    }
  }
}
