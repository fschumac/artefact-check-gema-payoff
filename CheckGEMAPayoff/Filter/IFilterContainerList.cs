﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Filter
{
	public interface IFilterContainerList
	{
		Dictionary<string, ICollectionView> Source { get; }
		void Init(object source, string name, FilterTypes filterType);
		void LoadFilter(string filterName);
		void AddSource(object source, string name);
	}
}
