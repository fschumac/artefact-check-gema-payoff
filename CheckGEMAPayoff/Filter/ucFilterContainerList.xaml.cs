﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Linq;

namespace Filter
{
	/// <summary>
	/// Interaction logic for ucFilterContainerList.xaml
	/// </summary>
	public partial class ucFilterContainerList : StackPanel, IFilterContainerList
	{
		private Dictionary<string, ICollectionView> _collectionView;
		private FilterTypes _filterType;

		public ucFilterContainerList()
		{
			this.InitializeComponent();
		}

		public Dictionary<string, ICollectionView> Source
		{
			get { return _collectionView; }
		}


		public void Init(object source, string name, FilterTypes filterType)
		{
			_collectionView = new Dictionary<string, ICollectionView>();
			ICollectionView theView = CollectionViewSource.GetDefaultView(source);
			theView.Filter = ThePredicate;

			_collectionView.Add(name, theView);
			_filterType = filterType;

			this.Children.Clear();
			this.Children.Add(CreateFilterContainer());
		}

		public void AddSource(object source, string name)
		{
			ICollectionView theView = CollectionViewSource.GetDefaultView(source);
			theView.Filter = ThePredicate;

			_collectionView.Add(name, theView);
		}

		private ucFilterContainer CreateFilterContainer()
		{
			ucFilterContainer filterContainer = new ucFilterContainer(_filterType);
			filterContainer.FilterSelected += new EventHandler<ArgsString>(FilterContainer_FilterSelected);
			filterContainer.FilterRemove += new EventHandler(FilterContainer_FilterRemove);
			return filterContainer;
		}

		void FilterContainer_FilterRemove(object sender, EventArgs e)
		{
			// remove the FilterContainer from the StackPanel
			ucFilterContainer filterContainer = sender as ucFilterContainer;
			if (filterContainer != null)
			{
				filterContainer.FilterSelected -= new EventHandler<ArgsString>(FilterContainer_FilterSelected);
				filterContainer.FilterRemove -= new EventHandler(FilterContainer_FilterRemove);

				this.Children.Remove(sender as UIElement);
			}

			// if there is no more Filter, add a new one
			if (this.Children.Count == 0)
				this.Children.Add(CreateFilterContainer());

			foreach (ICollectionView cv in _collectionView.Values)
				cv.Refresh();
		}

		void FilterContainer_FilterSelected(object sender, ArgsString e)
		{
			// Create the returned Filter and place it in the content control
			ucFilterContainer filterContainer = sender as ucFilterContainer;
			if (filterContainer != null)
			{
				var type = typeof(IFilter);
				var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(p => type.IsAssignableFrom(p) && p.IsClass);
				foreach (Type mytype in types)
				{
					IFilter filterUC = (IFilter)Activator.CreateInstance(mytype);
					if (filterUC.FilterType == _filterType)
						if (filterUC.FilterName == e.s)
						{
							filterUC.FilterUpdated += new EventHandler(FilterUpdated);
							filterContainer.ContentControlFilterDisplay.Content = filterUC;
						}
				}
			}

			// Execute the filter
			foreach (ICollectionView cv in _collectionView.Values)
				cv.Refresh();

			// Create a new empty FilterContainer
			this.Children.Add(CreateFilterContainer());
		}

		public void LoadFilter(string filterName)
		{
			ucFilterContainer emptyContainer = null;

			// get the empty FilterContainer
			foreach (ucFilterContainer filterContainer in this.Children)
				if (!filterContainer.IsUsed)
				{
					emptyContainer = filterContainer;
					break;
				}

			emptyContainer.TrySelectFilter(filterName);
		}

		void FilterUpdated(object sender, EventArgs e)
		{
			foreach (ICollectionView cv in _collectionView.Values)
				cv.Refresh();
		}

		private bool ThePredicate(Object o)
		{
			foreach (UserControl filterControl in this.Children)
			{
				ucFilterContainer container = filterControl as ucFilterContainer;
				IFilter filter = container.ContentControlFilterDisplay.Content as IFilter;
				if (filter != null)
				{
					bool result = filter.Evaluate(o);

					if (!result)
						return result;
				}
			}

			return true;
		}

	}
}