﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.ComponentModel;

namespace Filter
{
	public interface IFilter
	{
		bool Evaluate(object element);
		string FilterName { get; }
		event EventHandler FilterUpdated;
		FilterTypes FilterType { get; }
	}

	public enum FilterTypes
	{
		analysis
	}
}
