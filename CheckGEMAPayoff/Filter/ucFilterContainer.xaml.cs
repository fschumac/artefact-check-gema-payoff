﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Linq;

namespace Filter
{
	/// <summary>
	/// Interaction logic for ucFilterContainer.xaml
	/// </summary>
	public partial class ucFilterContainer : UserControl
	{
		private bool _isUsed;

		public ucFilterContainer(FilterTypes filterType)
		{
			this.InitializeComponent();

			_isUsed = false;
			List<string> filterNames = new List<string>();
			filterNames.Add("Wähle einen Filter");

			var type = typeof(IFilter);
			var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(p => type.IsAssignableFrom(p) && p.IsClass);
			foreach (Type mytype in types)
			{
				IFilter filterUC = (IFilter)Activator.CreateInstance(mytype);
				if (filterUC.FilterType == filterType)
					filterNames.Add(filterUC.FilterName);
			}

			ComboBoxFilterList.ItemsSource = filterNames;
			ComboBoxFilterList.SelectedIndex = 0;
		}

		public event EventHandler<ArgsString> FilterSelected;
		public event EventHandler FilterRemove;

		private void ComboBoxFilterList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
		{
			if (e.AddedItems.Count > 0)
				SelectFilter(e.AddedItems[0].ToString());

		}

		public bool IsUsed
		{
			get { return _isUsed; }
		}

		public void TrySelectFilter(string filterName)
		{
			if (((List<string>)ComboBoxFilterList.ItemsSource).Contains(filterName))
				SelectFilter(filterName);
		}

		private void ImageRemove_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (FilterRemove != null)
				if (ContentControlFilterDisplay.Content != null)
					FilterRemove(this, new EventArgs());
		}

		private void SelectFilter(string filterName)
		{
			if (FilterSelected != null)
			{
				FilterSelected(this, new ArgsString(filterName));

				TextBlockFilterName.Text = filterName;
				TextBlockFilterName.Visibility = Visibility.Visible;
				ComboBoxFilterList.Visibility = Visibility.Collapsed;
				_isUsed = true;
			}
		}
	}
}