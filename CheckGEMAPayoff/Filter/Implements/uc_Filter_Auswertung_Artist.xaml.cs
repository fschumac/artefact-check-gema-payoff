﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Filter;
using CheckGEMAPayoff.Classes;

namespace CheckGEMAPayoff
{
	/// <summary>
	/// Interaction logic for uc_Filter_Auswertung_Artist.xaml
	/// </summary>
	public partial class uc_Filter_Auswertung_Artist : UserControl, IFilter
	{
		public uc_Filter_Auswertung_Artist()
		{
			this.InitializeComponent();
		}

		public bool Evaluate(object element)
		{
			ReportedGig item = element as ReportedGig;

			if (item != null)
			{
				if (item.ArtistName == null)
					if (String.IsNullOrWhiteSpace(TextBox_Artist.Text))
						return true;
					else
						return false;

				if (item.ArtistName.Contains(TextBox_Artist.Text))
					return true;
				else
					return false;
			}

			return false;
		}

		public string FilterName
		{
			get { return "Artist"; }
		}

		public event EventHandler FilterUpdated;

		public FilterTypes FilterType
		{
			get { return FilterTypes.analysis; }
		}

		private void TextBox_Artist_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
		{
			if (FilterUpdated != null)
				FilterUpdated(this, new EventArgs());
		}
	}
}