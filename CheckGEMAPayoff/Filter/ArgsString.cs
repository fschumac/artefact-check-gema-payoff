﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Filter
{
	public class ArgsString : EventArgs
	{
		private string _s;

		public ArgsString(string s)
		{
			_s = s;
		}

		public string s
		{
			get { return _s; }
		}
	}
}
