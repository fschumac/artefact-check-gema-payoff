﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CheckGEMAPayoff.Classes;
using Microsoft.Win32;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.ComponentModel;
using CheckGEMAPayoff.Properties;
using DevFsExcel;
using DevFsFunctions.Logging;

namespace CheckGEMAPayoff
{
	/// <summary>
	/// Interaction logic for ucShowData.xaml
	/// </summary>
	public partial class ucShowData : UserControl
	{
		private const string NAME_SUBDIR_TRACKLIST = "Titelliste";
		private const string TRACKLIST_CSV_SEPERATOR = ";";

		private ObservableCollection<string> _importErrorList;
		private ObservableCollection<string> _importInfoList;

		public ucShowData()
		{
			this.InitializeComponent();

			_importErrorList = new ObservableCollection<string>();
			_importInfoList = new ObservableCollection<string>();
			listBoxErrorLog.ItemsSource = _importErrorList;
			listBoxInfoLog.ItemsSource = _importInfoList;
			listBoxShows.ItemsSource = Manager.instance.ArtistInfo;
			listBoxShows.DisplayMemberPath = "Name";
		}

		public ObservableCollection<string> ImportErrorList
		{
			get { return _importErrorList; }
		}

		private void buttonAddFolder_Click(object sender, System.Windows.RoutedEventArgs e)
		{
      Log log = new Log();

			System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
			string startFolder = Manager.instance.Configuration.PathShowData;
			if (!string.IsNullOrWhiteSpace(startFolder))
				fbd.SelectedPath = startFolder;

			if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
        Manager.instance.Configuration.PathShowData = fbd.SelectedPath;
        Manager.instance.Configuration.SaveConfiguration(Manager.CONFIGURATION_FILENAME, new Log());

				// get all excel-Files from that directory
				DirectoryInfo dirInfo = new DirectoryInfo(fbd.SelectedPath);
				DirectoryInfo[] artistDirs = dirInfo.GetDirectories();
				//Manager.instance.ArtistInfo.Clear();

				int errorCount = 0;

				foreach (DirectoryInfo di in artistDirs)
				{
					ImportArtistShowData(di, ref errorCount, log);
				}

				//CheckTrackListNumbers(ref errorCount);

				if (errorCount > 0)
				{
					Manager.instance.SelectTab(tabControlDisplay, "tabItemErrorLog");

					MessageBox.Show("Der Import war fehlerhaft. Bitte behebe die Fehler in den entsprechenden Dateien und starte den Import erneut.", "Import FEHLERHAFT!", MessageBoxButton.OK, MessageBoxImage.Error);
				}
				else
				{
					Manager.instance.InitReportedGigsList();

					Manager.instance.SelectTab(tabControlDisplay, "tabItemInfoLog");

					MessageBox.Show(artistDirs.Length.ToString() + " Künstler wurden erfolgreich importiert", "Import ERFOLGREICH!", MessageBoxButton.OK, MessageBoxImage.Information);
				}
			}

      if (log.CountByType(EnumLogData.Error) > 0)
        MessageBox.Show(log.ToString(), "Error importing concert lists", MessageBoxButton.OK, MessageBoxImage.Error);
		}

		private void ImportArtistShowData(DirectoryInfo di, ref int errorCount, Log log)
		{
			if (Manager.instance.ArtistInfo.Any(p => p.Name == di.Name))
			{
				_importInfoList.Add(di.Name + ": Bereits geladen. Wird übersprungen.");
				return;
			}

			_importInfoList.Add(di.Name + ": Beginne mit Import");
			int errorCountOnStart = errorCount;

			// get the Shows-File
			FileInfo[] fi = di.GetFiles("*.xls");

			// there has to be exactly ONE xls-File
			if (fi.Length != 1)
			{
				_importInfoList.Add(di.Name + ": Import NICHT ERFOLGREICH abgeschlossen!");
				_importErrorList.Add(di.Name + ": Im Künstlerverzeichnis muss genau EINE Aufführungsliste im XLS-Format liegen. Es wurden aber " + fi.Length + " xls-Dateien gefunden.");
				errorCount++;
				return;
			}

			int errorCountBefore = errorCount;
			ArtistInfo ai = new ArtistInfo(di.Name, fi[0].FullName);
			ai.Shows = ImportAuffuehrungsliste(fi[0], ref errorCount, di.Name, log);

			// if all the tracklists numbers are empty, there has to be just one tracklist 
			bool defaultTracklist = true;
			if (ai.Shows.Count > 0)
			{
				int defaultUseCount = 0;

				foreach (ReportedGig show in ai.Shows)
					if (String.IsNullOrWhiteSpace(show.TrackListName))
						defaultUseCount++;

				if (defaultUseCount == 0)
					defaultTracklist = false;
				else if (defaultUseCount < ai.Shows.Count)
				{
					_importErrorList.Add("Für " + ai.Name + " wurde bei einigen Shows keine Setliste angegeben. Entweder muss für ALLE Shows eine Setliste angegeben werden, oder für KEINE.");
					errorCount++;
					return;
				}
			}
			else
			{
				_importErrorList.Add("Konnte keine Shows für " + ai.Name + " laden. Bitte checke das Format der Musikfolge und prüfe, ob die im Reiter 'Konfiguration' angegebenen Spalten und Zeilen mit der Musikfolge übereinstimmt.");
				errorCount++;
				return;
			}

			if (errorCount > errorCountBefore)
			{
				_importInfoList.Add(di.Name + ": Import NICHT ERFOLGREICH abgeschlossen!");
				return;
			}

			// get the tracklists
			if (!Directory.Exists(di.FullName + "\\" + NAME_SUBDIR_TRACKLIST))
			{
				_importInfoList.Add(di.Name + ": Import NICHT ERFOLGREICH abgeschlossen!");
				_importErrorList.Add(di.Name + ": Im Künstlerverzeichnis muss ein Unterverzeichnis '" + NAME_SUBDIR_TRACKLIST + "' existieren.");
				errorCount++;
				return;
			}

			FileInfo[] fiTitellisten = new DirectoryInfo(di.FullName + "\\" + NAME_SUBDIR_TRACKLIST).GetFiles("*.csv");
			if (fiTitellisten.Length == 0)
			{
				_importInfoList.Add(di.Name + ": Import NICHT ERFOLGREICH abgeschlossen!");
				_importErrorList.Add(di.Name + ": Im Titellistenverzeichnis '" + di.FullName + "\\" + NAME_SUBDIR_TRACKLIST + "' wurden keine Titellisten gefunden.");
				errorCount++;
				return;
			}

			if (defaultTracklist && fiTitellisten.Length != 1)
			{
				_importInfoList.Add(di.Name + ": Import NICHT ERFOLGREICH abgeschlossen!");
				_importErrorList.Add(di.Name + ": Wenn in der Aufführungsliste keine Titellistennummer angegeben wird darf im Titellistenverzeichnis nur genau eine Titelliste liegen.");
				errorCount++;
				return;
			}

			// Import Setlists
			errorCountBefore = errorCount;
			foreach (FileInfo fiTitelliste in fiTitellisten)
			{
				ai.Tracklists.Add(fiTitelliste.Name, ImportTrackliste(fiTitelliste.FullName, ref errorCount, di.Name, fiTitelliste.Name));
			}

			// Import Setlists for Gigs
			foreach (ReportedGig gig in ai.Shows)
			{
				Dictionary<string, IList<Work>> tracklists = new Dictionary<string, IList<Work>>();
				foreach (KeyValuePair<string, IList<Work>> item in ai.Tracklists)
					if (item.Key.Contains(gig.TrackListName))
						tracklists.Add(item.Key, item.Value);

				switch (tracklists.Count)
				{
					case 0: // keine Trackliste gefunden
						_importErrorList.Add(di.Name + ": Keine Datei für Setlisten-Name [" + gig.TrackListName + "] für Gig vom " + gig.DayOfShow.ToShortDateString() + " gefunden.");
						errorCount++;
						break;
					case 1: // that's the way it got to be!
						foreach (Work work in ai.Tracklists[tracklists.Keys.ElementAt(0)])
							gig.Works.Add(work);
						break;
					default: // mehr als eine Tracklist gefunden
						_importErrorList.Add(di.Name + ": Setlisten-Name nicht eindeutig für Gig vom " + gig.DayOfShow.ToShortDateString() + ". " + gig.TrackListName + " ist in mehr als einem Dateinamen enthalten.");
						errorCount++;
						break;
				}
			}

			if (errorCountBefore < errorCount)
			{
				_importInfoList.Add(di.Name + ": Import NICHT ERFOLGREICH abgeschlossen!");
				return;
			}

			// add to list
			ai.ErrorCount = errorCount - errorCountOnStart;
			Manager.instance.ArtistInfo.Add(ai);
			_importInfoList.Add(di.Name + ": Import erfolgreich abgeschlossen!");
		}

		//private void buttonExport2TripleStore_Click(object sender, System.Windows.RoutedEventArgs e)
		//{
		//	if (Manager.instance.ArtistInfo.Count == 0)
		//	{
		//		MessageBox.Show("No shows to export!");
		//		return;
		//	}

		//	SparqlRemoteEndpoint endpoint = SparqlEndpointFactory.GetMBOInstanceEndpoint(Settings.Default.Endpoint_URL, Settings.Default.Graph_Name, Settings.Default.User_Name);

		//	// TRY USING GET BECAUSE OF THE URIFORMATEXCEPTION WITH POST
		//	endpoint.HttpMode = "GET";

		//	Write2TripleStoreArgs args = new Write2TripleStoreArgs();

		//	BackgroundWorker bgw = new BackgroundWorker();
		//	bgw.DoWork += new DoWorkEventHandler(bgw_DoWork);
		//	bgw.WorkerReportsProgress = true;
		//	bgw.ProgressChanged += new ProgressChangedEventHandler(bgw_ProgressChanged);
		//	bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgw_RunWorkerCompleted);

		//	args.Endpoint = endpoint;
		//	args.ArtistInfos = Manager.instance.ArtistInfo;
		//	args.BaseNamespace = Settings.Default.Graph_Name;
		//	args.CallProgressReport = bgw.ReportProgress;
		//	args.ErrorLog = new List<LogData>();
		//	args.LocalName = Settings.Default.Local_Name;


		//	BusyIndicatorObject.IsBusy = true;
		//	bgw.RunWorkerAsync(args);
		//}

		//void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		//{
		//	BusyIndicatorObject.IsBusy = false;
		//	MessageBox.Show("Export in Triplestore abgeschlossen");

		//	IList<string> errorLog = e.Result as IList<string>;

		//	if (errorLog != null && errorLog.Count > 0)
		//	{
		//		ErrorLogDisplayWindow wnd = new ErrorLogDisplayWindow();
		//		wnd.DataGrid_ErrorLog.DataContext = errorLog;
		//		wnd.ShowDialog();
		//	}
		//}

		//void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
		//{
		//	BusyIndicatorObject.BusyContent = e.ProgressPercentage.ToString() + "% - " + e.UserState.ToString();
		//}

		//void bgw_DoWork(object sender, DoWorkEventArgs e)
		//{
		//	Write2TripleStoreArgs args = e.Argument as Write2TripleStoreArgs;

		//	if (args != null)
		//	{
		//		e.Result = args.ErrorLog;
		//		Export2TripleStore exporter = new Export2TripleStore();
		//		exporter.StartExport(args);
		//		//e.Result = exporter.StartExport(args);
		//	}
		//}


		private void CheckTrackListNumbers(ref int errorCount)
		{
			int errorCountStart = errorCount;

			foreach (ArtistInfo artist in Manager.instance.ArtistInfo)
			{
				HashSet<string> trackListIdent = new HashSet<string>();
				foreach (KeyValuePair<string, IList<Work>> trackList in artist.Tracklists)
					trackListIdent.Add(trackList.Key);

				foreach (ReportedGig show in artist.Shows)
				{
					if (!trackListIdent.Contains(show.TrackListName))
					{
						errorCount++;
						_importErrorList.Add(artist.Name + ": Für Trackliste " + show.TrackListName.ToString() + " (verwendet in Show am " + show.DayOfShow.ToShortDateString() + ") liegt keine Trackliste vor.");
					}
				}
			}

			if (errorCount > errorCountStart)
			{
				_importInfoList.Add((errorCount - errorCountStart).ToString() + " Fehler bei der Kontrolle der Titellistenzuordnung.");
			}
		}

		private IList<Work> ImportTrackliste(string fileName, ref int errorCount, string artistName, string titelListeName)
		{
			IList<Work> resultSet = new List<Work>();

			try
			{
				using (StreamReader sr = new StreamReader(fileName, Encoding.GetEncoding(1252))) // Load as ANSI
				{
					String line;
					int lineCount = 0;

					_importInfoList.Add(artistName + ": Importiere Trackliste '" + fileName + "'.");
					while ((line = sr.ReadLine()) != null)
					{
						lineCount++;
						if (lineCount > 1)
						{
							Work performedWork = ReadTracklistLine(line, lineCount, artistName, titelListeName);

							//if (!resultSet.Any(w => w.WorkNumber == performedWork.WorkNumber))
							//{
								resultSet.Add(performedWork);

								if (performedWork.WorkNumber <= 0)
									_importInfoList.Add(artistName + ": Titelliste '" + titelListeName + "': Song '" + performedWork.Title + "' ohne Werknummer. Zuordnung zu GEMA-Daten dadurch nicht möglich.");
							//}
							//else
							//	_importInfoList.Add(artistName + ": Titelliste '" + titelListeName + "': Song '" + performedWork.Title + "' doppelt aufgeführt. Doppelung entfernt.");

						}
					}
					_importInfoList.Add(artistName + ": Tracklistenimport abgeschlossen.");
				}
			}
			catch (Exception e)
			{
				_importErrorList.Add("Exception: " + e.Message);
			}

			return resultSet;
		}

		private Work ReadTracklistLine(string line, int lineCount, string artistName, string trackListName)
		{
			Work result = new Work();

			// split the data
			string[] lineSplit = line.Split(TRACKLIST_CSV_SEPERATOR.ToCharArray());

			if (lineSplit.Length != 8)
			{
				_importInfoList.Add(artistName + "Import NICHT ERFOLGREICH abgeschlossen!");
				_importErrorList.Add(artistName + "Die Titelliste '" + trackListName + "hat ein in Zeile " + lineCount.ToString() + " ein ungültiges Format. 8 Spalten erwartet, " + lineSplit.Length.ToString() + " gefunden.");
				return null;
			}

			// worknumber
			int worknumber = -1;
			if (Int32.TryParse(lineSplit[0], out worknumber))
				result.WorkNumber = worknumber;
			//if (!Int32.TryParse(lineSplit[0], out worknumber))
			//{
			//  _importErrorList.Add(artistName + "Titelliste " + trackListName + ": Fehler in Zeile " + lineCount.ToString() + ". Werknummer: " + lineSplit[0] + " ist keine Zahl.");
			//}
			//result.WorkNumber = worknumber;

			result.Title = lineSplit[1];
			result.Composer = lineSplit[2];
			result.Editor = lineSplit[3];
			result.Publisher = lineSplit[4];

      result.Version = 1; // keine Angabe zur Werkversion in GEMA-Titelliste

			return result;
		}

		private void buttonRemove_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			Manager.instance.ArtistInfo.Clear();
			_importErrorList.Clear();
			_importInfoList.Clear();

			dataGridShowDetails.DataContext = null;
			StackPanelTracklisten.Children.Clear();
		}

		private void buttonRemoveSingle_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			ArtistInfo selectedArtits = listBoxShows.SelectedItem as ArtistInfo;

			if (selectedArtits != null)
			{
				Manager.instance.ArtistInfo.Remove(selectedArtits);

				IList<string> errorItems2Delete = new List<String>();
				foreach (String errorItem in _importErrorList)
					if (errorItem.Contains(selectedArtits.Name))
						errorItems2Delete.Add(errorItem);

				foreach (String errorItem in errorItems2Delete)
					_importErrorList.Remove(errorItem);
			}

		}

		private void Button_Cancel_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			MessageBox.Show("Cancel not supported");
		}

		private ObservableCollection<ReportedGig> ImportAuffuehrungsliste(FileInfo file, ref int errorCount, string nameArtist, Log log)
		{
      if (!Manager.instance.Configuration.CheckConfiguration(log)) return null;

			ObservableCollection<ReportedGig> resultSet = new ObservableCollection<ReportedGig>();
			_importInfoList.Add(nameArtist + ": Starte Import der Aufführungsliste '" + file.Name + "'");
			int errorCountStart = errorCount;

			ISheet sheet = null;
			try
			{
				HSSFWorkbook workbook = new HSSFWorkbook(file.Open(FileMode.Open, FileAccess.Read, FileShare.Read));
				sheet = workbook.GetSheet(Manager.instance.Configuration.ConcertlistSheetName);
			}
			catch (Exception ex)
			{
				_importErrorList.Add("Konnte Datei '" + file.Name + "' nicht öffenen. Die Fehlermeldung lautet: " + ex.Message);
				errorCount++;
			}

			if (sheet != null)
			{
				try
				{
					for (int currentRow = Manager.instance.Configuration.FirstDataRow; currentRow <= sheet.LastRowNum; currentRow++)
					{
						ReportedGig sd = new ReportedGig();
						IRow row = sheet.GetRow(currentRow);

						if (row == null || row.Cells.Count == 0)
							continue;

						if (String.IsNullOrWhiteSpace(row.GetCell(row.FirstCellNum, MissingCellPolicy.CREATE_NULL_AS_BLANK).ToString()))
							continue;

						// DAY OF SHOW
						if (row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_ShowDate), MissingCellPolicy.CREATE_NULL_AS_BLANK).DateCellValue == null)
						{
							_importErrorList.Add(nameArtist + ": '" + row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_ShowDate)).ToString() + "' in Zeile " + currentRow.ToString() + " konnte nicht als Datum erkannt werden. Diese Show wurde nicht aufgenommen!");
							errorCount++;
							continue;
						}
						else
							sd.DayOfShow = row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_ShowDate), MissingCellPolicy.CREATE_NULL_AS_BLANK).DateCellValue;

            // SHOW TIME
            sd.ShowTime = row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_ShowTime), MissingCellPolicy.CREATE_NULL_AS_BLANK).AsString();
            
						// VENUE
						sd.Venue.Name = row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_VenueName), MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue;
						sd.Venue.Street = row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_VenueStreet), MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue;
						string[] tmp = row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_VenueCity), MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue.Trim().Split(" ".ToCharArray());

						// check wheter we found a postal code. It has to contain at least one digit
						System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(tmp[0], @"\d");
						if (match.Success)
						{
							sd.Venue.Postalcode = tmp[0];
							StringBuilder sb = new StringBuilder();
							for (int i = 1; i < tmp.Length; i++)
								sb.Append(tmp[i] + " ");
							sd.Venue.City = sb.ToString().Trim();
						}
						else
							sd.Venue.City = row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_VenueCity), MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue.Trim();

						// ORGANIZER
						sd.Organiser.Name = row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_OrganiserName), MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue;
						sd.Organiser.Street = row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_OrganiserStreet), MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue;
						tmp = row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_OrganiserCity), MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue.Trim().Split(" ".ToCharArray());

						// check wheter we found a postal code. It has to contain at least one digit
						match = System.Text.RegularExpressions.Regex.Match(tmp[0], @"\d");
						if (match.Success)
						{
							sd.Organiser.Postalcode = tmp[0];
							StringBuilder sb = new StringBuilder();
							for (int i = 1; i < tmp.Length; i++)
								sb.Append(tmp[i] + " ");
							sd.Organiser.City = sb.ToString().Trim();
						}
						else
							sd.Organiser.City = row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_OrganiserCity), MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue.Trim();

						// TRACKLISTNAME
						if (row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_SetlistFileName), MissingCellPolicy.CREATE_NULL_AS_BLANK).CellType != CellType.String)
						{
							sd.TrackListName = string.Empty;
						}
						else
						{
							sd.TrackListName = row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_SetlistFileName), MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue;
						}

						//SUBMISSION DAY
						if (row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_SubmissionDate), MissingCellPolicy.CREATE_NULL_AS_BLANK).DateCellValue != null)
							sd.DayOfTransmission = row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_SubmissionDate), MissingCellPolicy.CREATE_NULL_AS_BLANK).DateCellValue;

						// Online or Paper
						if (row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_SubmissionType), MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue.Trim().ToLower() == "papier")
							sd.OnlineTransmission = false;
						else
							sd.OnlineTransmission = true;

						// Category
						//if (row.GetCell(COL_CATEGORY, MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue.Trim().ToLower() == "ja")
						//	sd.Category = "UD";
						//else
						//	sd.Category = "U";

						// EVENT NAME
						sd.EventName = row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_EventName), MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue.Trim();

						// ROW NUMBER
						sd.rowNumber = currentRow;

						resultSet.Add(sd);
					}
				}
				catch (Exception ex)
				{
					_importErrorList.Add(nameArtist + ": Der folgende Fehler trat beim Import der Showdaten auf:" + Environment.NewLine + ex.Message + Environment.NewLine + "Bitte überprüfen sie das Format der Showdaten-Excel-Datei und passen gegebenenfalls die Konfiguration an.");
					errorCount++;				
				}
			}

			_importInfoList.Add(nameArtist + ": Import der Aufführungsliste '" + file.Name + "' mit " + (errorCount - errorCountStart).ToString() + " Fehlern abgeschlossen");

			return resultSet;
		}

		private void listBoxShows_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			ArtistInfo sel = listBoxShows.SelectedItem as ArtistInfo;

			if (sel != null)
			{
				// display ShowDetails
				dataGridShowDetails.DataContext = sel.Shows;

				// fill info tracklist
				StackPanel header = null;
				if (StackPanelTracklisten.Children.Count > 0)
					header = StackPanelTracklisten.Children[0] as StackPanel;

				StackPanelTracklisten.Children.Clear();
				if (header != null)
					StackPanelTracklisten.Children.Add(header);

				foreach (KeyValuePair<string, IList<Work>> tracklists in sel.Tracklists)
				{
					ucTrackListViewer trackListViewer = new ucTrackListViewer();

					trackListViewer.TextBlockTrackliste.Text = "Trackliste " + tracklists.Key;
					trackListViewer.dataGridTrackliste.DataContext = tracklists.Value;

					StackPanelTracklisten.Children.Add(trackListViewer);
				}

				Manager.instance.SelectTab(tabControlDisplay, "tabItemDetails");
				e.Handled = true;
				TextBlock_SelectedArtist.Text = sel.Name;
			}
		}

		private void buttonAddArtist_Click(object sender, RoutedEventArgs e)
		{
      Log log = new Log();

			System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();

			if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				// get all excel-Files from that directory
				DirectoryInfo dirInfo = new DirectoryInfo(fbd.SelectedPath);

				int errorCount = 0;

				ImportArtistShowData(dirInfo, ref errorCount, log);

				//CheckTrackListNumbers(ref errorCount);

				if (errorCount > 0)
				{
					Manager.instance.SelectTab(tabControlDisplay, "tabItemErrorLog");

					MessageBox.Show("Der Import war fehlerhaft. Bitte behebe die Fehler in den entsprechenden Dateien und starte den Import erneut.", "Import FEHLERHAFT!", MessageBoxButton.OK, MessageBoxImage.Error);
				}
				else
				{
					Manager.instance.InitReportedGigsList();

					Manager.instance.SelectTab(tabControlDisplay, "tabItemInfoLog");

					MessageBox.Show("Künstler " + dirInfo.Name + " wurde erfolgreich importiert", "Import ERFOLGREICH!", MessageBoxButton.OK, MessageBoxImage.Information);
				}
			}

      if (log.CountByType(EnumLogData.Error) > 0)
        MessageBox.Show(log.ToString(), "Error importing concert lists", MessageBoxButton.OK, MessageBoxImage.Error);
    }

    private void buttonExport2TurtleFile_Click(object sender, RoutedEventArgs e)
		{

		}

		private void buttonExport2XMLFile_Click(object sender, RoutedEventArgs e)
		{

		}
	}

}