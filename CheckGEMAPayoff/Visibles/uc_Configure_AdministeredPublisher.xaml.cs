﻿using CheckGEMAPayoff.Properties;
using DevFsFunctions.Logging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckGEMAPayoff
{
	/// <summary>
	/// Interaktionslogik für uc_Configure_AdministeredPublisher.xaml
	/// </summary>
	public partial class uc_Configure_AdministeredPublisher : UserControl
	{
		ObservableCollection<string> _publishers = null;

		public uc_Configure_AdministeredPublisher()
		{
			this.InitializeComponent();
		}

		public void Init()
		{
			_publishers = new ObservableCollection<string>();

			foreach (string item in Manager.instance.Configuration.AdministeredPublishers)
				_publishers.Add(item);

			ListBox_AdministeredPublisher.ItemsSource = _publishers;
		}

		private void Button_AddPublisher_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			if (!string.IsNullOrWhiteSpace(TextBox_NewPublisher.Text) && !_publishers.Contains(TextBox_NewPublisher.Text))
			{
				_publishers.Add(TextBox_NewPublisher.Text);
				TextBox_NewPublisher.Text = String.Empty;
			}
		}

		private void Button_RemoveSelectedEntry_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			if (ListBox_AdministeredPublisher.SelectedItem != null)
				_publishers.Remove((string)ListBox_AdministeredPublisher.SelectedItem);
		}

		private void Button_Save_Click(object sender, System.Windows.RoutedEventArgs e)
		{
      Manager.instance.Configuration.AdministeredPublishers.Clear();

			foreach (string item in _publishers)
        Manager.instance.Configuration.AdministeredPublishers.Add(item);

      Log log = new Log();
      Manager.instance.Configuration.SaveConfiguration(Manager.CONFIGURATION_FILENAME, log);

      if (log.Count == 0)
        MessageBox.Show("Liste der administrierten Verlage/Editionen gespeichert!");
      else
        MessageBox.Show(log.ToString(), "Error saving configuration", MessageBoxButton.OK, MessageBoxImage.Error);
		}

		private void Button_Reset_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			Init();
		}
	}
}