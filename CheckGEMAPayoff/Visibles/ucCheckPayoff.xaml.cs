﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CheckGEMAPayoff.Classes;
using System.Collections.ObjectModel;
using CheckGEMAPayoff.Check;
using System.Linq;
using System.IO;
using Microsoft.Win32;
using System.ComponentModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using CheckGEMAPayoff.Properties;
using DevFsFunctions.Logging;
using DevFsExcel;

namespace CheckGEMAPayoff
{
	/// <summary>
	/// Interaction logic for ucCheckPayoff.xaml
	/// </summary>
	public partial class ucCheckPayoff : UserControl
	{
		ReportedGig _gig4Alternatives;

		public ucCheckPayoff()
		{
			this.InitializeComponent();

			FilterContainerList.Init(Manager.instance.CheckedGigs.CorrectAccounted, "CorrectAccounted", Filter.FilterTypes.analysis);
			dataGrid_CorrectAccounted.DataContext = FilterContainerList.Source["CorrectAccounted"];

			FilterContainerList.AddSource(Manager.instance.CheckedGigs.ReportedButNotAccountedGigs, "NotAccounted");
			dataGrid_NotAccounted.DataContext = FilterContainerList.Source["NotAccounted"];

			FilterContainerList.AddSource(Manager.instance.CheckedGigs.NotCompletelyAccountedGigs, "Incomplete");
			dataGrid_NotCompletelyAccountedGigs.DataContext = FilterContainerList.Source["Incomplete"];

			//FilterContainerList.AddSource(Manager.instance.CheckedGigs.WrongCategory, "WrongCategory");
			//dataGrid_WrongCategory.DataContext = FilterContainerList.Source["WrongCategory"];

			FilterContainerList.AddSource(Manager.instance.CheckedGigs.AccountedButNotReportedGigs, "AccountedNotReported");
			dataGrid_AccountedButNotReportedGigs.DataContext = FilterContainerList.Source["AccountedNotReported"];

			//FilterContainerList.AddSource(Manager.instance.CheckedGigs.CorrectAccounted_2, "CorrectAccounted_2");
			//dataGrid_CorrectAccounted_2.DataContext = FilterContainerList.Source["CorrectAccounted_2"];

			//FilterContainerList.AddSource(Manager.instance.CheckedGigs.NotAccounted_2, "NotAccounted_2");
			//dataGrid_NotAccounted_2.DataContext = FilterContainerList.Source["NotAccounted_2"];
		}

		private void ButtonStartCheck_Click(object sender, RoutedEventArgs e)
		{

			Manager.instance.Check();


			ButtonExport.IsEnabled = true;
		}

		private void ButtonExport_Click(object sender, RoutedEventArgs e)
		{
      Log log = new Log();

			ListCollectionView notAccountedView = dataGrid_NotAccounted.DataContext as ListCollectionView;
			ListCollectionView incompleteView = dataGrid_NotCompletelyAccountedGigs.DataContext as ListCollectionView;
			ListCollectionView wrongCategoryView = dataGrid_WrongCategory.DataContext as ListCollectionView;
			ListCollectionView correctView = dataGrid_CorrectAccounted.DataContext as ListCollectionView;
			ListCollectionView accountedNotReportedView = dataGrid_AccountedButNotReportedGigs.DataContext as ListCollectionView;

			var notAccounted = notAccountedView.Cast<ReportedGig>();
			var incomplete = incompleteView.Cast<ReportedGig>();
			var wrongCategory = wrongCategoryView.Cast<ReportedGig>();
			var correct = correctView.Cast<ReportedGig>();
			var accountedNotReported = accountedNotReportedView.Cast<Gig>();

			Manager.instance.ExportChecks(notAccounted, incomplete, wrongCategory, correct, accountedNotReported, ComboBox_Exportformat.SelectedIndex, log);

      if (log.Count == 0)
        MessageBox.Show("Successfully exported reclamation.");
      else
        MessageBox.Show("Error(s) during export: " + log.ToString());
		}

		private void Button_Accept_Click(object sender, RoutedEventArgs e)
		{
			if (MessageBox.Show("Wenn du die Show als die abgerechnete Show akzeptierst, wird in deiner Showdatentabelle Datum und Venue mit den Angaben aus der GEMA-Abrechnung ersetzt!", "Show akzeptieren?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Cancel) == MessageBoxResult.No)
				return;

			Gig acceptedGig = ((Button)sender).DataContext as Gig;

			ArtistInfo ai = (from p in Manager.instance.ArtistInfo where p.Name.Equals(_gig4Alternatives.ArtistName) select p).FirstOrDefault();

			HSSFWorkbook workbook = null;
			ISheet sheet = null;
			try
			{
				using (FileStream sourceStream = new FileStream(ai.ShowDataFileName, FileMode.Open, FileAccess.Read))
				{
					workbook = new HSSFWorkbook(sourceStream);
					sheet = workbook.GetSheet(Manager.instance.Configuration.ConcertlistSheetName);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Konnte Datei '" + ai.ShowDataFileName + "' nicht öffnen: " + ex.Message);
			}

			if (sheet != null)
			{
				// get the row with the gig 2 correct

				IRow row = sheet.GetRow(_gig4Alternatives.rowNumber);
				if (row != null)
				{
					row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_ShowDate)).SetCellValue(acceptedGig.DayOfShow);
					row.GetCell(ExcelFunctions.GetColumnNumber(Manager.instance.Configuration.Column_VenueCity)).SetCellValue(acceptedGig.Venue.Postalcode + " " + acceptedGig.Venue.City);

					using (FileStream stream = new FileStream(ai.ShowDataFileName, FileMode.Create, FileAccess.Write))
					{
						try
						{
							workbook.Write(stream);
							MessageBox.Show("Änderungen erfolgreich geschrieben");
							Manager.instance.Check();
						}
						catch (Exception ex)
						{
							MessageBox.Show("Fehler beim Schreiben der Exceldatei: " + ex.Message);
						}
					}
				}
				else
				{
					MessageBox.Show("Zielspalte nicht gefunden.");
				}
			}
		}

		private void dataGridGigs_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
		{
			if (e.AddedItems.Count > 0)
			{
				ReportedGig ca = e.AddedItems[0] as ReportedGig;

				//TextBlockRelatedGig.Text = ca.DayOfShow.ToShortDateString() + " " + ca.Venue.City + ": " + ca.Artist;

				//dataGrid1.DataContext = ca.ViewCorrectAccounted;
				//dataGrid2.DataContext = ca.ViewNotAccounted;
				//dataGrid3.DataContext = ca.ViewNotReported;
				//dataGrid4.DataContext = ca.ViewWrongCategory;

				Manager.instance.SelectTab(tabControlEvaluation, "tabItemWorksPerGig");
				e.Handled = true;
			}
		}

		private void TextBlockRelatedGig_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			Manager.instance.SelectTab(tabControlEvaluation, "tabItemGigs");
			e.Handled = true;
		}


		private void TextBlock_CandidatesFor_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			Manager.instance.SelectTab(tabControl_NotAccountedGigs, "tabItem_NotAccountedGigs");
			e.Handled = true;
		}

		private void dataGrid_NotCompletelyAccountedGigs_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			ReportedGig gig = dataGrid_NotCompletelyAccountedGigs.SelectedItem as ReportedGig;

			if (gig != null)
			{
				dataGrid_NotAccountedWorks.DataContext = gig.Works;
				TextBlockRelatedGig.Text = "« " + gig.DayOfShow.ToShortDateString() + " " + gig.Venue.Display + ": " + gig.ArtistName;

				Manager.instance.SelectTab(tabControl_NotCompletelyAccountedGigs, "tabItemWorksPerGig");
				e.Handled = true;
			}
		}

		private void dataGrid_AccountedButNotReportedGigs_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			Gig gig = dataGrid_AccountedButNotReportedGigs.SelectedItem as Gig;

			if (gig != null)
			{
				dataGrid_AccountedButNotReportedWorks.DataContext = gig.Works;
				TextBlock_AccountedButNotReportedGig.Text = "« " + gig.DayOfShow.ToShortDateString() + " " + gig.Venue.Display;

				Manager.instance.SelectTab(tabControl_AccountedButNotReported, "tabItem_WorksPerNotReportedGig");
				e.Handled = true;
			}
		}

		private void TextBlock_AccountedButNotReportedGig_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			Manager.instance.SelectTab(tabControl_AccountedButNotReported, "tabItem_AccountedButNotReportedGigs");
			e.Handled = true;
		}

		private void dataGrid_NotAccounted_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			ReportedGig selectedGig = dataGrid_NotAccounted.SelectedItem as ReportedGig;

			if (selectedGig != null)
			{
				TextBlock_CandidatesFor.Text = "Eventueller Treffer für Gig von " + selectedGig.ArtistName + ": " + selectedGig.DayOfShow.ToShortDateString() + " in " + selectedGig.Venue.Display;
				dataGrid_NotAccounted_Candidates.DataContext = selectedGig.Candidates;
				Manager.instance.SelectTab(tabControl_NotAccountedGigs, "tabItem_NotAccounted_Candidates");
				e.Handled = true;
			}

			_gig4Alternatives = selectedGig;
		}
	}
}