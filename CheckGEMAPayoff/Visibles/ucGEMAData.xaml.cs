﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Collections.ObjectModel;
using CheckGEMAPayoff.Classes;
using System.ComponentModel;
using CheckGEMAPayoff.Check;
using CheckGEMAPayoff.GEMA_Accounting;

namespace CheckGEMAPayoff
{
	/// <summary>
	/// Interaction logic for ucGEMAData.xaml
	/// </summary>
	public partial class ucGEMAData : UserControl
	{
		private const string GEMA_CSV_SEPERATOR = ";";

		private ObservableCollection<string> _errorLog;
		private ObservableCollection<string> _infoLog;
		//private ICollectionView _viewBeneficiary;
		//private ICollectionView _viewWorkIdentification;
		//private ICollectionView _viewUsage;

		public ucGEMAData()
		{
			this.InitializeComponent();

			_errorLog = new ObservableCollection<string>();
			listBoxErrorLog.ItemsSource = _errorLog;
			_infoLog = new ObservableCollection<string>();
			listBoxInfoLog.ItemsSource = _infoLog;
			//dataGridPayoffSet.DataContext = Manager.instance.GEMAPayoffSet;
		}

		public ObservableCollection<string> ErrorLog
		{
			get { return _errorLog; }
		}

		//private void BindGrids()
		//{
		//  _viewBeneficiary = CollectionViewSource.GetDefaultView(Manager.instance.GEMABeneficiary);
		//  _viewBeneficiary.Filter = PredicateBeneficiary;
		//  dataGridBeneficiary.DataContext = _viewBeneficiary;
		//  _viewWorkIdentification = CollectionViewSource.GetDefaultView(Manager.instance.GEMAWorkIdentification);
		//  _viewWorkIdentification.Filter = PredicateWorkIdentification;
		//  dataGridWorkIdentification.DataContext = _viewWorkIdentification;
		//  _viewUsage = CollectionViewSource.GetDefaultView(Manager.instance.GEMAUsage);
		//  _viewUsage.Filter = PredicateUsage;
		//  dataGridWorkUsage.DataContext = _viewUsage;
		//}

		private void buttonLoadGEMAFile_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog1 = new OpenFileDialog();

			string startFolder = Manager.instance.Configuration.PathGEMAFile;
			if (!string.IsNullOrWhiteSpace(startFolder) && System.IO.Directory.Exists(startFolder))
				openFileDialog1.InitialDirectory = startFolder;

			openFileDialog1.Multiselect = true;
			openFileDialog1.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
			if (openFileDialog1.ShowDialog().Value)
			{
        Manager.instance.Configuration.PathGEMAFile = openFileDialog1.FileName.Substring(0, openFileDialog1.FileName.LastIndexOf("\\"));
        Manager.instance.Configuration.SaveConfiguration(Manager.CONFIGURATION_FILENAME, new DevFsFunctions.Logging.Log());

				foreach (string file in openFileDialog1.FileNames)
					LoadGEMADataFromCSV(file);
			}

			//LoadGigList();
			//dataGridBeneficiary.DataContext = Manager.instance.AccountingSet;
			dataGridAccountedGigs.DataContext = Manager.instance.AccountingSet;

			MessageBox.Show(Manager.instance.AccountingSet.Count + " abgerechnete Gigs geladen", "Import GEMA Abrechnungsdaten", MessageBoxButton.OK, MessageBoxImage.Information);

			Manager.instance.SelectTab(tabControlDisplay, "tabItemAccountedGigs");
		}

		//private void LoadGigList()
		//{
		//  CheckGigs checkGigs = new CheckGigs();
		//  checkGigs.CreateAccountedGigsList();
		//  dataGridAccountedGigs.DataContext = checkGigs.accountedGigList;
		//}

		private void LoadGEMADataFromCSV(string filename)
		{
			//try
			//{
				using (StreamReader sr = new StreamReader(filename))
				{
					IGEMA_Accounting_Importer importer = Manager.instance.GetImporterGEMA(sr);

					if (importer != null)
					{
						DateTime start = DateTime.Now;
						_infoLog.Add("Starte Import der GEMA-Daten mittels Importer '" + importer.Name + "' aus Datei " + filename + " " + start.ToString());

						foreach (Gig gig in importer.Get_GEMA_Accounting(_infoLog, _errorLog, sr))
							Manager.instance.AccountingSet.Add(gig);

						_infoLog.Add("Import der GEMA-Daten aus Datei " + filename + " " + DateTime.Now.ToString() + " abgeschlossen. Benötigte Zeit: " + (DateTime.Now - start).Milliseconds + " Millisekunden.");
					}
					//BindGrids();
				}
			//}
			//catch (Exception e)
			//{
			//  _errorLog.Add("Exception: " + e.Message);

			//  MessageBox.Show("Import nicht erfolgreich. Nähere Informationen im Fehlerlog.", "Import FEHLGESCHLAGEN!", MessageBoxButton.OK, MessageBoxImage.Error);
			//  Manager.instance.SelectTab(tabControlDisplay, "tabItemErrorLog");

			//}
		}

		//#region Import
		//private void ReadGEMALine(string line, int lineCount)
		//{
		//  // split the data
		//  string[] lineSplit = line.Split(GEMA_CSV_SEPERATOR.ToCharArray());

		//  if (lineSplit.Length > 0)
		//  {
		//    // depending on the set type
		//    switch (lineSplit[0])
		//    {
		//      case "01": ReadCategory(lineSplit, lineCount);
		//        break;
		//      case "02": ReadMainAccount(lineSplit, lineCount);
		//        break;
		//      case "03": ReadWorkIdentification(lineSplit, lineCount);
		//        break;
		//      case "04": ReadUsageReport(lineSplit, lineCount);
		//        break;
		//      default:
		//        break;
		//    }
		//  }

		//}

		//private void ReadCategory(string[] lineSplit, int lineCount)
		//{
		//  if (lineSplit.Length != 5)
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Es wurden 5 Felder erwartet, aber " + lineSplit.Length.ToString() + " gefunden.");
		//    return;
		//  }

		//  GEMA_PayoffSet data = new GEMA_PayoffSet();
			
		//  //id
		//  int id = -1;
		//  if (!Int32.TryParse(lineSplit[1], out id))
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID: " + lineSplit[1] + " ist keine Zahl.");
		//  }
		//  data.id = id;

		//  // sparte
		//  if (lineSplit[2].Length < 3)
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Sparte: " + lineSplit[2] + " ist zu kurz.");
		//  }
		//  else
		//    data.Category = lineSplit[2].Substring(1, lineSplit[2].Length - 2);

		//  // datum
		//  DateTime creationDate = ConvertGEMADateTime(lineSplit[3]);
		//  if (creationDate == DateTime.MinValue)
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Datum: " + lineSplit[3] + " konnte nicht gelesen werden.");
		//  }
		//  data.PayoffDate = creationDate;

		//  // bemerkung
		//  data.Description = lineSplit[4];

		//  Manager.instance.GEMAPayoffSet.Add(data);
		//}

		//private void ReadMainAccount(string[] lineSplit, int lineCount)
		//{
		//  if (lineSplit.Length != 7)
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Es wurden 7 Felder erwartet, aber " + lineSplit.Length.ToString() + " gefunden.");
		//    return;
		//  }

		//  GEMA_Beneficiary bene = new GEMA_Beneficiary();

		//  //id
		//  int id = -1;
		//  if (!Int32.TryParse(lineSplit[1], out id))
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID: " + lineSplit[1] + " ist keine Zahl.");
		//  }
		//  bene.id = id;

		//  //Überkonto
		//  int aboveAccount = -1;
		//  if (!Int32.TryParse(lineSplit[3], out aboveAccount))
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Überkontonummer: " + lineSplit[3] + " ist keine Zahl.");
		//  }
		//  bene.AboveAccountNumber = aboveAccount;

		//  //Hauptkontonummer
		//  int mainAccount = -1;
		//  if (!Int32.TryParse(lineSplit[4], out mainAccount))
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Hauptkontonummer: " + lineSplit[4] + " ist keine Zahl.");
		//  }
		//  bene.MainAccountNumber = mainAccount;

		//  // Hauptkontoinhaber
		//  if (lineSplit[5].Length < 3)
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Hauptkontoinhaber: " + lineSplit[5] + " ist zu kurz.");
		//  }
		//  else
		//    bene.MainAccountOwner = lineSplit[5].Substring(1, lineSplit[5].Length - 2);

		//  // bemerkung
		//  bene.Description = lineSplit[6];

		//  // Zugehörige Abrechnung
		//  int idRoot = -1;
		//  if (!Int32.TryParse(lineSplit[2], out idRoot))
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID Satzart 01: " + lineSplit[2] + " ist keine Zahl.");
		//  }

		//  bene.PayoffSet = Manager.instance.GetGEMAPayoffSet(idRoot);

		//  Manager.instance.GEMABeneficiary.Add(bene);
		//}

		//private void ReadWorkIdentification(string[] lineSplit, int lineCount)
		//{
		//  if (lineSplit.Length != 8)
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Es wurden 8 Felder erwartet, aber " + lineSplit.Length.ToString() + " gefunden.");
		//    return;
		//  }

		//  GEMA_WorkIdentification wi = new GEMA_WorkIdentification();

		//  //id
		//  int id = -1;
		//  if (!Int32.TryParse(lineSplit[1], out id))
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID: " + lineSplit[1] + " ist keine Zahl.");
		//  }
		//  wi.ID = id;

		//  //werknummer
		//  int worknumber = -1;
		//  if (!Int32.TryParse(lineSplit[3], out worknumber))
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Werknummer: " + lineSplit[3] + " ist keine Zahl.");
		//  }
		//  wi.WorkNumber = worknumber;

		//  //fassungsnummer
		//  int copynumber = -1;
		//  if (!Int32.TryParse(lineSplit[4], out copynumber))
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Fassungsnummer: " + lineSplit[4] + " ist keine Zahl.");
		//  }
		//  wi.Version = copynumber;

		//  // Werktitel
		//  if (lineSplit[5].Length < 3)
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Werktitel: " + lineSplit[5] + " ist zu kurz.");
		//  }
		//  else
		//    wi.Title = lineSplit[5].Substring(1, lineSplit[5].Length - 2);

		//  // Komponist
		//  if (lineSplit[6].Length < 3)
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Komponist: " + lineSplit[6] + " ist zu kurz.");
		//  }
		//  else
		//    wi.Composer = lineSplit[6].Substring(1, lineSplit[6].Length - 2);

		//  // Editor
		//  if (lineSplit[5].Length < 2)
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Bearbeiter: " + lineSplit[7] + " ist zu kurz.");
		//  }
		//  else if (lineSplit[5].Length > 2)
		//    wi.Editor = lineSplit[7].Substring(1, lineSplit[7].Length - 2);

		//  // Zugehöriger Berechtigter
		//  int idRoot = -1;
		//  if (!Int32.TryParse(lineSplit[2], out idRoot))
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID Satzart 02: " + lineSplit[2] + " ist keine Zahl.");
		//  }
		//  else
		//    wi.Beneficiary = Manager.instance.GetGEMABeneficiary(idRoot);

		//  Manager.instance.GEMAWorkIdentification.Add(wi);
		//}

		//private void ReadUsageReport(string[] lineSplit, int lineCount)
		//{
		//  // only Category "U" implemented yet
		//  if (lineSplit.Length != 9)
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Es wurden 9 Felder erwartet, aber " + lineSplit.Length.ToString() + " gefunden.");
		//    return;
		//  }

		//  if (lineSplit[3] != "\"U\"" && lineSplit[3] != "\"UD\"" && lineSplit[3] != "\"M\"")
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Bisher werden nur die Abrechnungsarten \"U\", \"UD\" und \"M\" unterstützt.");
		//    return;
		//  }

		//  GEMA_Usage usage = new GEMA_Usage();

		//  //id
		//  int id = -1;
		//  if (!Int32.TryParse(lineSplit[1], out id))
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID: " + lineSplit[1] + " ist keine Zahl.");
		//  }
		//  usage.id = id;

		//  // sparte
		//  if (lineSplit[3].Length < 3)
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Sparte: " + lineSplit[3] + " ist zu kurz.");
		//  }
		//  else
		//    usage.Category = lineSplit[3].Substring(1, lineSplit[3].Length - 2);


		//  // datum
		//  DateTime showDate = ConvertGEMADate(lineSplit[4]);
		//  if (showDate == DateTime.MinValue)
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Datum: " + lineSplit[4] + " konnte nicht gelesen werden.");
		//  }
		//  else
		//    usage.DayOfShow = showDate;

		//  // Venue
		//  if (lineSplit[6].Length < 3)
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". Ort: " + lineSplit[6] + " ist zu kurz.");
		//  }
		//  else
		//    usage.Venue.City = lineSplit[6].Substring(1, lineSplit[6].Length - 2);

		//  // VA-Raum
		//  if (lineSplit[7].Length >= 3)
		//    usage.Venue.Name = lineSplit[7].Substring(1, lineSplit[7].Length - 2);

		//  // VA-Raum
		//  if (lineSplit[8].Length >= 3)
		//    usage.Organiser.Name = lineSplit[8].Substring(1, lineSplit[8].Length - 2);

		//  // Zugehöriges Werk
		//  int idRoot = -1;
		//  if (!Int32.TryParse(lineSplit[2], out idRoot))
		//  {
		//    _errorLog.Add("Fehler in Zeile " + lineCount.ToString() + ". ID Satzart 03: " + lineSplit[2] + " ist keine Zahl.");
		//  }
		//  else
		//    usage.WorkIdentification = Manager.instance.GetGEMAWorkIdentification(idRoot);

		//  Manager.instance.GEMAUsage.Add(usage);
		//}
		//#endregion

		#region Helper
		private DateTime ConvertGEMADateTime(string p)
		{
			if (p.Length < 3)
				return DateTime.MinValue;

			string[] tmp = p.Substring(1, p.Length -2).Split(" ".ToCharArray());

			if (tmp.Length != 2)
				return DateTime.MinValue;

			int year;
			if (!Int32.TryParse(tmp[0].Substring(0, 4), out year))
				return DateTime.MinValue;
			int day;
			if (!Int32.TryParse(tmp[0].Substring(4, 2), out day))
				return DateTime.MinValue;
			int month;
			if (!Int32.TryParse(tmp[0].Substring(6, 2), out month))
				return DateTime.MinValue;

			int hour;
			if (!Int32.TryParse(tmp[1].Substring(0, 2), out hour))
				return DateTime.MinValue;
			int minute;
			if (!Int32.TryParse(tmp[1].Substring(3, 2), out minute))
				return DateTime.MinValue;
			int second;
			if (!Int32.TryParse(tmp[1].Substring(6, 2), out second))
				return DateTime.MinValue;

			DateTime result = DateTime.MinValue;

			try
			{
				result = new DateTime(year, month, day, hour, minute, second);
			}
			catch (Exception ex)
			{
				_errorLog.Add("Fehler bei der Datumskonvertierung: Die Zeichenkette '" + p + "' konnte nicht als Datum interpretiert werden: " + ex.Message);
			}

			return result;
		}

		private DateTime ConvertGEMADate(string p)
		{
			if (p.Length < 3)
				return DateTime.MinValue;

			int year;
			if (!Int32.TryParse(p.Substring(7, 4), out year))
				return DateTime.MinValue;
			int month;
			if (!Int32.TryParse(p.Substring(4, 2), out month))
				return DateTime.MinValue;
			int day;
			if (!Int32.TryParse(p.Substring(1, 2), out day))
				return DateTime.MinValue;

			return new DateTime(year, month, day);
		}

		#endregion

		#region Navigation
		//private bool PredicateBeneficiary(object bene)
		//{
		//  if (dataGridPayoffSet.SelectedItem == null)
		//    return false;

		//  GEMA_PayoffSet eq = (bene as GEMA_Beneficiary).PayoffSet;
		//  if ((dataGridPayoffSet.SelectedItem).Equals(eq))
		//    return true;
		//  else
		//    return false;
		//}

		//private bool PredicateWorkIdentification(object wi)
		//{
		//  if (dataGridBeneficiary.SelectedItem == null)
		//    return false;

		//  GEMA_Beneficiary eq = (wi as GEMA_WorkIdentification).Beneficiary;
		//  if ((dataGridBeneficiary.SelectedItem).Equals(eq))
		//    return true;
		//  else
		//    return false;
		//}

		//private bool PredicateUsage(object usage)
		//{
		//  if (dataGridWorkIdentification.SelectedItem == null)
		//    return false;

		//  GEMA_WorkIdentification eq = (usage as GEMA_Usage).WorkIdentification;
		//  if ((dataGridWorkIdentification.SelectedItem).Equals(eq))
		//    return true;
		//  else
		//    return false;
		//}

		//private void dataGridBeneficiary_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		//{
		//  Beneficiary item = dataGridBeneficiary.SelectedItem as Beneficiary;

		//  if (item != null)
		//  {
		//    dataGridAccountedGigs.DataContext = item.GEMA_Accounting;
		//    //_viewWorkIdentification.Refresh();
		//    Manager.instance.SelectTab(tabControlDisplay, "tabItemAccountedGigs");
		//    textBlockBeneficiary.Text = item.MainAccountOwner;

		//    e.Handled = true;
		//  }
		//}

		//private void dataGridPayoffSet_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		//{
		//  GEMA_PayoffSet item = dataGridPayoffSet.SelectedItem as GEMA_PayoffSet;

		//  if (item != null)
		//  {
		//    _viewBeneficiary.Refresh();
		//    Manager.instance.SelectTab(tabControlDisplay, "tabItemBeneficiary");
		//    textBlockPayoffSet.Text = item.Description;

		//    e.Handled = true;
		//  }
		//}

		//private void dataGridWorkIdentification_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		//{
		//  GEMA_WorkIdentification item = dataGridWorkIdentification.SelectedItem as GEMA_WorkIdentification;

		//  if (item != null)
		//  {
		//    _viewUsage.Refresh();
		//    Manager.instance.SelectTab(tabControlDisplay, "tabItemUsage");
		//    textBlockWerk.Text = item.Title + " (" + item.WorkNumber.ToString() + ")";

		//    e.Handled = true;
		//  }
		//}

		private void dataGridAccountedGigs_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			Gig item = dataGridAccountedGigs.SelectedItem as Gig;

			if (item != null)
			{
        textBlockGig.Text = item.DayOfShow.ToShortDateString() + " | " + item.Venue.Display;
				dataGridWorkPerGig.DataContext = item.Works;
				Manager.instance.SelectTab(tabControlDisplay, "tabItemWorkPerGig");

				e.Handled = true;
			}
		}

		#endregion
	}
}