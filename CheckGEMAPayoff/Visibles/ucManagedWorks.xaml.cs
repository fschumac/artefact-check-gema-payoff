﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using Microsoft.Win32;
using System.Collections.ObjectModel;
using CheckGEMAPayoff.Classes;

namespace CheckGEMAPayoff
{
	/// <summary>
	/// Interaction logic for ucManagedWorks.xaml
	/// </summary>
	public partial class ucManagedWorks : UserControl
	{
		private int _count;

		public ucManagedWorks()
		{
			this.InitializeComponent();

			dataGridCatalog.DataContext = Manager.instance.ManagedWorks;
		}

		private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			_count = 0;
			Manager.instance.ManagedWorks.Clear();

			OpenFileDialog openFileDialog1 = new OpenFileDialog();

			string startFolder = Manager.instance.Configuration.PathManagedWorksFiles;
			if (startFolder.Length > 0 && System.IO.Directory.Exists(startFolder))
				openFileDialog1.InitialDirectory = startFolder;


			openFileDialog1.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
			openFileDialog1.Multiselect = true;
			if (openFileDialog1.ShowDialog().Value)
			{
        Manager.instance.Configuration.PathManagedWorksFiles = openFileDialog1.FileName.Substring(0, openFileDialog1.FileName.LastIndexOf("\\"));
        Manager.instance.Configuration.SaveConfiguration(Manager.CONFIGURATION_FILENAME, new DevFsFunctions.Logging.Log());

				foreach (string fName in openFileDialog1.FileNames)
					LoadGEMAPublisherXML(fName);
			}

			Manager.instance.SelectTab(tabControlDisplay, "tabItemCatalog");
			MessageBox.Show(_count.ToString() + " Tracks geladen.");
		}

		private void LoadGEMAPublisherXML(string fileName)
		{
			try
			{
				XmlReader reader;
				reader = XmlReader.Create(fileName);

				while (reader.Read())
				{
					if (reader.NodeType == XmlNodeType.Element)
						if (reader.Name == "WORK")
						{
							ReadElement_Work(reader);
							_count++;
						}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Beim Importieren der Daten der verwalteten Werke ist folgender Fehler aufgetreten: " + Environment.NewLine + ex.Message, "Fehler beim Import", MessageBoxButton.OK, MessageBoxImage.Error);
				Manager.instance.ManagedWorks.Clear();
			}
		}

		private void ReadElement_Work(XmlReader reader)
		{
			string workcode = String.Empty;
			string title = String.Empty;
			string hh = String.Empty;
			string mm = String.Empty;
			string ss = String.Empty;

			while (reader.Read())
			{
				if (reader.NodeType == XmlNodeType.Element)
				{
					if (reader.Name == "SOCWORKCDE")
						workcode = reader.ReadString();
					if (reader.Name == "DURHH")
						hh = reader.ReadString();
					if (reader.Name == "DURMM")
						mm = reader.ReadString();
					if (reader.Name == "DURSS")
						ss = reader.ReadString();
					else if (reader.Name == "TITLES")
						title = ReadElement_Titles(reader);
				}
				else if (reader.NodeType == XmlNodeType.EndElement)
					if (reader.Name == "WORK")
					{
						string[] splitWorkCode = workcode.Split("-".ToCharArray());

						Work mWork = new Work();
						mWork.Title = title;
						mWork.Version = Convert.ToInt32(splitWorkCode[1]);
						mWork.WorkNumber = Convert.ToInt32(splitWorkCode[0]);
						//mWork.Duration = GetTimeSpan(hh, mm, ss);

						Manager.instance.ManagedWorks.Add(mWork);

						return;
					}
			}
		}

		private string ReadElement_Titles(XmlReader reader)
		{
			string title = String.Empty;

			while (reader.Read())
			{
				if (reader.NodeType == XmlNodeType.Element)
				{
					if (reader.Name == "TITLE")
					{
						string tmp = ReadElement_Title(reader);
						if (tmp != String.Empty)
							title = tmp;
					}
				}
				else if (reader.NodeType == XmlNodeType.EndElement)
					if (reader.Name == "TITLES")
						return title;
			}

			return string.Empty;
		}

		private string ReadElement_Title(XmlReader reader)
		{
			string title = string.Empty;
			string type = string.Empty;

			while (reader.Read())
			{
				if (reader.NodeType == XmlNodeType.Element)
				{
					if (reader.Name == "TIT")
					{
						title = reader.ReadString();
					}
					else if (reader.Name == "TYPE")
					{
						type = reader.ReadString();
					}
				}
				else if (reader.NodeType == XmlNodeType.EndElement)
				{
					if (reader.Name == "TITLE")
					{
						if (type == "OT")
							return title;
						else
							return String.Empty;
					}
				}
			}

			return String.Empty;
		}


		private TimeSpan GetTimeSpan(string hh, string mm, string ss)
		{
			int h;
			int m;
			int s;

			if (!Int32.TryParse(hh, out h))
				return new TimeSpan();
			if (!Int32.TryParse(mm, out m))
				return new TimeSpan();
			if (!Int32.TryParse(ss, out s))
				return new TimeSpan();

			return new TimeSpan(h, m, s);
		}

	}
}