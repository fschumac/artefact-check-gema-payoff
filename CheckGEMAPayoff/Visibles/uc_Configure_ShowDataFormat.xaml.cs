﻿using CheckGEMAPayoff.Properties;
using DevFsExcel;
using DevFsFunctions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckGEMAPayoff
{
	/// <summary>
	/// Interaktionslogik für uc_Configure_ShowDataFormat.xaml
	/// </summary>
	public partial class uc_Configure_ShowDataFormat : UserControl
	{
		public uc_Configure_ShowDataFormat()
		{
			this.InitializeComponent();
		}

		private void Button_Save_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			if (CheckUserInput())
			{
				int value;

        Manager.instance.Configuration.ConcertlistSheetName = TextBox_SheetName.Text;

        Manager.instance.Configuration.Column_ShowTime = TextBox_ShowTime.Text;

        Manager.instance.Configuration.Column_SubmissionDate = TextBox_Einreichungsdatum.Text;

        Manager.instance.Configuration.Column_SubmissionType = TextBox_Einreichungsform.Text;

        Manager.instance.Configuration.Column_EventName = TextBox_EventName.Text;

				Int32.TryParse(TextBox_FirstDataRow.Text, out value);
        Manager.instance.Configuration.FirstDataRow = value - 1;

        Manager.instance.Configuration.Column_OrganiserCity = TextBox_OrganiserCity.Text;

        Manager.instance.Configuration.Column_OrganiserName = TextBox_OrganiserName.Text;

        Manager.instance.Configuration.Column_OrganiserStreet = TextBox_OrganiserStreet.Text;

        Manager.instance.Configuration.Column_ShowDate = TextBox_ShowDate.Text;

        Manager.instance.Configuration.Column_SetlistFileName = TextBox_TracklistNumber.Text;

        Manager.instance.Configuration.Column_VenueCity = TextBox_VenueCity.Text;

        Manager.instance.Configuration.Column_VenueName = TextBox_VenueName.Text;

        Manager.instance.Configuration.Column_VenueStreet = TextBox_VenueStreet.Text;

        Manager.instance.Configuration.Column_Artist = TextBox_Artistname.Text;

        Manager.instance.Configuration.Column_Bandleader = TextBox_Bandleader.Text;

        Manager.instance.Configuration.Column_EventType = TextBox_EventType.Text;

        Log log = new Log();
        Manager.instance.Configuration.SaveConfiguration(Manager.CONFIGURATION_FILENAME, log);

        if (log.Count == 0)
          MessageBox.Show("Einstellungen erfolgreich gespeichert.");
        else
          MessageBox.Show(log.ToString(), "Error saving configuration", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		private bool CheckUserInput()
		{
      Log log = new Log();

			int test;
			if (!ExcelFunctions.IsValidColumnString(TextBox_ShowTime.Text, false))
  			log.Add("Spalte ShowTime: [" + TextBox_ShowTime.Text + "] ist keine gültige Excelspalte");

			if (!ExcelFunctions.IsValidColumnString(TextBox_Einreichungsdatum.Text, false))
				log.Add("Spalte Einreichungsdatum: [" + TextBox_Einreichungsdatum.Text + "] ist keine gültige Excelspalte");

			if (!ExcelFunctions.IsValidColumnString(TextBox_Einreichungsform.Text, false))
        log.Add("Spalte Einreichungsform: [" + TextBox_Einreichungsform.Text + "] ist keine gültige Excelspalte");

			if (!ExcelFunctions.IsValidColumnString(TextBox_EventName.Text, false))
        log.Add("Spalte Eventname: [" + TextBox_EventName.Text + "] ist keine gültige Excelspalte");

			if (!int.TryParse(TextBox_FirstDataRow.Text, out test))
        log.Add("Erste Zeile mit Daten: [" + TextBox_FirstDataRow.Text + "] muss eine Zahl sein.");
      else if (test <= 0)
        log.Add("Erste Zeile mit Daten: [" + TextBox_FirstDataRow.Text + "] muss eine Zahl größer als Null sein.");

      if (!ExcelFunctions.IsValidColumnString(TextBox_OrganiserCity.Text, false))
        log.Add("Spalte Veranstalter Ort: [" + TextBox_OrganiserCity.Text + "] ist keine gültige Excelspalte");

			if (!ExcelFunctions.IsValidColumnString(TextBox_OrganiserName.Text, false))
        log.Add("Spalte Veranstaltername: [" + TextBox_OrganiserName.Text + "] ist keine gültige Excelspalte");

			if (!ExcelFunctions.IsValidColumnString(TextBox_OrganiserStreet.Text, false))
        log.Add("Spalte Veranstalter Straße: [" + TextBox_OrganiserStreet.Text + "] ist keine gültige Excelspalte");

			if (!ExcelFunctions.IsValidColumnString(TextBox_ShowDate.Text, false))
        log.Add("Spalte Aufführungstag: [" + TextBox_ShowDate.Text + "] ist keine gültige Excelspalte");

			if (!ExcelFunctions.IsValidColumnString(TextBox_TracklistNumber.Text, false))
        log.Add("Spalte Titellistennummer: [" + TextBox_TracklistNumber.Text + "] ist keine gültige Excelspalte");

			if (!ExcelFunctions.IsValidColumnString(TextBox_VenueCity.Text, false))
        log.Add("Spalte Aufführungsort: [" + TextBox_VenueCity.Text + "] ist keine gültige Excelspalte");

			if (!ExcelFunctions.IsValidColumnString(TextBox_VenueName.Text, false))
        log.Add("Spalte Venue Name: [" + TextBox_VenueName.Text + "] ist keine gültige Excelspalte");

			if (!ExcelFunctions.IsValidColumnString(TextBox_VenueStreet.Text, false))
        log.Add("Spalte Venue Straße: [" + TextBox_VenueStreet.Text + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(TextBox_Artistname.Text, false))
        log.Add("Spalte Artist: [" + TextBox_Artistname.Text + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(TextBox_Bandleader.Text, false))
        log.Add("Spalte Musikalischer Leiter: [" + TextBox_Bandleader.Text + "] ist keine gültige Excelspalte");

      if (!ExcelFunctions.IsValidColumnString(TextBox_EventType.Text, false))
        log.Add("Spalte Art der Veranstaltung: [" + TextBox_EventType.Text + "] ist keine gültige Excelspalte");

      if (log.Count > 0)
			{
				MessageBox.Show(log.ToString(), "Fehler bei der Konfiguration", MessageBoxButton.OK, MessageBoxImage.Error);
				return false;
			}

			return true;
		}

		private void Button_Reset_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			LoadConfiguration();
		}

		public void LoadConfiguration()
		{
			TextBox_SheetName.Text = Manager.instance.Configuration.ConcertlistSheetName;

			TextBox_ShowTime.Text = Manager.instance.Configuration.Column_ShowTime;

			TextBox_Einreichungsdatum.Text = Manager.instance.Configuration.Column_SubmissionDate;

			TextBox_Einreichungsform.Text = Manager.instance.Configuration.Column_SubmissionType;

			TextBox_EventName.Text = Manager.instance.Configuration.Column_EventName;

			TextBox_FirstDataRow.Text = (Manager.instance.Configuration.FirstDataRow + 1).ToString();

			TextBox_OrganiserCity.Text = Manager.instance.Configuration.Column_OrganiserCity;

			TextBox_OrganiserName.Text = Manager.instance.Configuration.Column_OrganiserName;

			TextBox_OrganiserStreet.Text = Manager.instance.Configuration.Column_OrganiserStreet;

			TextBox_ShowDate.Text = Manager.instance.Configuration.Column_ShowDate;

			TextBox_TracklistNumber.Text = Manager.instance.Configuration.Column_SetlistFileName;

			TextBox_VenueCity.Text = Manager.instance.Configuration.Column_VenueCity;

			TextBox_VenueName.Text = Manager.instance.Configuration.Column_VenueName;

			TextBox_VenueStreet.Text = Manager.instance.Configuration.Column_VenueStreet;

      TextBox_Artistname.Text = Manager.instance.Configuration.Column_Artist;

      TextBox_EventType.Text = Manager.instance.Configuration.Column_EventType;

      TextBox_Bandleader.Text = Manager.instance.Configuration.Column_Bandleader;

    }
  }
}