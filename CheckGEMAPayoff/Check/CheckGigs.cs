﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CheckGEMAPayoff.Classes;
using System.Collections.ObjectModel;

namespace CheckGEMAPayoff.Check
{
	class CheckGigs
	{
		//private ObservableCollection<Gig> _accountedGigList;
		private ObservableCollection<ReportedGig> _reportedButNotAccountedGigs;
		private ObservableCollection<ReportedGig> _notCompletelyAccountedGigs;
		private ObservableCollection<ReportedGig> _correctAccounted;
		//private ObservableCollection<ReportedGig> _wrongCategory;
		private ObservableCollection<Gig> _accountedButNotReportedGigs;

		//private ObservableCollection<ReportedGig> _correctAccounted_2;
		//private ObservableCollection<ReportedGig> _notAccounted_2;
		//private ObservableCollection<ReportedGig> _missingWorks_2;

		private HashSet<int> _managedTrackWorkNumbers;

		//public ObservableCollection<Gig> accountedGigList
		//{
		//  get { return _accountedGigList; }
		//}

		public ObservableCollection<ReportedGig> ReportedButNotAccountedGigs
		{
			get { return _reportedButNotAccountedGigs; }
		}

		public ObservableCollection<ReportedGig> NotCompletelyAccountedGigs
		{
			get { return _notCompletelyAccountedGigs; }
		}

		public ObservableCollection<ReportedGig> CorrectAccounted
		{
			get { return _correctAccounted; }
			set { _correctAccounted = value; }
		}

		//public ObservableCollection<ReportedGig> WrongCategory
		//{
		//	get { return _wrongCategory; }
		//	set { _wrongCategory = value; }
		//}

		public ObservableCollection<Gig> AccountedButNotReportedGigs
		{
			get { return _accountedButNotReportedGigs; }
		}

		//public ObservableCollection<ReportedGig> CorrectAccounted_2
		//{
		//  get { return _correctAccounted_2; }
		//  set { _correctAccounted_2 = value; }
		//}

		//public ObservableCollection<ReportedGig> NotAccounted_2
		//{
		//  get { return _notAccounted_2; }
		//  set { _notAccounted_2 = value; }
		//}

		//public ObservableCollection<ReportedGig> MissingWorks_2
		//{
		//  get { return _missingWorks_2; }
		//  set { _missingWorks_2 = value; }
		//}

		public CheckGigs()
		{
			//_accountedGigList = new ObservableCollection<Gig>();
			_managedTrackWorkNumbers = new HashSet<int>();
			_reportedButNotAccountedGigs = new ObservableCollection<ReportedGig>();
			_notCompletelyAccountedGigs = new ObservableCollection<ReportedGig>();
			_correctAccounted = new ObservableCollection<ReportedGig>();
			//_wrongCategory = new ObservableCollection<ReportedGig>();
			_accountedButNotReportedGigs = new ObservableCollection<Gig>();

			//_correctAccounted_2 = new ObservableCollection<ReportedGig>();
			//_notAccounted_2 = new ObservableCollection<ReportedGig>();
			//_missingWorks_2 = new ObservableCollection<ReportedGig>();
		}


		/// <summary>
		/// this function creates a list of all gigs accounted by the gema
		/// make sure to load the gema- and publisher-data first
		/// </summary>
		//public void CreateAccountedGigsList()
		//{
		//  foreach (Gig gig in Manager.instance.GEMA_Accounting)
		//  {
		//    Gig result = (from p in _accountedGigList where p.DayOfShow == gig.DayOfShow && p.Venue.City == gig.Venue.City && p.Venue.Name == gig.Venue.Name select p).FirstOrDefault();

		//    if (result == null)
		//    {
		//      // this gig is not in the list yet => create it
		//      result = new AccountedGig();

		//      result.DayOfShow = gig.DayOfShow;
		//      result.Venue.City = gig.Venue.City;
		//      result.Venue.Name = gig.Venue.Name;
		//      result.Organiser.Name = gig.Organiser.Name;
		//      result.Category = gig.Category;
		//      result.Works = gig.Works;

		//      _accountedGigList.Add(result);
		//    }

		//    // if a usage is accounted for more than one beneficiary (i.e. Label and Artist), just use one Data
		//    //if ((from p in result.Works where p.WorkNumber == gig.WorkNumber select p).FirstOrDefault() == null)
		//    //  result.Usages.Add(usage);
		//  }
		//}

		/// <summary>
		/// This function checks if all reported gigs from the showdata appear in the gema-account-list
		/// make sure to load the gema- and publisher-data first
		/// also make sure to create the giglist first (CheckGigs.CreateGigList())
		/// </summary>
		public void Go(ObservableCollection<Work> managedWorks, ObservableCollection<ReportedGig> reportedGigs, ObservableCollection<Gig> accountedGigs)
		{
			// refresh the worknumber Hash
			//_managedTrackWorkNumbers.Clear();
			//foreach (Work mWork in managedWorks)
			//{
			//  if (!_managedTrackWorkNumbers.Contains(mWork.WorkNumber))
			//    _managedTrackWorkNumbers.Add(mWork.WorkNumber);
			//}

			// CheckEachPerformedWork(reportedGigs, accountedGigs);

			CheckForCompletelyNotAccountedGigs(reportedGigs, accountedGigs);

			CheckForPartiallyNotAccountedGigs(reportedGigs, accountedGigs);

			// CheckForWrongCategory(reportedGigs, accountedGigs);

			AddPossibleAlernatives(accountedGigs);

			CheckForAccountedButNotReportetGigs(reportedGigs, accountedGigs);
		}

		//private void CheckEachPerformedWork(ObservableCollection<ReportedGig> reportedGigs, ObservableCollection<Gig> accountedGigs)
		//{
		//  ObservableCollection<CheckItem> reportetCheckList = CreateCheckList4ReportedGigs(reportedGigs);
		//  ObservableCollection<CheckItem> accountedCheckList = CreateCheckList4AccountedGigs(accountedGigs);

		//  ObservableCollection<ConfirmedItem> confirmedItems = new ObservableCollection<ConfirmedItem>();
		//  ObservableCollection<UnconfirmedItem> unconfirmedItems = new ObservableCollection<UnconfirmedItem>();

		//  _missingWorks_2.Clear();
		//  _correctAccounted_2.Clear();
		//  _notAccounted_2.Clear();

		//  foreach (CheckItem reportedItem in reportetCheckList)
		//  {
		//    CheckItem accountedItem = (from p in accountedCheckList where Manager.instance.IsInbetween(reportedItem.PerformanceDate, p.PerformanceDate, p.PerformanceDay_Last) && (reportedItem.WorkNumber == p.WorkNumber) select p).FirstOrDefault();
		//    ObservableCollection<UnconfirmedItem> tmpUnconfirmedList = new ObservableCollection<UnconfirmedItem>();

		//    if (accountedItem != null)
		//    {
		//      foreach (Gig gig2Test in reportedItem.Gigs)
		//      {
		//        Gig connectedGig = (from p in accountedItem.Gigs where gig2Test.Venue.Plz == p.Venue.Plz select p).FirstOrDefault();

		//        if (connectedGig != null)
		//        {
		//          // correct accounted work on gig
		//          ConfirmedItem confirmedItem = new ConfirmedItem();
		//          confirmedItem.reportedGig = gig2Test as ReportedGig;
		//          confirmedItem.accountedGig = connectedGig;
		//          confirmedItem.confirmedWork = reportedItem.Work;
		//          confirmedItems.Add(confirmedItem);

		//          // remove the connected Gig from the list
		//          accountedItem.Gigs.Remove(connectedGig);
		//        }
		//        else
		//        {
		//          UnconfirmedItem unconfirmedItem = new UnconfirmedItem();
		//          unconfirmedItem.reportedGig = gig2Test as ReportedGig;
		//          unconfirmedItem.work = reportedItem.Work;

		//          tmpUnconfirmedList.Add(unconfirmedItem);
		//          unconfirmedItems.Add(unconfirmedItem);
		//        }
		//      }

		//      // add remaining accounted gigs to candidate list
		//      foreach (UnconfirmedItem item in tmpUnconfirmedList)
		//        foreach (Gig remainingGig in accountedItem.Gigs)
		//          item.candidates.Add(remainingGig);
		//    }
		//    else
		//    {
		//      foreach (Gig gig2Test in reportedItem.Gigs)
		//      {
		//        UnconfirmedItem unconfirmedItem = new UnconfirmedItem();
		//        unconfirmedItem.reportedGig = gig2Test as ReportedGig;
		//        unconfirmedItem.work = reportedItem.Work;

		//        unconfirmedItems.Add(unconfirmedItem);
		//      }
		//    }
		//  }


		//  foreach (ReportedGig reportedGig in reportedGigs)
		//  {
		//    // check wheter there are accounted works from that gig
		//    List<ConfirmedItem> confirmedWorks = (from p in confirmedItems where p.reportedGig == reportedGig select p).ToList();

		//    if (confirmedWorks.Count == reportedGig.Works.Count)
		//    {
		//      _correctAccounted_2.Add(reportedGig);
		//    }
		//    else if (confirmedWorks.Count == 0)
		//    {
		//      List<UnconfirmedItem> unconfirmedWorks = (from p in unconfirmedItems where p.reportedGig == reportedGig select p).ToList();

		//      foreach (UnconfirmedItem item in unconfirmedWorks)
		//        foreach (Gig gig in item.candidates)
		//          if (!reportedGig.Candidates.Contains(gig))
		//            reportedGig.Candidates.Add(gig);

		//      _notAccounted_2.Add(reportedGig);
		//    }
		//    else
		//    {
		//      _missingWorks_2.Add(reportedGig);
		//    }
		//  }
		//}

		//private void CheckForWrongCategory(ObservableCollection<ReportedGig> reportedGigs, ObservableCollection<Gig> accountedGigs)
		//{
		//	_wrongCategory.Clear();

		//	foreach (ReportedGig gig in reportedGigs)
		//	{
		//		var accountedGigList = (from p in accountedGigs where Manager.instance.IsInbetween(gig.DayOfShow, p.DayOfShow, p.DayOfShow_Last) && p.Venue.Postalcode == gig.Venue.Postalcode select p);
		//		Gig accountedGig = GetTheRightAccountedGig(accountedGigList, gig);

		//		if (accountedGig != null)
		//		{
		//			if (accountedGig.Category != gig.Category)
		//			{
		//				gig.Beneficiaries = accountedGig.Beneficiaries;
		//				WrongCategory.Add(gig);
		//			}
		//		}
		//	}
		//}

		private void CheckForCompletelyNotAccountedGigs(ObservableCollection<ReportedGig> reportedGigs, ObservableCollection<Gig> accountedGigs)
		{
			_reportedButNotAccountedGigs.Clear();

			foreach (ReportedGig gig in reportedGigs)
			{
				var accountedGigList = (from p in accountedGigs where Manager.instance.IsInbetween(gig.DayOfShow, p.DayOfShow, p.DayOfShow_Last) && p.Venue.Postalcode == gig.Venue.Postalcode select p);
				Gig accountedGig = GetTheRightAccountedGig(accountedGigList, gig);

				if (accountedGig == null)
				{
					_reportedButNotAccountedGigs.Add(gig);
				}
			}
		}

		private void AddPossibleAlernatives(ObservableCollection<Gig> accountedGigs)
		{
			foreach (ReportedGig gig2check in _reportedButNotAccountedGigs)
			{
				foreach (Work work2check in gig2check.Works)
				{
					List<Gig> candidatesTmp = (from p in accountedGigs where Manager.instance.IsInbetween(gig2check.DayOfShow, p.DayOfShow, p.DayOfShow_Last) && (from q in p.Works where q.WorkNumber == work2check.WorkNumber select p).FirstOrDefault() != null select p).ToList();

					foreach (Gig candidate in candidatesTmp)
						if (!gig2check.Candidates.Contains(candidate))
							gig2check.Candidates.Add(candidate);
				}
			}
		}

		private void CheckForPartiallyNotAccountedGigs(ObservableCollection<ReportedGig> reportedGigs, ObservableCollection<Gig> accountedGigs)
		{
			_notCompletelyAccountedGigs.Clear();
			_correctAccounted.Clear();

			foreach (ReportedGig gig in reportedGigs)
			{
				// we may not be able to identify an accounted Gig by Date and PLZ
				// in this rare cases, we try to guess, which accounted gig is the fitting one 
				var accountedGigList = (from p in accountedGigs where Manager.instance.IsInbetween(gig.DayOfShow, p.DayOfShow, p.DayOfShow_Last) && p.Venue.Postalcode == gig.Venue.Postalcode select p);
				Gig	accountedGig = GetTheRightAccountedGig(accountedGigList, gig);

				if (accountedGig != null)
				{
					bool workIsMissing = false;

					foreach (Work work in gig.Works)
					{
						Work accountedWork = (from p in accountedGig.Works where EqualWorks(p, work) select p).FirstOrDefault();

						if (accountedWork == null && this.IsThisWork2Check(work))
						{
							// => Werk wurde nicht abgerechnet
							workIsMissing = true;
							ReportedGig gig2Complain = (from p in _notCompletelyAccountedGigs where p.DayOfShow == gig.DayOfShow && p.Venue.Postalcode == gig.Venue.Postalcode && p.ArtistName.Equals(gig.ArtistName) select p).FirstOrDefault();

							if (gig2Complain == null)
							{
								// => Erstes fehlendes Werk des Gigs => neu anlegen
								gig2Complain = new ReportedGig();
								gig2Complain.ArtistName = gig.ArtistName;
								gig2Complain.DayOfShow = gig.DayOfShow;
								gig2Complain.Category = gig.Category;
								gig2Complain.Venue = gig.Venue;
								gig2Complain.Organiser = gig.Organiser;
								gig2Complain.Beneficiaries = gig.Beneficiaries;
								gig2Complain.OnlineTransmission = gig.OnlineTransmission;
								gig2Complain.DayOfTransmission = gig.DayOfTransmission;

								_notCompletelyAccountedGigs.Add(gig2Complain);
							}

							gig2Complain.Works.Add(work);
						}
					}

					if (!workIsMissing)
					{
						//if (gig.Category == accountedGig.Category)
						//{
							gig.Beneficiaries = accountedGig.Beneficiaries;
							_correctAccounted.Add(gig);
						//}
					}
				}
			}
		}

		private Gig GetTheRightAccountedGig(IEnumerable<Gig> candidates, ReportedGig reportedGig)
		{
			int maxConcurrence = 0;
			Gig selectedGig = null;

			if (candidates != null)
			{
				foreach (Gig g in candidates)
				{
					int count = 0;

					foreach (Work w in reportedGig.Works)
					{
						Work isInCandidates = (from p in g.Works where p.WorkNumber == w.WorkNumber select p).FirstOrDefault();

						if (isInCandidates != null) count++;
					}

					if (count > maxConcurrence)
					{
						maxConcurrence = count;
						selectedGig = g;
					}
				}
			}

			return selectedGig;
		}

		private bool AnySongOfMeInThisShow(ObservableCollection<Work> trackList)
		{
			foreach (Work work in trackList)
			{
				foreach (string publisher in Manager.instance.Configuration.AdministeredPublishers)
					if (publisher.Equals(work.Publisher, StringComparison.OrdinalIgnoreCase))
						return true;
			}

			return false;
		}

		//private ObservableCollection<CheckItem> CreateCheckList4ReportedGigs(ObservableCollection<ReportedGig> gigs)
		//{
		//  ObservableCollection<CheckItem> resultSet = new ObservableCollection<CheckItem>();

		//  foreach (ReportedGig gig in gigs)
		//  {
		//    foreach (Work work in gig.Works)
		//    {
		//      CheckItem item = (from p in resultSet where p.WorkNumber == work.WorkNumber && Manager.instance.IsInbetween(p.PerformanceDate, gig.DayOfShow, gig.DayOfShow_Last) select p).FirstOrDefault();

		//      if (item == null)
		//      {
		//        item = new CheckItem();

		//        item.WorkNumber = work.WorkNumber;
		//        item.PerformanceDate = gig.DayOfShow;
		//        item.Work = work;
		//        item.Gigs.Add(gig);

		//        resultSet.Add(item);
		//      }
		//      else
		//      {
		//        item.Gigs.Add(gig);
		//      }
		//    }
		//  }

		//  return resultSet;
		//}

		//private ObservableCollection<CheckItem> CreateCheckList4AccountedGigs(ObservableCollection<Gig> accountedGigs)
		//{
		//  ObservableCollection<CheckItem> resultSet = new ObservableCollection<CheckItem>();

		//  foreach (Gig gig in accountedGigs)
		//  {
		//    foreach (Work work in gig.Works)
		//    {
		//      CheckItem item = (from p in resultSet where p.WorkNumber == work.WorkNumber && Manager.instance.IsInbetween(p.PerformanceDate, gig.DayOfShow, gig.DayOfShow_Last) select p).FirstOrDefault();

		//      if (item == null)
		//      {
		//        item = new CheckItem();

		//        item.WorkNumber = work.WorkNumber;
		//        item.PerformanceDate = gig.DayOfShow;
		//        item.PerformanceDay_Last = gig.DayOfShow_Last;
		//        item.Work = work;
		//        item.Gigs.Add(gig);

		//        resultSet.Add(item);
		//      }
		//      else
		//      {
		//        item.Gigs.Add(gig);
		//      }
		//    }
		//  }

		//  return resultSet;
		//}

		private bool IsThisWork2Check(Work work)
		{
			foreach (string publisher in Manager.instance.Configuration.AdministeredPublishers)
				if (publisher.Equals(work.Publisher, StringComparison.OrdinalIgnoreCase))
					return true;

			return false;
		}

		private bool EqualWorks(Work work1, Work work2)
		{
			if (work1 == null || work2 == null)
				return false;

			if (work1.WorkNumber <= 0 || work2.WorkNumber <= 0)
				if (work1.Title.Equals(work2.Title, StringComparison.OrdinalIgnoreCase))
					return true;
				else
					return false;

			if (work1.WorkNumber == work2.WorkNumber)
				return true;
			else
				return false;
		}

		private void CheckForAccountedButNotReportetGigs(ObservableCollection<ReportedGig> reportedGigs, ObservableCollection<Gig> accountedGigs)
		{
			_accountedButNotReportedGigs.Clear();

			foreach (Gig accountedGig in accountedGigs)
			{
				ReportedGig reportedGig = (from p in reportedGigs where Manager.instance.IsInbetween(p.DayOfShow, accountedGig.DayOfShow, accountedGig.DayOfShow_Last) && p.Venue.Postalcode == accountedGig.Venue.Postalcode select p).FirstOrDefault();

				if (reportedGig == null)
				{
					if (AnySongOfMeInThisShow(accountedGig.Works))
						_accountedButNotReportedGigs.Add(accountedGig);
				}
			}

		}
	}

	//public class CheckItem
	//{
	//  public int WorkNumber { get; set; }
	//  public DateTime PerformanceDate { get; set; }
	//  public DateTime PerformanceDay_Last { get; set; }
	//  public Work Work { get; set; }
	//  public ObservableCollection<Gig> Gigs { get; private set; }

	//  public CheckItem()
	//  {
	//    Gigs = new ObservableCollection<Gig>();
	//  }
	//}

	//public class ConfirmedItem
	//{
	//  public ReportedGig reportedGig { get; set; }
	//  public Gig accountedGig { get; set; }
	//  public Work confirmedWork { get; set; }
	//}

	//public class UnconfirmedItem
	//{
	//  public ReportedGig reportedGig { get; set; }
	//  public ObservableCollection<Gig> candidates { get; private set; }
	//  public Work work { get; set; }

	//  public UnconfirmedItem()
	//  {
	//    candidates = new ObservableCollection<Gig>();
	//  }
	//}
}
