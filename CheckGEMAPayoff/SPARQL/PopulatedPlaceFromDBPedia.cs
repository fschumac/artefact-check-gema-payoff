﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDS.RDF.Query;
using VDS.RDF;
using devfs.Sparql;

namespace Sparql
{
	public class PopulatedPlaceFromDBPedia
	{
		private IList<UriInfo> _dbPediaPopulatedPlacesCache;
		SparqlRemoteEndpoint _endpoint_dbpedia;

		public PopulatedPlaceFromDBPedia()
		{
			_endpoint_dbpedia = new SparqlRemoteEndpoint(new Uri("http://www.dbpedia.org/sparql"), "http://dbpedia.org");
			_endpoint_dbpedia.Timeout = 60000;
			_dbPediaPopulatedPlacesCache = new List<UriInfo>();
		}

		private const string PrefixRDFS = "PREFIX rdf: <http://www.w3.org/2000/01/rdf-schema#> ";
		private const string PrefixDBPedia = "PREFIX dbpedia: <http://dbpedia.org/ontology/> ";

		public IUriNode GetPopulatedPlaceURI(string nameOfPopulatedPlace, IList<LogData> errorLog)
		{
			// check cache
			foreach (UriInfo uriInfo in _dbPediaPopulatedPlacesCache)
				if (uriInfo.Labels.Contains(nameOfPopulatedPlace))
					return uriInfo.Uri;

			StringBuilder query = new StringBuilder();

			query.AppendLine(PrefixRDFS);
			query.AppendLine(PrefixDBPedia);
			query.AppendLine("select distinct * where {");
			query.AppendLine("?uri rdfs:label ?label. ");
			query.Append("FILTER (REGEX(STR(?label), \"^");
			query.Append(EncodeNonAsciiCharacters(nameOfPopulatedPlace));
			query.AppendLine("$\")). ");
			query.AppendLine("?uri a dbpedia:PopulatedPlace. ");
			query.AppendLine("}");

			//try - catchblock + fallbackmechanismus
			SparqlResultSet srs = Calls.CallSparqlEndpointWithResultSet(_endpoint_dbpedia, query.ToString(), errorLog);
			if (srs != null)
			{
				if (srs.Count > 0)
				{
					Dictionary<UriInfo, int> resultAggregation = new Dictionary<UriInfo, int>();

					foreach (SparqlResult sr in srs)
					{
						INode uriNode = null;
						INode labelNode = null;
						sr.TryGetValue("uri", out uriNode);
						sr.TryGetValue("label", out labelNode);

						if (uriNode != null && labelNode != null)
						{
							UriInfo foundItem = null;
							foreach (UriInfo element in resultAggregation.Keys)
								if (element.Uri == uriNode)
									foundItem = element;

							if (foundItem != null)
							{
								foundItem.Labels.Add(labelNode.ToString());
								resultAggregation[foundItem]++;
							}
							else
							{
								UriInfo info = new UriInfo(uriNode as IUriNode);
								info.Labels.Add(labelNode.ToString());
								resultAggregation.Add(info, 1);
							}
						}
						else
						{
							Console.WriteLine("uri/label for dbpedia:PopulatedPlace-Request for '" + nameOfPopulatedPlace + "'  was empty");
						}
					}

					int currentMaxAppearance = 0;
					UriInfo targetUri = null;

					foreach (KeyValuePair<UriInfo, int> item in resultAggregation)
					{
						if (item.Value > currentMaxAppearance)
						{
							currentMaxAppearance = item.Value;
							targetUri = item.Key;
						}
					}

					if (targetUri != null)
						_dbPediaPopulatedPlacesCache.Add(targetUri);

					return targetUri.Uri;

				}
				else
				{
					Console.WriteLine("nothing found for dbpedia:PopulatedPlace-Request for '" + nameOfPopulatedPlace + "'");
				}
			}
			else
			{
				Console.WriteLine("Error during SPARQL-Request against dbPedia-Endpoint");
				// Error during request => create a "guessed" URI
				// return "http://dbpedia.org/resource/" + nameOfPopulatedPlace;
				return null;
			}

			return null;
		}

		private string EncodeNonAsciiCharacters(string value)
		{
			StringBuilder sb = new StringBuilder();
			foreach (char c in value)
			{
				if (c > 127)
				{
					// This character is too big for ASCII
					string encodedValue = "\\u" + ((int)c).ToString("x4");
					sb.Append(encodedValue);
				}
				else
				{
					sb.Append(c);
				}
			}
			return sb.ToString();
		}

		public bool? UriHasLabel(IUriNode uri, string label)
		{
			IUriNode newUri = GetPopulatedPlaceURI(label, new List<LogData>());

			if (newUri == null)
				return null;

			if (newUri == uri)
				return true;

			return false;
		}
	}

}
