﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDS.RDF;

namespace Sparql
{
	public class SparqlHelperFunctions
	{
		/// <summary>
		/// This function creates an entry for sparql functions of the given URI
		/// Depending on the Node-Type and an existing namespace-prefix in the graph
		/// it will format the URI
		/// </summary>
		/// <param name="graph">Graph object</param>
		/// <param name="node">The node to format</param>
		/// <returns>formated URI for SPARQL-functions</returns>
		public static string UriForSparqlRequest(IGraph graph, INode node)
		{
			if (node.NodeType == NodeType.Uri)
			{
				string reducedUri = String.Empty;
				bool success = graph.NamespaceMap.ReduceToQName(node.ToString(), out reducedUri);
				if (success)
					return reducedUri;
				else
				{
					return "<" + node.ToString().Replace("\"", "\\\"") + ">";
				}
			}

			// Literal Node
			return "\"" + node.ToString().Replace("\"", "\\\"") + "\"";
		}

		public StringBuilder SparqlRequestStart(string baseNamespace, string localName)
		{
			StringBuilder query = new StringBuilder();
			query.AppendLine("prefix mbo: <http://creativeartefact.org/ontology/>");
			query.AppendLine("prefix res: <" + baseNamespace + localName + "/>");
			query.AppendLine("prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>");
			query.AppendLine("INSERT DATA { GRAPH <" + baseNamespace + "> { ");

			return query;
		}
	}
}
