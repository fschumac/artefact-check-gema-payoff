﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using VDS.RDF;

namespace SPARQL
{
	public class Artist
	{
		public IUriNode Uri { get; set; }
		public ObservableCollection<string> Label { get; private set; }
		public ObservableCollection<string> Disambiguation { get; private set; }
		public ObservableCollection<string> Annotation { get; private set; }

		public Artist()
		{
			Label = new ObservableCollection<string>();
			Disambiguation = new ObservableCollection<string>();
			Annotation = new ObservableCollection<string>();
		}
	}

	public class Distributor
	{
		public IUriNode Uri { get; set; }
		public ObservableCollection<string> Label { get; private set; }
		public ObservableCollection<string> Comment { get; private set; }

		public Distributor()
		{
			Label = new ObservableCollection<string>();
			Comment = new ObservableCollection<string>();
		}
	}

	public class MusicalObject
	{
		public IUriNode Uri { get; set; }
		public ObservableCollection<string> Titel { get; private set; }
		public ObservableCollection<string> CatalogueNumber { get; private set; }
		public ObservableCollection<string> Disambiguation { get; private set; }
		public ObservableCollection<string> Annotation { get; private set; }
		public Artist Artist { get; set; }

		public MusicalObject()
		{
			Titel = new ObservableCollection<string>();
			CatalogueNumber = new ObservableCollection<string>();
			Disambiguation = new ObservableCollection<string>();
			Annotation = new ObservableCollection<string>();
		}
	}
}
