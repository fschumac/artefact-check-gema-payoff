﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDS.RDF.Query;
using System.Net;
using Helper;

namespace Sparql
{
	//public static class Calls
	//{
	//  public static SparqlResultSet CallSparqlEndpointWithResultSet(SparqlRemoteEndpoint endpoint, string query, IList<string> errorLog)
	//  {
	//    SparqlResultSet result = null;

	//    try
	//    {
	//      result = endpoint.QueryWithResultSet(query);
	//    }
	//    catch (Exception ex)
	//    {
	//      HandleEndpointErrors(ex, new System.Diagnostics.StackFrame(1, true).GetMethod().Name, query, errorLog);
	//    }

	//    return result;
	//  }

	//  public static HttpWebResponse CallSparqlEndpointRaw(SparqlRemoteEndpoint endpoint, string query, IList<string> errorLog)
	//  {
	//    HttpWebResponse response = null;

	//    try
	//    {
	//      response = endpoint.QueryRaw(query);
	//    }
	//    catch (Exception ex)
	//    {
	//      HandleEndpointErrors(ex, new System.Diagnostics.StackFrame(1, true).GetMethod().Name, query, errorLog);
	//    }

	//    return response;
	//  }

	//  public static void HandleEndpointErrors(Exception ex, string callingMethod, string query, IList<string> errorLog)
	//  {
	//    if (ex.GetType() == typeof(RdfQueryException))
	//    {
	//      if (ex.InnerException != null)
	//      {
	//        if (ex.InnerException.Message.Contains("400"))
	//        {
	//          // => Fehler in SPARQL-Query
	//          bool writeErrorLogSuccess = Logger.instance.ErrorRoutine(ex.InnerException, "Query error: " + query);
	//          errorLog.Add("Error in SPARQL-Query: " + query);
	//        }

	//        else if (ex.InnerException.Message.Contains("401"))
	//        {
	//          // => Fehler bei Autentifizierung
	//          bool writeErrorLogSuccess = Logger.instance.ErrorRoutine(ex.InnerException, "Authenticationerror");
	//          errorLog.Add("The server did not accept the credentials (username / password).");
	//        }

	//        else if (ex.InnerException.Message.Contains("500"))
	//        {
	//          // => Internal Server Error
	//          bool writeErrorLogSuccess = Logger.instance.ErrorRoutine(ex.InnerException, "Method:" + callingMethod + Environment.NewLine + "Query: " + query);
	//          errorLog.Add("The server returned an internal error");
	//        }

	//        else
	//        {
	//          bool writeErrorLogSuccess = Logger.instance.ErrorRoutine(ex.InnerException, "Method:" + callingMethod + Environment.NewLine + "Query: " + query);
	//          errorLog.Add("General Server Error: " + ex.InnerException.Message);
	//        }

	//      }
	//      else
	//      {
	//        Logger.instance.ErrorRoutine(ex, "RdfQueryException - Method: " + callingMethod + Environment.NewLine + "Query: " + query);
	//        errorLog.Add("RdfQueryException: " + ex.Message);
	//      }
	//    }
	//    else if (ex.GetType() == typeof(RdfQueryTimeoutException))
	//    {
	//      Logger.instance.ErrorRoutine(ex, "Query timed out - Method: " + callingMethod + Environment.NewLine + "Server Time Out!");
	//      errorLog.Add("Server Time Out");
	//    }
	//    else
	//    {
	//      Logger.instance.ErrorRoutine(ex, "General exception - Method: " + callingMethod + Environment.NewLine + "Query: " + query);
	//      errorLog.Add("General exception: " + ex.Message);
	//    }
	//  }
	//}
}
