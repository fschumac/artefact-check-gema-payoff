﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CheckGEMAPayoff.Classes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using SPARQL;
using VDS.RDF.Query;
using Sparql;
using devfs.Sparql;
using VDS.RDF;

namespace CheckGEMAPayoff.SPARQL
{
	public class Export2TripleStore
	{
		public Export2TripleStore()
		{
		}

		public IList<LogData> StartExport(Write2TripleStoreArgs args)
		{
			IGraph updateGraph = new Graph(true);
			updateGraph.BaseUri = new Uri(args.BaseNamespace);

			updateGraph.NamespaceMap.AddNamespace("res", new Uri(args.BaseNamespace + args.LocalName));
			updateGraph.NamespaceMap.AddNamespace("rdf", new Uri("http://www.w3.org/1999/02/22-rdf-syntax-ns#"));
			updateGraph.NamespaceMap.AddNamespace("rdfs", new Uri("http://www.w3.org/2000/01/rdf-schema#"));
			updateGraph.NamespaceMap.AddNamespace("mbo", new Uri("http://creativeartefact.org/ontology/"));
			updateGraph.NamespaceMap.AddNamespace("dbpedia", new Uri("http://dbpedia.org/ontology/"));
			updateGraph.NamespaceMap.AddNamespace("dbres", new Uri("http://dbpedia.org/resource/"));
			
			args.CallProgressReport(10, "Updating venues in Triple-Store");
			IList<Address> venuesInStore = UpdateVenues(args, updateGraph);
			args.CallProgressReport(20, "Updating organisers in Triple-Store");
			IList<Address> organisersInStore = UpdateOrganiser(args, updateGraph);
			args.CallProgressReport(30, "Updating artists in Triple-Store");
			IList<Artist> artistsInStore = UpdateArtists(args, updateGraph);
			args.CallProgressReport(40, "Updating Live events in Triple-Store");
			IList<ReportedGig> liveEventsInStore = UpdateLiveEvents(args, venuesInStore, organisersInStore, updateGraph);
			args.CallProgressReport(50, "Updating Works in Triple-Store");
			IList<Work> worksInStore = UpdateWorks(args, updateGraph);
			args.CallProgressReport(60, "Updating Setlists in Triple-Store");
			IList<Setlist> setlistsInStore = UpdateSetlists(args, worksInStore, updateGraph);
			args.CallProgressReport(70, "Updating LivePerformances in Triple-Store");
			IList<LivePerformance> gigsInStore = UpdateLivePerformances(args, artistsInStore, liveEventsInStore, setlistsInStore, updateGraph);

			SparqlHelperFunctions.WriteToSPARQLEndpoint(args.Endpoint, updateGraph, args.ErrorLog, args.CallProgressReport);

			return args.ErrorLog;
		}

		private ObservableCollection<Address> GetVenues(IEnumerable<ReportedGig> gigs)
		{
			ObservableCollection<Address> resultSet = new ObservableCollection<Address>();

			foreach (Gig gig in gigs)
			{
				if ((from p in resultSet where p.Postalcode == gig.Venue.Postalcode && p.Name == gig.Venue.Name && p.Street == gig.Venue.Street select p) == null)
				{
					Address address = new Address();

					address.Name = gig.Venue.Name;
					address.City = gig.Venue.City;
					address.Postalcode = gig.Venue.Postalcode;
					address.Street = gig.Venue.Street;

					resultSet.Add(address);
				}
			}

			return resultSet;
		}

		private ObservableCollection<Address> GetOrganisers(IEnumerable<ReportedGig> gigs)
		{
			ObservableCollection<Address> resultSet = new ObservableCollection<Address>();

			foreach (Gig gig in gigs)
			{
				if ((from p in resultSet where p == gig.Organiser select p) == null)
				{
					Address address = new Address();

					address.Name = gig.Organiser.Name;
					address.City = gig.Organiser.City;
					address.Postalcode = gig.Organiser.Postalcode;
					address.Street = gig.Organiser.Street;

					resultSet.Add(address);
				}
			}

			return resultSet;
		}

		private IList<Work> UpdateWorks(Write2TripleStoreArgs args, IGraph updateGraph)
		{
			// make sure that all works are contained in the triple store. If not, create them.
			List<Work> worksInStore = SparqlHandler.AllWorks(args.Endpoint, args.ErrorLog);
			// get all works from the statements
			List<Work> worksInReport = new List<Work>();
			foreach (ArtistInfo artistInfo in args.ArtistInfos)
				foreach (IList<Work> setlist in artistInfo.Tracklists.Values)
					foreach (Work work in setlist)
					{
						Work tmp = (from p in worksInReport where p.WorkNumber == work.WorkNumber select p).FirstOrDefault();

						if (tmp == null)
							worksInReport.Add(work);
					}

			//// for each organiser:
			//StringBuilder query = SparqlRequestStart(args.BaseNamespace, args.LocalName);

			//// check wheter the work already exists in the Storage (if not create an uri for it)
			//bool newWorks = false;
			foreach (Work workInReport in worksInReport)
			{
				Work tmp = (from p in worksInStore where p.WorkNumber == workInReport.WorkNumber select p).FirstOrDefault();

				if (tmp == null)
				{
					workInReport.Uri = updateGraph.CreateUriNode("res:" + Guid.NewGuid().ToString());
					worksInStore.Add(workInReport);

					updateGraph.Assert(workInReport.Uri, updateGraph.CreateUriNode("rdf:type"), updateGraph.CreateUriNode("mbo:Work"));
					updateGraph.Assert(workInReport.Uri, updateGraph.CreateUriNode("rdfs:label"), updateGraph.CreateLiteralNode(workInReport.Title));
					updateGraph.Assert(workInReport.Uri, updateGraph.CreateUriNode("mbo:copyrightCollectiveNumber"), updateGraph.CreateLiteralNode(workInReport.WorkNumber.ToString()));

					//query.AppendLine(workInReport.Uri + " a mbo:Work;");
					//query.AppendLine("rdfs:label \"" + workInReport.Title + "\" ;");
					//query.AppendLine("mbo:copyrightCollectiveNumber \"" + workInReport.WorkNumber + "\" .");

					//newWorks = true;
				}
			}

			//query.AppendLine("} }");

			//if (newWorks)
			//{
			//  using (Calls.CallSparqlEndpointRaw(args.Endpoint, query.ToString(), args.ErrorLog)) ;
			//}

			return worksInStore;
		}

		private IList<Setlist> UpdateSetlists(Write2TripleStoreArgs args, IList<Work> worksInStore, IGraph updateGraph)
		{
			// make sure that all setlists are contained in the triple store. If not, create them.
			IList<Setlist> setlistsInStore = SparqlHandler.AllSetlists(args.Endpoint, args.ErrorLog, worksInStore);
			// get all setlists from the statements
			IList<Setlist> setlistsInReport = new List<Setlist>();

			foreach (ArtistInfo artistInfo in args.ArtistInfos)
				foreach (IList<Work> setlist in artistInfo.Tracklists.Values)
				{
					Setlist newSetlist = new Setlist();
					foreach (Work work in setlist)
						newSetlist.Works.Add(work);

					if (!setlistsInReport.Contains(newSetlist))
						setlistsInReport.Add(newSetlist);
				}

			//// for each setlist:
			//StringBuilder query = SparqlRequestStart(args.BaseNamespace, args.LocalName); 

			//// check wheter the setlist already exists in the Storage (if not create an uri for it)
			//bool newSetlists = false;
			foreach (Setlist setlistInReport in setlistsInReport)
			{
				if (!setlistsInStore.Contains(setlistInReport))
				{
					setlistInReport.Uri = updateGraph.CreateUriNode("res:" + Guid.NewGuid().ToString());
					setlistsInStore.Add(setlistInReport);

					updateGraph.Assert(setlistInReport.Uri, updateGraph.CreateUriNode("rdf:type"), updateGraph.CreateUriNode("mbo:Setlist"));
					foreach (Work work in setlistInReport.Works)
						updateGraph.Assert(setlistInReport.Uri, updateGraph.CreateUriNode("mbo:containsWork"), work.Uri);

					//query.AppendLine(setlistInReport.Uri + " a mbo:Setlist;");
					//query.Append("mbo:containsWork ");
					//for (int i = 0; i < setlistInReport.Works.Count; i++)
					//  if (i + 1 == setlistInReport.Works.Count)
					//    query.AppendLine(setlistInReport.Works[i].Uri + " .");
					//  else
					//    query.AppendLine(setlistInReport.Works[i].Uri + " ,");

					//newSetlists = true;
				}
			}

			//query.AppendLine("} }");

			//if (newSetlists)
			//{
			//  using (Calls.CallSparqlEndpointRaw(args.Endpoint, query.ToString(), args.ErrorLog)) ;
			//}

			return setlistsInStore;
		}

		private IList<LivePerformance> UpdateLivePerformances(Write2TripleStoreArgs args, IList<Artist> artistInStore, IList<ReportedGig> liveEventsInStore, IList<Setlist> setlistsInStore, IGraph updateGraph)
		{
			// make sure that all gigs are contained in the triple store. If not, create them.
			IList<LivePerformance> livePerformancesInStore = SparqlHandler.AllLivePerformances(args.Endpoint, args.ErrorLog, liveEventsInStore, artistInStore, setlistsInStore);
			// get all gigs from the statements
			ObservableCollection<LivePerformance> livePerformancesInReport = new ObservableCollection<LivePerformance>();
			foreach (ArtistInfo artistInfo in args.ArtistInfos)
				foreach (ReportedGig gig in artistInfo.Shows)
				{
					LivePerformance lp = new LivePerformance();
					lp.Artist = (from p in artistInStore where p.Label.Contains(artistInfo.Name) select p).FirstOrDefault();
					lp.LiveMusicEvent = (from p in liveEventsInStore where Manager.instance.IsInbetween(gig.DayOfShow, p.DayOfShow, p.DayOfShow_Last) && gig.Venue.Name == p.Venue.Name select p).FirstOrDefault();
					lp.ccsCategory = gig.Category;

					Setlist setlist2Compare = new Setlist();

					foreach (Work work in gig.Works)
						setlist2Compare.Works.Add(work);

					lp.Setlist = setlistsInStore.First(p => p.Equals(setlist2Compare));

					if (!livePerformancesInReport.Contains(lp))
						livePerformancesInReport.Add(lp);
				}

			//// for each LivePerformance:
			//StringBuilder query = SparqlRequestStart(args.BaseNamespace, args.LocalName);

			//// check wheter the live performance already exists in the Storage (if not create an uri for it)
			//bool newLiveEvents = false;
			foreach (LivePerformance gigInReport in livePerformancesInReport)
			{
				LivePerformance lpInStore = (from p in livePerformancesInStore where p.Artist == gigInReport.Artist && p.LiveMusicEvent == gigInReport.LiveMusicEvent select p).FirstOrDefault();

				if (lpInStore == null)
				{
					gigInReport.Uri = updateGraph.CreateUriNode("res:" + Guid.NewGuid().ToString());

					livePerformancesInStore.Add(gigInReport);

					updateGraph.Assert(gigInReport.Uri, updateGraph.CreateUriNode("rdf:type"), updateGraph.CreateUriNode("mbo:LivePerformance"));

					if (String.IsNullOrWhiteSpace(gigInReport.LiveMusicEvent.EventName))
						updateGraph.Assert(gigInReport.Uri, updateGraph.CreateUriNode("rdfs:label"), updateGraph.CreateLiteralNode(gigInReport.Artist.Label[0] + " @unnamed event"));
					else
						updateGraph.Assert(gigInReport.Uri, updateGraph.CreateUriNode("rdfs:label"), updateGraph.CreateLiteralNode(gigInReport.Artist.Label[0] + " @" + gigInReport.LiveMusicEvent.EventName));

					updateGraph.Assert(gigInReport.Uri, updateGraph.CreateUriNode("mbo:playedSetlist"), gigInReport.Setlist.Uri);

					updateGraph.Assert(gigInReport.Uri, updateGraph.CreateUriNode("mbo:ccsCategory"), updateGraph.CreateLiteralNode(gigInReport.ccsCategory));

					updateGraph.Assert(gigInReport.LiveMusicEvent.Uri, updateGraph.CreateUriNode("mbo:eventPerformance"), gigInReport.Uri);

					updateGraph.Assert(gigInReport.Artist.Uri, updateGraph.CreateUriNode("mbo:playsPerformance"), gigInReport.Uri);

					//query.AppendLine(gigInReport.Uri + " a mbo:LivePerformance;");

					//string label = String.Empty;
					//if (String.IsNullOrWhiteSpace(gigInReport.LiveMusicEvent.EventName))
					//  label = "unnamed event";
					//else
					//  label = gigInReport.LiveMusicEvent.EventName;
					//query.AppendLine("rdfs:label \"" + gigInReport.Artist.Label[0] + "@ " + label + "\" ;"); 
					
					//query.AppendLine("mbo:playedSetlist <" + gigInReport.Setlist.Uri + "> ;");
					//query.AppendLine("mbo:ccsCategory \"" + gigInReport.ccsCategory + "\" .");
					//query.AppendLine(gigInReport.LiveMusicEvent.Uri + " mbo:eventPerformance " + gigInReport.Uri + " .");
					//query.AppendLine(gigInReport.Artist.Uri + " mbo:playsPerformance " + gigInReport.Uri + " .");

					//newLiveEvents = true;
				}
			}

			//query.AppendLine("} }");

			//if (newLiveEvents)
			//{
			//  Console.WriteLine(query.ToString());
			//  using (Calls.CallSparqlEndpointRaw(args.Endpoint, query.ToString(), args.ErrorLog)) ;
			//}

			return livePerformancesInStore;
		}

		private IList<ReportedGig> UpdateLiveEvents(Write2TripleStoreArgs args, IList<Address> venuesInStore, IList<Address> organisersInStore, IGraph updateGraph)
		{
			// make sure that all live events are contained in the triple store. If not, create them.
			IList<ReportedGig> liveEventsInStore = SparqlHandler.AllLiveEvents(args.Endpoint, args.ErrorLog, organisersInStore, venuesInStore);
			// get all live Events from the statements
			IList<ReportedGig> liveEventsInReport = new List<ReportedGig>();
			foreach (ArtistInfo artistInfo in args.ArtistInfos)
				foreach (ReportedGig gig in artistInfo.Shows)
					if (!liveEventsInReport.Contains(gig))
						liveEventsInReport.Add(gig);

			//// for each organiser:
			//StringBuilder queryStart = SparqlRequestStart(args.BaseNamespace, args.LocalName);

			//StringBuilder query = new StringBuilder();

			// check wheter the live event already exists in the Storage (if not create an uri for it)
			foreach (ReportedGig gigInReport in liveEventsInReport)
			{
				if (!liveEventsInStore.Any(p => p.DayOfShow == gigInReport.DayOfShow && p.Venue != null && p.Venue.Name == gigInReport.Venue.Name && p.Venue.City == p.Venue.City))
				{
					gigInReport.Uri = updateGraph.CreateUriNode("res:" + Guid.NewGuid().ToString());

					liveEventsInStore.Add(gigInReport);

					updateGraph.Assert(gigInReport.Uri, updateGraph.CreateUriNode("rdf:type"), updateGraph.CreateUriNode("mbo:LiveMusicEvent"));

					if (!String.IsNullOrWhiteSpace(gigInReport.EventName))
						updateGraph.Assert(gigInReport.Uri, updateGraph.CreateUriNode("rdfs:label"), updateGraph.CreateLiteralNode(gigInReport.EventName));

					if (gigInReport.Organiser.Uri != null)
						updateGraph.Assert(gigInReport.Uri, updateGraph.CreateUriNode("mbo:organisedBy"), gigInReport.Organiser.Uri);

					if (gigInReport.Venue.Uri != null)
						updateGraph.Assert(gigInReport.Uri, updateGraph.CreateUriNode("mbo:takesPlaceAt"), gigInReport.Venue.Uri);

					updateGraph.Assert(gigInReport.Uri, updateGraph.CreateUriNode("mbo:begin"), updateGraph.CreateLiteralNode(gigInReport.DayOfShow.ToShortDateString()));
					if (gigInReport.DayOfShow_Last == DateTime.MinValue)
						updateGraph.Assert(gigInReport.Uri, updateGraph.CreateUriNode("mbo:end"), updateGraph.CreateLiteralNode(gigInReport.DayOfShow.ToShortDateString()));
					else
						updateGraph.Assert(gigInReport.Uri, updateGraph.CreateUriNode("mbo:end"), updateGraph.CreateLiteralNode(gigInReport.DayOfShow_Last.ToShortDateString()));

					//query.Clear();
					//query.AppendLine(gigInReport.Uri + " a mbo:LiveMusicEvent;");
					//if (!String.IsNullOrWhiteSpace(gigInReport.EventName))
					//  query.AppendLine("rdfs:label \"" + gigInReport.EventName + "\" ;");
					//if (gigInReport.Organiser.Uri != null)
					//  query.AppendLine("mbo:organisedBy " + gigInReport.Organiser.Uri + " ;");
					//if (gigInReport.Venue.Uri != null)
					//  query.AppendLine("mbo:takesPlaceAt " + gigInReport.Venue.Uri + " ;");
	
					//query.AppendLine("mbo:begin \"" + gigInReport.DayOfShow.ToShortDateString() + "\" ;");
					//if (gigInReport.DayOfShow_Last == DateTime.MinValue)
					//  query.AppendLine("mbo:end \"" + gigInReport.DayOfShow.ToShortDateString() + "\" .");
					//else
					//  query.AppendLine("mbo:end \"" + gigInReport.DayOfShow_Last.ToShortDateString() + "\" .");

					//query.AppendLine("} }");
					
					//using (Calls.CallSparqlEndpointRaw(args.Endpoint, queryStart.ToString() + query.ToString(), args.ErrorLog)) ;
				}
			}

			return liveEventsInStore;
		}

		private IList<Address> UpdateVenues(Write2TripleStoreArgs args, IGraph updateGraph)
		{
			// make sure that all venues are contained in the triple store. If not, create them.
			List<Address> venuesInStore = SparqlHandler.AllVenues(args.Endpoint, args.ErrorLog);
			// get all venues from the statements
			List<Address> venuesInReport = new List<Address>();

			foreach (ArtistInfo artistInfo in args.ArtistInfos)
			{
				foreach (ReportedGig gig in artistInfo.Shows)
				{
					Address venue = (from p in venuesInReport where p.Equals(gig.Venue) select p).FirstOrDefault();

					if (venue == null)
					{
						venuesInReport.Add(gig.Venue);
					}
				}
			}

			foreach (Address venueInReport in venuesInReport)
			{
				if (!venuesInStore.Contains(venueInReport))
				{
					venueInReport.Uri = updateGraph.CreateUriNode("res:" + Guid.NewGuid().ToString());
					updateGraph.Assert(venueInReport.Uri, updateGraph.CreateUriNode("rdf:type"), updateGraph.CreateUriNode("mbo:Venue"));

					venuesInStore.Add(venueInReport);

					if (!String.IsNullOrWhiteSpace(venueInReport.Name))
						updateGraph.Assert(venueInReport.Uri, updateGraph.CreateUriNode("rdfs:label"), updateGraph.CreateLiteralNode(venueInReport.Name));

					IUriNode popPlaceURI = Manager.instance.PopulatedPlace.GetPopulatedPlaceURI(venueInReport.City, args.ErrorLog);
					
					if (popPlaceURI != null)
						updateGraph.Assert(venueInReport.Uri, updateGraph.CreateUriNode("mbo:city"), popPlaceURI);

					if (!String.IsNullOrWhiteSpace(venueInReport.Street))
						updateGraph.Assert(venueInReport.Uri, updateGraph.CreateUriNode("mbo:street"), updateGraph.CreateLiteralNode(venueInReport.Street));
					
					if (!String.IsNullOrWhiteSpace(venueInReport.Postalcode))
						updateGraph.Assert(venueInReport.Uri, updateGraph.CreateUriNode("mbo:postalcode"), updateGraph.CreateLiteralNode(venueInReport.Postalcode));
				}
			}



			//// for each organiser:
			//StringBuilder query = SparqlRequestStart(args.BaseNamespace, args.LocalName);

			//// check wheter the organiser already exists in the Storage (if not create an uri for it)
			//bool newVenue = false;
			//foreach (Address venueInReport in venuesInReport)
			//{
			//  if (!venuesInStore.Contains(venueInReport))
			//  //if (!venuesInStore.Any(p => p.Name == venueInReport.Name && p.City == venueInReport.City))
			//  {
			//    venueInReport.Uri = "res:" + Guid.NewGuid().ToString();

			//    venuesInStore.Add(venueInReport);

			//    query.AppendLine(venueInReport.Uri + " a mbo:Venue");
			//    if (!String.IsNullOrWhiteSpace(venueInReport.Name))
			//      query.AppendLine("; rdfs:label \"" + venueInReport.Name + "\"");
			//    string popPlaceURI = Manager.instance.PopulatedPlace.GetPopulatedPlaceURI(venueInReport.City, args.ErrorLog);
			//    if (!String.IsNullOrWhiteSpace(popPlaceURI))
			//      query.AppendLine("; mbo:city <" + popPlaceURI + ">");
			//    if (!String.IsNullOrWhiteSpace(venueInReport.Street))
			//      query.AppendLine("; mbo:street \"" + venueInReport.Street + "\"");
			//    if (!String.IsNullOrWhiteSpace(venueInReport.Postalcode))
			//      query.AppendLine("; mbo:postalcode \"" + venueInReport.Postalcode + "\"");
			//    query.AppendLine(".");

			//    newVenue = true;
			//  }
			//}

			//query.AppendLine("} }");

			//if (newVenue)
			//{
			//  using (Calls.CallSparqlEndpointRaw(args.Endpoint, query.ToString(), args.ErrorLog)) ;
			//}

			return venuesInStore;
		}

		#region Organiser
		private IList<Address> UpdateOrganiser(Write2TripleStoreArgs args, IGraph updateGraph)
		{
			// make sure that all organisers are contained in the triple store. If not, create them.
			List<Address> organisersInStore = SparqlHandler.AllOrganiser(args.Endpoint, args.ErrorLog);
			// get all organisers from the statements
			List<Address> organisersInReport = new List<Address>();
			foreach (ArtistInfo artistInfo in args.ArtistInfos)
			{
				foreach (ReportedGig gig in artistInfo.Shows)
				{
					Address organiser = (from p in organisersInReport where p.Equals(gig.Organiser) select p).FirstOrDefault();

					if (organiser == null)
					{
						organisersInReport.Add(gig.Organiser);
					}
				}
			}

			foreach (Address organiserInReport in organisersInReport)
			{
				if (!organisersInStore.Contains(organiserInReport))
				{
					organiserInReport.Uri = updateGraph.CreateUriNode("res:" + Guid.NewGuid().ToString());
					updateGraph.Assert(organiserInReport.Uri, updateGraph.CreateUriNode("rdf:type"), updateGraph.CreateUriNode("mbo:Organiser"));

					organisersInStore.Add(organiserInReport);

					if (!String.IsNullOrWhiteSpace(organiserInReport.Name))
						updateGraph.Assert(organiserInReport.Uri, updateGraph.CreateUriNode("rdfs:label"), updateGraph.CreateLiteralNode(organiserInReport.Name));

					IUriNode popPlaceURI = Manager.instance.PopulatedPlace.GetPopulatedPlaceURI(organiserInReport.City, args.ErrorLog);

					if (popPlaceURI != null)
						updateGraph.Assert(organiserInReport.Uri, updateGraph.CreateUriNode("mbo:city"), popPlaceURI);

					if (!String.IsNullOrWhiteSpace(organiserInReport.Street))
						updateGraph.Assert(organiserInReport.Uri, updateGraph.CreateUriNode("mbo:street"), updateGraph.CreateLiteralNode(organiserInReport.Street));

					if (!String.IsNullOrWhiteSpace(organiserInReport.Postalcode))
						updateGraph.Assert(organiserInReport.Uri, updateGraph.CreateUriNode("mbo:postalcode"), updateGraph.CreateLiteralNode(organiserInReport.Postalcode));
				}
			}

			//// for each organiser:
			//StringBuilder query = SparqlRequestStart(args.BaseNamespace, args.LocalName);

			//// check wheter the organiser already exists in the Storage (if not create an uri for it)
			//bool newOrganiser = false;
			//foreach (Address organiserInReport in organisersInReport)
			//{
			//  if (!organisersInStore.Contains(organiserInReport))
			//  {
			//    organiserInReport.Uri = updateGraph.CreateUriNode("res:" + Guid.NewGuid().ToString());

			//    organisersInStore.Add(organiserInReport);

			//    query.AppendLine(organiserInReport.Uri + " a mbo:Organiser");
			//    if (!String.IsNullOrWhiteSpace(organiserInReport.Name))
			//      query.AppendLine("; rdfs:label \"" + organiserInReport.Name + "\"");
			//    string popPlaceURI = Manager.instance.PopulatedPlace.GetPopulatedPlaceURI(organiserInReport.City, args.ErrorLog);
			//    if (!String.IsNullOrWhiteSpace(popPlaceURI))
			//      query.AppendLine("; mbo:city <" + popPlaceURI + ">");
			//    if (!String.IsNullOrWhiteSpace(organiserInReport.Street))
			//      query.AppendLine("; mbo:street \"" + organiserInReport.Street + "\"");
			//    if (!String.IsNullOrWhiteSpace(organiserInReport.Postalcode))
			//      query.AppendLine("; mbo:postalcode \"" + organiserInReport.Postalcode + "\"");
			//    query.AppendLine(".");

			//    newOrganiser = true;
			//  }
			//}

			//query.AppendLine("} }");

			//if (newOrganiser)
			//{
			//  using (Calls.CallSparqlEndpointRaw(args.Endpoint, query.ToString(), args.ErrorLog)) ;
			//}

			return organisersInStore;
		}

		private Address GetOrganiser(Address organiser, ObservableCollection<Address> organisersInStore)
		{
			IList<Address> theOrganisers = (from p in organisersInStore where p == organiser select p).ToList();

			if (theOrganisers.Count == 0)
				return null;

			if (theOrganisers.Count > 1)
				// TODO: Create Form that displays the found artists and let the user choose one (or create a new one)
				Console.WriteLine(theOrganisers.Count.ToString() + " objects found for '" + organiser.Name + "'");

			return theOrganisers[0];
		}
		#endregion


		#region Artists
		private IList<Artist> UpdateArtists(Write2TripleStoreArgs args, IGraph updateGraph)
		{
			// make sure that all artists are contained in the triple store. If not, create them.
			IList<Artist> artistsInStore = SparqlHandler.AllArtists(args.Endpoint, args.ErrorLog);
			// get all artists from the statements
			IList<Artist> artistsInReport = new List<Artist>();

			//IEnumerable<string> artistsInReport = (from p in args.ArtistInfos select p.Name).Distinct();

			// for each artist:
			//StringBuilder query = SparqlRequestStart(args.BaseNamespace, args.LocalName);

			// check wheter the artist already exists in the Storage (if not create an uri for it)
			foreach (ArtistInfo artistInfo in args.ArtistInfos)
			{
				Artist artist = GetArtist(artistInfo.Name, artistsInStore);

				if (artist == null)
				{
					artist = new Artist();
					artist.Uri = updateGraph.CreateUriNode("res:" + Guid.NewGuid().ToString());
					artist.Label.Add(artistInfo.Name);
					
					artistsInStore.Add(artist);

					updateGraph.Assert(artist.Uri, updateGraph.CreateUriNode("rdf:type"), updateGraph.CreateUriNode("mbo:Artist"));
					updateGraph.Assert(artist.Uri, updateGraph.CreateUriNode("rdfs:label"), updateGraph.CreateLiteralNode(artistInfo.Name));
				}
			}
			
			
			//bool newArtists = false;
			//foreach (string artistName in artistsInReport)
			//{
			//  Artist artist = GetArtist(artistName, artistsInStore);

			//  // if artist doesn't exist => create it
			//  if (artist == null)
			//  {
			//    // we need to create a new uri for the artist
			//    Artist newArtist = new Artist();
			//    newArtist.Uri = updateGraph.CreateUriNode("res:" + Guid.NewGuid().ToString());
			//    newArtist.Label.Add(artistName);
			//    artistsInStore.Add(artist);

			//    query.AppendLine(artist.Uri + " a mbo:Artist;");
			//    query.AppendLine("rdfs:label \"" + artistName + "\" .");

			//    newArtists = true;
			//  }
			//}

			//query.AppendLine("} }");

			//if (newArtists)
			//{
			//  using (Calls.CallSparqlEndpointRaw(args.Endpoint, query.ToString(), args.ErrorLog)) ; 
			//}

			return artistsInStore;
		}

		private Artist GetArtist(string artistName, IList<Artist> artistsInStore)
		{
			IList<Artist> theArtists = (from p in artistsInStore where p.Label.Contains(artistName) select p).ToList();

			if (theArtists.Count == 0)
				return null;

			if (theArtists.Count > 1)
				// TODO: Create Form that displays the found artists and let the user choose one (or create a new one)
				Console.WriteLine(theArtists.Count.ToString() + " objects found for '" + artistName + "'");

			return theArtists[0];
		}
		#endregion

		#region Helper
		private StringBuilder SparqlRequestStart(string baseNamespace, string localName)
		{
			StringBuilder query = new StringBuilder();
			query.AppendLine("prefix mbo: <http://creativeartefact.org/ontology/>");
			query.AppendLine("prefix res: <" + baseNamespace + localName + ">");
			query.AppendLine("prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>");
			query.AppendLine("INSERT DATA { GRAPH <" + baseNamespace + "> { ");

			return query;
		}
		#endregion
	}
}
