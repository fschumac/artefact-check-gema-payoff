﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDS.RDF.Query;
using CheckGEMAPayoff.Classes;
using devfs.Sparql;

namespace CheckGEMAPayoff.SPARQL
{
	public class Write2TripleStoreArgs
	{
		public delegate void CallProgressReportDelegate(int percentage, object userState);

		public SparqlRemoteEndpoint Endpoint { get; set; }
		public IList<LogData> ErrorLog { get; set; }
		public IList<ArtistInfo> ArtistInfos { get; set; }
		public SparqlHelperFunctions.CallProgressReportDelegate CallProgressReport { get; set; }
		public string LocalName { get; set; }
		public string BaseNamespace { get; set; }
	}
}
