﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDS.RDF.Query;
using System.Net;
using System.Windows;
using Helper;
using System.Collections.ObjectModel;
using VDS.RDF;
using CheckGEMAPayoff.Classes;
using SPARQL;
using CheckGEMAPayoff.Properties;
using devfs.Sparql;

namespace Sparql
{
	public static class SparqlHandler
	{


		#region Prefixes
		public const string PrefixMBO = "PREFIX mbo: <http://creativeartefact.org/ontology/> ";
		public const string PrefixRDFS = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ";
		public const string PrefixRDF = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> ";
		public const string PrefixDBPedia = "PREFIX dbpedia: <http://dbpedia.org/ontology/> ";
		#endregion

		#region Requests
		public static List<Address> AllVenues(SparqlRemoteEndpoint endpoint, IList<LogData> errorLog)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(PrefixMBO);
			sb.Append(PrefixRDFS);
			sb.Append("SELECT DISTINCT * WHERE {");
			sb.Append("?uri a mbo:Venue. ");
			sb.Append("?uri rdfs:label ?label. ");
			sb.Append("OPTIONAL {?uri mbo:city ?city}. ");
			sb.Append("OPTIONAL {?uri mbo:street ?street}. ");
			sb.Append("OPTIONAL {?uri mbo:postalcode ?postalcode}. ");
			sb.Append("}");

			SparqlResultSet sparqlResults = Calls.CallSparqlEndpointWithResultSet(endpoint, sb.ToString(), errorLog);

			List<Address> resultSet = new List<Address>();

			if (sparqlResults == null)
				return resultSet;

			foreach (SparqlResult result in sparqlResults)
			{
				INode node = null;
				Address address = null;
				if (result.TryGetValue("uri", out node))
					if (node != null)
						address = (from p in resultSet where p.Uri.ToString() == node.ToString() select p).FirstOrDefault();

				if (address == null)
				{
					address = new Address();
					address.Uri = node as IUriNode;
				}

				result.TryGetValue("label", out node);
				if (node != null)
					address.Name = node.ToString();

				result.TryGetValue("postalcode", out node);
				if (node != null)
					address.Postalcode = node.ToString();
				
				result.TryGetValue("city", out node);
				if (node != null)
					address.City = node.ToString();

				result.TryGetValue("street", out node);
				if (node != null)
					address.Street = node.ToString();

				resultSet.Add(address);
			}

			return resultSet;
		}

		public static List<Address> AllOrganiser(SparqlRemoteEndpoint endpoint, IList<LogData> errorLog)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(PrefixMBO);
			sb.Append(PrefixRDFS);
			sb.Append("SELECT DISTINCT * WHERE {");
			sb.Append("?uri a mbo:Organiser. ");
			sb.Append("?uri rdfs:label ?label. ");
			sb.Append("OPTIONAL {?uri mbo:city ?city}. ");
			sb.Append("OPTIONAL {?uri mbo:street ?street}. ");
			sb.Append("OPTIONAL {?uri mbo:postalcode ?postalcode}. ");
			sb.Append("}");

			SparqlResultSet sparqlResults = Calls.CallSparqlEndpointWithResultSet(endpoint, sb.ToString(), errorLog);

			List<Address> resultSet = new List<Address>();

			if (sparqlResults == null)
				return resultSet;

			foreach (SparqlResult result in sparqlResults)
			{
				INode node = null;
				Address address = null;
				result.TryGetValue("uri", out node);
				if (node != null)
					address = (from p in resultSet where p.Uri.ToString().Contains(((UriNode)node).Uri.ToString()) select p).FirstOrDefault();

				if (address == null)
				{
					address = new Address();
					address.Uri = node as IUriNode;
				}

				result.TryGetValue("label", out node);
				if (node != null)
					address.Name = node.ToString();

				result.TryGetValue("postalcode", out node);
				if (node != null)
					address.Postalcode = node.ToString();

				result.TryGetValue("city", out node);
				if (node != null)
					address.City = node.ToString();

				result.TryGetValue("street", out node);
				if (node != null)
					address.Street = node.ToString();

				resultSet.Add(address);
			}

			return resultSet;
		}

		public static List<Artist> AllArtists(SparqlRemoteEndpoint endpoint, IList<LogData> errorLog)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(PrefixMBO);
			sb.Append(PrefixRDFS);
			sb.Append("SELECT DISTINCT * WHERE {");
			sb.Append("?uri a mbo:Artist. ");
			sb.Append("?uri rdfs:label ?label. ");
			sb.Append("OPTIONAL {?uri mbo:disambiguation ?dis}. ");
			sb.Append("OPTIONAL {?uri mbo:annotation ?ann}. ");
			sb.Append("}");

			SparqlResultSet	sparqlResults = endpoint.QueryWithResultSet(sb.ToString());

			List<Artist> resultSet = new List<Artist>();

			if (sparqlResults == null)
				return resultSet;

			foreach (SparqlResult result in sparqlResults)
			{
				INode node = null;
				Artist artist = null;
				result.TryGetValue("uri", out node);
				if (node != null)
					artist = (from p in resultSet where p.Uri.ToString() == node.ToString() select p).FirstOrDefault();

				if (artist == null)
				{
					artist = new Artist();
					artist.Uri = node as IUriNode;
				}

				result.TryGetValue("label", out node);
				if (node != null)
					artist.Label.Add(node.ToString());

				result.TryGetValue("dis", out node);
				if (node != null)
					artist.Disambiguation.Add(node.ToString());

				result.TryGetValue("ann", out node);
				if (node != null)
					artist.Annotation.Add(node.ToString());

				resultSet.Add(artist);
			}

			return resultSet;
		}

		public static List<Work> AllWorks(SparqlRemoteEndpoint endpoint, IList<LogData> errorLog)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(PrefixMBO);
			sb.Append(PrefixRDFS);
			sb.Append("SELECT DISTINCT ?uri ?number ?label WHERE {");
			sb.Append("?uri a mbo:Work. ");
			sb.Append("?uri mbo:copyrightCollectiveNumber ?number. ");
			sb.Append("OPTIONAL {?uri rdfs:label ?label.} ");
			sb.Append("}");

			SparqlResultSet sparqlResults = Calls.CallSparqlEndpointWithResultSet(endpoint, sb.ToString(), errorLog);

			List<Work> resultSet = new List<Work>();

			if (sparqlResults == null)
				return resultSet;

			foreach (SparqlResult result in sparqlResults)
			{
				INode node = null;
				Work work = null;
				result.TryGetValue("uri", out node);
				if (work != null)
					work = (from p in resultSet where p.Uri.ToString() == node.ToString() select p).FirstOrDefault();

				if (work == null)
				{
					work = new Work();
					work.Uri = node as IUriNode;
				}

				int tmpNumber = 0;
				if (result.TryGetValue("number", out node))
					if (node != null)
						if (Int32.TryParse(node.ToString(), out tmpNumber))
							work.WorkNumber = tmpNumber;
						else
							work.WorkNumber = -1;

				result.TryGetValue("label", out node);
				if (node != null)
					work.Title = node.ToString();

				resultSet.Add(work);
			}

			return resultSet;
		}

		public static IList<Setlist> AllSetlists(SparqlRemoteEndpoint endpoint, IList<LogData> errorLog, IList<Work> works)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(PrefixMBO);
			sb.Append(PrefixRDFS);
			sb.Append("SELECT DISTINCT ?uri ?work WHERE {");
			sb.Append("?uri a mbo:Setlist. ");
			sb.Append("?uri mbo:containsWork ?work. ");
			sb.Append("}");

			SparqlResultSet sparqlResults = Calls.CallSparqlEndpointWithResultSet(endpoint, sb.ToString(), errorLog);

			List<Setlist> resultSet = new List<Setlist>();

			if (sparqlResults == null)
				return resultSet;

			foreach (SparqlResult result in sparqlResults)
			{
				INode node = null;
				Setlist setlist = null;
				result.TryGetValue("uri", out node);
				if (node != null)
					setlist = (from p in resultSet where p.Uri.ToString() == node.ToString() select p).FirstOrDefault();

				if (setlist == null)
				{
					setlist = new Setlist();
					setlist.Uri = node as IUriNode;
					resultSet.Add(setlist);
				}


				result.TryGetValue("work", out node);
				if (node != null)
				{
					Work work = (from p in works where p.Uri.ToString() == node.ToString() select p).FirstOrDefault();

					if (work != null)
						setlist.Works.Add(work);
				}
			}

			return resultSet;
		}

		public static IList<ReportedGig> AllLiveEvents(SparqlRemoteEndpoint endpoint, IList<LogData> errorLog, IList<Address> organisers, IList<Address> venues)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(PrefixMBO);
			sb.Append(PrefixRDFS);
			sb.Append("SELECT * WHERE {");
			sb.Append("?uri a mbo:LiveMusicEvent; ");
			sb.Append("?p ?o. ");
			sb.Append("}");
			//sb.Append("SELECT DISTINCT ?uri ?label ?organiser ?venue ?begin ?end WHERE {");
			//sb.Append("?uri a mbo:LiveMusicEvent. ");
			//sb.Append("OPTIONAL {?uri rdfs:label ?label}. ");
			//sb.Append("OPTIONAL {?uri mbo:organisedBy ?organiser}. ");
			//sb.Append("OPTIONAL {?uri mbo:takesPlaceAt ?venue}. ");
			//sb.Append("OPTIONAL {?uri mbo:begin ?begin}. ");
			//sb.Append("OPTIONAL {?uri mbo:end ?end}. ");
			//sb.Append("}");

			SparqlResultSet sparqlResults = Calls.CallSparqlEndpointWithResultSet(endpoint, sb.ToString(), errorLog);

			IList<ReportedGig> resultSet = new List<ReportedGig>();

			if (sparqlResults == null)
				return resultSet;

			foreach (SparqlResult result in sparqlResults)
			{
				INode node = null;
				ReportedGig gig = null;
				result.TryGetValue("uri", out node);
				if (node != null)
					gig = (from p in resultSet where p.Uri.ToString().Contains(((UriNode)node).Uri.ToString()) select p).FirstOrDefault();

				if (gig == null)
				{
					gig = new ReportedGig();
					gig.Uri = node as IUriNode;
					resultSet.Add(gig);
				}

				result.TryGetValue("p", out node);
				if (node != null)
				{
					INode objectNode = null;
					DateTime dtResult;

					result.TryGetValue("o", out objectNode);
					if (objectNode != null)
					{
						switch (((UriNode)node).Uri.ToString())
						{
							case "http://www.w3.org/2000/01/rdf-schema#label":
								gig.EventName = objectNode.ToString();
								break;
							case "http://creativeartefact.org/ontology/organisedBy":
								gig.Organiser = (from p in organisers where p.Uri.ToString() == objectNode.ToString() select p).FirstOrDefault();
								break;
							case "http://creativeartefact.org/ontology/takesPlaceAt":
								gig.Venue = (from p in venues where p.Uri.ToString() == objectNode.ToString() select p).FirstOrDefault();
								break;
							case "http://creativeartefact.org/ontology/begin":
							  if (DateTime.TryParse(objectNode.ToString(), out dtResult))
							    gig.DayOfShow = dtResult;								
								break;
							case "http://creativeartefact.org/ontology/end":
								if (DateTime.TryParse(objectNode.ToString(), out dtResult))
									gig.DayOfShow_Last = dtResult;
								break;
						}
					}
					else
						Console.WriteLine("Did not found object (o) in " + result.ToString());
				}
				else
					Console.WriteLine("Did not found property (p) in " + result.ToString());

			}

			return resultSet;
		}

		public static IList<LivePerformance> AllLivePerformances(SparqlRemoteEndpoint endpoint, IList<LogData> errorLog, IList<ReportedGig> liveMusicEvents, IList<Artist> artists, IList<Setlist> setlists)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(PrefixMBO);
			sb.Append(PrefixRDFS);
			sb.Append("SELECT DISTINCT ?uri ?liveMusicEvent ?artist ?setlist ?ccsCategory WHERE {");
			sb.Append("?uri a mbo:LivePerformance. ");
			sb.Append("OPTIONAL {?liveMusicEvent mbo:eventPerformance ?uri}. ");
			sb.Append("OPTIONAL {?artist mbo:playsPerformance ?uri}. ");
			sb.Append("OPTIONAL {?uri mbo:playedSetlist ?setlist}. ");
			sb.Append("OPTIONAL {?uri mbo:ccsCategory ?ccsCategory}. ");
			sb.Append("}");

			SparqlResultSet sparqlResults = Calls.CallSparqlEndpointWithResultSet(endpoint, sb.ToString(), errorLog);

			List<LivePerformance> resultSet = new List<LivePerformance>();

			if (sparqlResults == null)
				return resultSet;

			foreach (SparqlResult result in sparqlResults)
			{
				INode node = null;
				LivePerformance gig = null;
				result.TryGetValue("uri", out node);
				if (node != null)
					gig = (from p in resultSet where p.Uri.ToString().Contains(((UriNode)node).Uri.ToString()) select p).FirstOrDefault();

				if (gig == null)
				{
					gig = new LivePerformance();
					gig.Uri = node as IUriNode;
				}

				result.TryGetValue("liveMusicEvent", out node);
				if (node != null)
					gig.LiveMusicEvent = (from p in liveMusicEvents where p.Uri.ToString() == node.ToString() select p).FirstOrDefault();

				result.TryGetValue("artist", out node);
				if (node != null)
					gig.Artist = (from p in artists where p.Uri.ToString() == node.ToString() select p).FirstOrDefault();

				result.TryGetValue("setlist", out node);
				if (node != null)
					gig.Setlist = (from p in setlists where p.Uri.ToString() == node.ToString() select p).FirstOrDefault();

				result.TryGetValue("ccsCategory", out node);
				if (node != null)
					gig.ccsCategory = node.ToString();

				resultSet.Add(gig);
			}

			return resultSet;
		}
		#endregion

	}
}
