﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDS.RDF.Query;
using System.Windows;
using CheckGEMAPayoff.Properties;
using CheckGEMAPayoff.Configuration;
using CheckGEMAPayoff;
using devfs.Sparql;

namespace Sparql
{
	public static class SparqlEndpointFactory
	{
		public static SparqlRemoteEndpoint GetMBOInstanceEndpoint()
		{
			string sparqlEndpoint = Settings.Default["Endpoint_URL"].ToString();
			if (String.IsNullOrWhiteSpace(sparqlEndpoint))
			{
				MessageBox.Show("Sparql-Endpunkt nicht konfiguriert");
				return null;
			}

			string graph = Settings.Default["Graph_Name"].ToString();
			if (String.IsNullOrWhiteSpace(graph))
			{
				MessageBox.Show("Graph nicht konfiguriert");
				return null;
			}

			string user = Settings.Default["User_Name"].ToString();
			if (String.IsNullOrWhiteSpace(user))
			{
				MessageBox.Show("User nicht konfiguriert");
				return null;
			}

			SparqlRemoteEndpoint ep = new SparqlRemoteEndpoint(new Uri(sparqlEndpoint), graph);
			ep.Timeout = 100000;
			ep.HttpMode = "POST";

			if (String.IsNullOrWhiteSpace(PasswordStore.instance.Password))
			{
				PasswordWindow pwdWnd = new PasswordWindow();
				if (pwdWnd.ShowDialog() == true)
				{
					PasswordStore.instance.Password = pwdWnd.Password;
				}
				else
				{
					MessageBox.Show("You have to set a password to be able to access the Sparql-Endpoint.", "Could not create Endpoint", MessageBoxButton.OK, MessageBoxImage.Error);
					return null;
				}
			}

				//ep.Credentials = new System.Net.NetworkCredential("Updator", "secret");
			ep.Credentials = new System.Net.NetworkCredential(user, PasswordStore.instance.Password);


			// test the endpoint
			IList<string> errorLog = new List<string>();
			if (Calls.CallSparqlEndpointWithResultSet(ep, "SELECT * where { ?s ?p ?o} LIMIT 1", errorLog) == null)
			{
				string msg = String.Empty;

				if (errorLog.Count == 0)
					msg = "Could not access endpoint. Could not get information about the problem.";
				else
					foreach (string error in errorLog)
						msg += error + Environment.NewLine;

				MessageBox.Show(msg, "Error contacting endpoint", MessageBoxButton.OK, MessageBoxImage.Error);

				PasswordStore.instance.Password = String.Empty;
				ep = null;
			}

			return ep;
		}

		public static SparqlRemoteEndpoint GetDBPediaEndpoint()
		{
			return new SparqlRemoteEndpoint(new Uri("http://www.dbpedia.org/sparql"), "http://dbpedia.org");
		}

	}
}
