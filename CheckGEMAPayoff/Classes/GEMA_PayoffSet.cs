﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CheckGEMAPayoff.GEMA_Accounting;

namespace CheckGEMAPayoff.Classes
{
	public class GEMA_PayoffSet : IGEMA_Type
	{
		public int id { get; set; }
		public string Category { get; set; }
		public DateTime PayoffDate { get; set; }
		public string Description { get; set; }
	}
}
