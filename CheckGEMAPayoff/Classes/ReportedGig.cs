﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace CheckGEMAPayoff.Classes
{
	public class ReportedGig : Gig
	{
		public string ArtistName { get; set; }
		public string TrackListName { get; set; }
		public bool OnlineTransmission { get; set; }
		public DateTime DayOfTransmission { get; set; }
		public ObservableCollection<Gig> Candidates { get; private set; }
		public int rowNumber { get; set; }
    public string ShowTime { get; set; }

		public ReportedGig() : base()
		{
			Candidates = new ObservableCollection<Gig>();
			rowNumber = -1;
		}
	}

}
