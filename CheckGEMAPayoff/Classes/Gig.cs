﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace CheckGEMAPayoff.Classes
{
	public class Gig
	{
		public DateTime DayOfShow { get; set; }
		public DateTime DayOfShow_Last { get; set; }

		public Address Venue { get; set; }
		public Address Organiser { get; set; }

		public string Category { get; set; }
		public string EventName { get; set; }

		public ObservableCollection<Work> Works { get; set; }
		public ObservableCollection<Beneficiary> Beneficiaries { get; set; }

		//public TimeSpan Duration
		//{
		//	get
		//	{
		//		TimeSpan result = new TimeSpan();
		//		foreach (Work work in Works)
		//			result += work.Duration;

		//		return result;
		//	}
		//}

		public string DisplayBeneficiaries
		{
			get
			{
				string result = String.Empty;

				foreach (Beneficiary bene in Beneficiaries)
					result += bene.MainAccountOwner + "; ";

				if (String.IsNullOrWhiteSpace(result))
					return result;
				else
					return result.Substring(0, result.Length - 2);
			}
		}

		public Gig()
		{
			Venue = new Address();
			Organiser = new Address();
			Works = new ObservableCollection<Work>();
			Beneficiaries = new ObservableCollection<Beneficiary>();
		}

		public override bool Equals(object obj)
		{
			Gig compare = obj as Gig;
			if (compare != null)
				if (this.DayOfShow == compare.DayOfShow && this.DayOfShow_Last == compare.DayOfShow_Last && this.Organiser == compare.Organiser && this.Venue == compare.Venue)
					return true;
				else
					return false;
			else
				return false;
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hash = 17;
				hash = hash * 23 + this.DayOfShow.GetHashCode();
				hash = hash * 23 + this.DayOfShow_Last.GetHashCode();
				hash = hash * 23 + this.Organiser.GetHashCode();
				hash = hash * 23 + this.Venue.GetHashCode();

				return hash;
			}
		}
	}
}
