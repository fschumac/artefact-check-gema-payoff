﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheckGEMAPayoff.Classes
{
	public class Track
	{
		public int id { get; set; }
		public int WorkNumber { get; set; }
		public string Title { get; set; }
		public string Composer { get; set; }
		public string Publisher { get; set; }
	}

	public class ManagedTrack
	{
		public int WorkNumber { get; set; }
		public int Version { get; set; }
		public string Title { get; set; }
	}
}
