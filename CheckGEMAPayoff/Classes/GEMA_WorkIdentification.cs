﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CheckGEMAPayoff.GEMA_Accounting;

namespace CheckGEMAPayoff.Classes
{
	public class GEMA_WorkIdentification : Work, IGEMA_Type
	{
		public int ID { get; set; }
		public GEMA_Beneficiary Beneficiary { get; set; }
	}
}
