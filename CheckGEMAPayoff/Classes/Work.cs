﻿namespace CheckGEMAPayoff.Classes
{
  public class Work
	{
		public int WorkNumber { get; set; }
		public int Version { get; set; }
		public string Title { get; set; }
		public string Composer { get; set; }
		public string Editor { get; set; }
		public string Publisher { get; set; }
		//public TimeSpan Duration { get; set; }

		public override bool Equals(object obj)
		{
			Work compare = obj as Work;

			if (compare != null)
			{
				if (compare.WorkNumber == this.WorkNumber && compare.Title.Equals(this.Title))
					return true;
				else
					return false;
			}
			else
				return false;
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hash = 17;
				hash = hash * 23 + this.WorkNumber.GetHashCode();
        hash = hash * 23 + this.Title.GetHashCode();

				return hash;
			}
		}
	}
}
