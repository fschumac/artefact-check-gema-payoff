﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CheckGEMAPayoff.GEMA_Accounting;

namespace CheckGEMAPayoff.Classes
{
	public class GEMA_Beneficiary : IGEMA_Type
	{
		public int id { get; set; }
		public int AboveAccountNumber { get; set; }
		public int MainAccountNumber { get; set; }
		public string MainAccountOwner { get; set; }
		public string Description { get; set; }
		public GEMA_PayoffSet PayoffSet { get; set; }
	}
}
