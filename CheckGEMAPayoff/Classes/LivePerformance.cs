﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheckGEMAPayoff.Classes
{
	public class LivePerformance
	{
		public Gig LiveMusicEvent { get; set; }
		public ArtistInfo Artist { get; set; }
		public Setlist Setlist { get; set; }
		public string ccsCategory { get; set; }

    public override bool Equals(object obj)
    {
      LivePerformance compare = obj as LivePerformance;
      if (compare != null)
        if (this.LiveMusicEvent == compare.LiveMusicEvent && this.Artist == compare.Artist && this.Setlist == compare.Setlist && this.ccsCategory == compare.ccsCategory)
          return true;
        else
          return false;
      else
        return false;
    }

    public override int GetHashCode()
    {
      unchecked
      {
        int hash = 17;
        hash = hash * 23 + this.LiveMusicEvent.GetHashCode();
        hash = hash * 23 + this.Artist.GetHashCode();
        hash = hash * 23 + this.Setlist.GetHashCode();
        hash = hash * 23 + this.ccsCategory.GetHashCode();

        return hash;
      }
    }
  }
}
