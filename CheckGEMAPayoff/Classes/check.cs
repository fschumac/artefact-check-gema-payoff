﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace CheckGEMAPayoff.Classes
{
	public class TrackUsage : IComparable
	{
		public int WorkNumber { get; set; }
		public string WorkTitle { get; set; }
		public string Composer { get; set; }
		public DateTime ShowDate { get; set; }
		public string PLZ { get; set; }
		public string Cat { get; set; }
		public ObservableCollection<GEMA_Usage> Gema_Usage { get; set; }

		public TrackUsage()
		{
			Gema_Usage = new ObservableCollection<GEMA_Usage>();
		}

		public int CompareTo(object obj)
		{
			TrackUsage compareToElement = obj as TrackUsage;

			if (this.WorkNumber == compareToElement.WorkNumber)
			{
				if (this.ShowDate == compareToElement.ShowDate)
				{
					if (this.PLZ == compareToElement.PLZ)
					{
						if (this.Cat == compareToElement.Cat)
						{
							return 0;
						}
						else
						{
							return String.Compare(this.Cat, compareToElement.Cat);
						}
					}
					else
					{
						return String.Compare(this.PLZ, compareToElement.PLZ, true);
					}
				}
				else if (this.ShowDate < compareToElement.ShowDate)
				{
					return -1;
				}
				else
				{
					return 1;
				}
			}
			else if (this.WorkNumber < compareToElement.WorkNumber)
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}
	}

	public class ComparerTrackUsageComplete : Comparer<TrackUsage>
	{
		public override int Compare(TrackUsage x, TrackUsage y)
		{
			if (x.WorkNumber == y.WorkNumber)
			{
				if (x.ShowDate == y.ShowDate)
				{
					if (x.PLZ == y.PLZ)
					{
						if (x.Cat == y.Cat)
						{
							return 0;
						}
						else
						{
							return String.Compare(x.Cat, y.Cat);
						}
					}
					else
					{
						return String.Compare(x.PLZ, y.PLZ, true);
					}
				}
				else if (x.ShowDate < y.ShowDate)
				{
					return -1;
				}
				else
				{
					return 1;
				}
			}
			else if (x.WorkNumber < y.WorkNumber)
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}
	}

	public class ComparerTrackUsageIgnoreCat : Comparer<TrackUsage>
	{
		public override int Compare(TrackUsage x, TrackUsage y)
		{
			if (x.WorkNumber == y.WorkNumber)
			{
				if (x.ShowDate == y.ShowDate)
				{
					if (x.PLZ == y.PLZ)
					{
						return 0;
					}
					else
					{
						return String.Compare(x.PLZ, y.PLZ, true);
					}
				}
				else if (x.ShowDate < y.ShowDate)
				{
					return -1;
				}
				else
				{
					return 1;
				}
			}
			else if (x.WorkNumber < y.WorkNumber)
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}
	}

	public class AccountedGig
	{
		public DateTime DayOfShow { get; set; }
		public string Venue { get; set; }
		public string Location { get; set; }
		public string Organizer { get; set; }
		public string PlzVenue
		{
			get
			{
				if (Venue.Trim().Length >= 5)
					return Venue.Trim().Substring(0, 5);
				else
					return "";
			}
		}

		public ObservableCollection<GEMA_Usage> CorrespondingDataSets { get; private set; }

		public AccountedGig()
		{
			CorrespondingDataSets = new ObservableCollection<GEMA_Usage>();
		}
	}

}
