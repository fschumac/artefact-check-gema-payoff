﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheckGEMAPayoff.Classes
{
	public class Setlist
	{
		public IList<Work> Works { get; private set; }

		public Setlist()
		{
			Works = new List<Work>();
		}

		public override bool Equals(object obj)
		{
			Setlist compare = obj as Setlist;
			if (compare != null)
			{
				if (this.Works.Count != compare.Works.Count)
					return false;

				foreach (Work work in this.Works)
					if (!compare.Works.Contains(work))
						return false;

				foreach (Work work in compare.Works)
					if (!this.Works.Contains(work))
						return false;

				return true;
			}
			else
				return false;
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hash = 17;
				hash = hash * 23 + this.Works.GetHashCode();

				return hash;
			}
		}
	}
}
