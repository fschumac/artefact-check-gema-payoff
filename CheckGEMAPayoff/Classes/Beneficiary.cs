﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace CheckGEMAPayoff.Classes
{
	public class Beneficiary
	{
		public int AboveAccountNumber { get; set; }
		public int MainAccountNumber { get; set; }
		public string MainAccountOwner { get; set; }
	}
}
