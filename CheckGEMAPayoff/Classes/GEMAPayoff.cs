﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace CheckGEMAPayoff.Classes
{
	public class GEMA_PayoffSet
	{
		public int id { get; set; }
		public string Category { get; set; }
		public DateTime PayoffDate { get; set; }
		public string Description { get; set; }
	}

	public class GEMA_Beneficiary
	{
		public int id { get; set; }
		public int AboveAccountNumber { get; set; }
		public int MainAccountNumber { get; set; }
		public string MainAccountOwner { get; set; }
		public string Description { get; set; }
		public GEMA_PayoffSet PayoffSet { get; set; }
	}

	public class GEMA_WorkIdentification
	{
		public int id { get; set; }
		public int WorkNumber { get; set; }
		public int CopyNumber { get; set; }
		public string Title { get; set; }
		public string Composer { get; set; }
		public string Editor { get; set; }
		public GEMA_Beneficiary Beneficiary { get; set; }
	}

	public class GEMA_Usage
	{
		public int id { get; set; }
		public string Category { get; set; }
		public DateTime DayOfUsage { get; set; }
		public string PlzVenue
		{
			get
			{
				if (Venue.Trim().Length >= 5)
					return Venue.Trim().Substring(0, 5);
				else
					return "";
			}
		}
		public string Venue { get; set; }
		public string LocationName { get; set; }
		public string Organiser { get; set; }
		public GEMA_WorkIdentification WorkIdentification { get; set; }
	}
}
