﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace CheckGEMAPayoff.Classes
{
	public class ArtistInfo
	{
		public string Name { get; private set; }
		public ObservableCollection<ReportedGig> Shows { get; set; }
		public Dictionary<string, IList<Work>> Tracklists { get; private set; }
		public String ShowDataFileName { get; private set; }
		public int ErrorCount { get; set; }

		public ArtistInfo(string artistName, string showDataFileName)
		{
			Name = artistName;
			ShowDataFileName = showDataFileName;
			Shows = new ObservableCollection<ReportedGig>();
			Tracklists = new Dictionary<string, IList<Work>>();
		}
	}
}
