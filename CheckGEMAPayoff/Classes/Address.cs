﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheckGEMAPayoff.Classes
{
	public class Address
	{
		public string Name { get; set; }
		public string Street { get; set; }

		public string Postalcode { get; set; }
		public string City { get; set; }

		public string Display
		{
			get { return Name + (!string.IsNullOrWhiteSpace(Postalcode) ? " | " + Postalcode.Trim('\r', '\n', ' ') : "") + (!string.IsNullOrWhiteSpace(City) ? " " + City.Trim('\r', '\n', ' ') : "") + (!string.IsNullOrWhiteSpace(Street) ? " | " + Street.Trim('\r', '\n', ' ') : ""); }
		}

		public override bool Equals(object obj)
		{
			Address compare = obj as Address;

			if (compare != null)
			{
				if (compare.Name == this.Name && compare.Street == this.Street && compare.Postalcode == this.Postalcode)
					return true;
				else
					return false;
			}
			return false;
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hash = 17;
				if (this.Name != null)
					hash = hash * 23 + this.Name.GetHashCode();
				if (this.City != null)
					hash = hash * 23 + this.City.GetHashCode();
				if (this.Street != null)
					hash = hash * 23 + this.Street.GetHashCode();
				if (this.Postalcode != null)
					hash = hash * 23 + this.Postalcode.GetHashCode();

				return hash;
			}
		}
	}
}
